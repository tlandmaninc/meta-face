
-- Create New Database named 'CORPUS'
CREATE DATABASE CORPUS
USE CORPUS

-- Create new User & Login Credentials
 
CREATE Login Carmel WITH PASSWORD ='FR2018CGT'  
go
Create USER Carmel For Login Carmel
go

EXEC sp_addrolemember N'db_datareader', N'Carmel'
go

EXEC sp_addrolemember N'db_datawriter', N'Carmel'
go

/*==============================================================*/
/* Table: Video_Analysis                                        */
/*==============================================================*/

CREATE TABLE CORPUS.dbo.Video_Analysis (
   VideoID              int                  not null,
   Name                 varchar(max)        null,
   Path                 varchar(max)        null,
   Size                 decimal              null,
   Format               varchar(1024)        null,
   FrameRate            int                  null,
   Duration             decimal(8)           null,
   ResHeight            decimal(8)           null,
   ResWidth             decimal (8)           null,
   hasVoice             bit                  null,
   Gender               varchar(25)          null,
   EstimatedAge         varchar(25)          null,
   Ethnicity            varchar(25)          null,
   hasGlasses           varchar(25)          null,
   hasMoustache         varchar(25)          null,
   hasBeard             varchar(25)          null,
   ImageQuality			varchar(30)			 null,
   StateAnalysis        varchar(max)		 null,
   DetailedAnalysis     varchar(max)         null,
   VideoFile            varchar(max)         null,
   FromSubDB            int                  null,
    CONSTRAINT PK_MOVIE_ANALYSIS PRIMARY KEY NONCLUSTERED (VideoID)
)
go



/*==============================================================*/
/* Table: SubDB                                                 */
/*==============================================================*/
CREATE TABLE CORPUS.dbo.SubDB (
   ID                   int                  not null,
   Name                 varchar(1024)        null,
   Description          varchar(1024)        null,
   isActed              varchar(100)         null,
   hasSpeech            bit			         null,
   Link                 varchar(1024)        null,
   EULA                 varchar(1024)        null, 
   TermOfUse            varchar(1024)        null,
   Rights               varchar(1024)        null,
   CreatedOn            int					 null,
   CONSTRAINT PK_SUBDB PRIMARY KEY NONCLUSTERED (ID)
)
go


/*==============================================================*/
/* Inserting Sub DB Tags Data to Table: SubDB                                                 */
/*==============================================================*/
BULK INSERT CORPUS.dbo.SubDB
FROM 'Z:\carmel\MetaFace\DB\Sub DB Documentation\SubDB-Tags-Data-Insertion.csv'
WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = '|',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
    TABLOCK
)
go


--//////////////////////////////////////////////////////////////////////INDEX//////////////////////////////////////////////////////////////////////////////////*/

/*Index of (Gender,EstimatedAge,Ethnicity) Combinations*/
USE CORPUS
CREATE INDEX 	IDX_VA_GenderAgeEthnicity
ON 	CORPUS.dbo.Video_Analysis (Gender,EstimatedAge,Ethnicity)
go

/*Index of (Gender,EstimatedAge,Ethnicity,hasGlasses,hasBeard,hasMoustache) Combinations*/
CREATE INDEX 	IDX_VA_GenderAgeEthnicityGlassesBearedMoustache
ON 	dbo.Video_Analysis (Gender,EstimatedAge,Ethnicity,hasGlasses,hasBeard,hasMoustache)
go

CREATE INDEX 	IDX_VA_GenderAgeEthnicityGlassesBearedMoustache
ON 	dbo.Video_Analysis (Gender,EstimatedAge,Ethnicity,hasGlasses,hasBeard,hasMoustache)
go

--//////////////////////////////////////////////////////////////////////RULES//////////////////////////////////////////////////////////////////////////////////
--/Approved Format Values Set/ 
CREATE RULE Approved_Formats AS @Format IN ('MP3', 'AVI', 'MP4','MPG','VOB','MOV','ASF','BGP','RM','WMV','FLV') 
EXECUTE sp_bindrule 'Approved_Formats', 'dbo.Video_Analysis.Format'
go


/*Approved Frame Rate Range*/ 
CREATE   RULE   FrameRate_Range AS @FrameRateRange >=15 AND @FrameRateRange < 60
EXECUTE sp_bindrule 'FrameRate_Range', 'dbo.Video_Analysis.FrameRate'
GO


/*Approved Gender Values Set*/ 
CREATE   RULE   Approved_Gender AS @Gender IN ('Female','Male')
EXECUTE sp_bindrule 'Approved_Gender', 'dbo.Video_Analysis.Gender'
GO


/*Approved Ethnicity Values Set*/ 
CREATE RULE   Approved_Ethnicity AS @Ethnicity IN ('Caucasian','Eastern Asia','African','South Asia','Other')
EXECUTE sp_bindrule 'Approved_Ethnicity', 'dbo.Video_Analysis.Ethnicity'
GO

/*Approved hasCharacters Values Set*/ 
CREATE   RULE   Approved_hasCharacter AS @has IN ('Yes','No','None','Some','Heavy')
EXECUTE sp_bindrule 'Approved_hasCharacter', 'dbo.Video_Analysis.hasGlasses'
GO
EXECUTE sp_bindrule 'Approved_hasCharacter', 'dbo.Video_Analysis.hasMoustache'
GO
EXECUTE sp_bindrule 'Approved_hasCharacter', 'dbo.Video_Analysis.hasBeard'
GO

           
/*Approved Duration Values Set*/
CREATE   RULE   Positive_Duration AS @Duration > 0
EXECUTE sp_bindrule 'Positive_Duration', 'dbo.Video_Analysis.Duration'
GO

/*Approved Estimated Age Format as Range - [% - %]*/ 
CREATE   RULE     EstimatedAge_Range AS @EstimatedAge LIKE '%-%'
EXECUTE sp_bindrule 'EstimatedAge_Range', 'dbo.Video_Analysis.EstimatedAge'
GO


/* From sub DB based on new Num*/
UPDATE dbo.Video_Analysis
SET FromSubDB =
				(CASE
	 WHEN (Path LIKE '%Affectiva%' ) THEN 1
	 WHEN (Path LIKE '%Belfast%' )	THEN 2
	 WHEN (Path LIKE '%Biwi%' ) THEN 3
	 WHEN (Path LIKE '%Cohn-Kanade%' ) THEN 4
	 WHEN (Path LIKE '%Denver%' ) THEN 5
	 WHEN (Path LIKE '%FEED%' ) THEN 6	 
	 WHEN (Path LIKE '%MMI%' ) THEN 7
	 WHEN (Path LIKE '%FABO%' ) THEN 8	 
	 WHEN (Path LIKE '%UNBC%' ) THEN 9
	 ELSE 10
	 END)
GO

