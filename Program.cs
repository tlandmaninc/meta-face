﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using Shell32;
using System.IO;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Collections;
using System.Configuration.Assemblies;
using System.Configuration;
using System.Globalization;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Drawing;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
//Add reference browse C:\Windows\System32\Shell32.Dll
//using Microsoft.SqlServer.Dts.Runtime;


namespace MetaFace
{
    class Program
    {
                
        //Global Variables
        private static int maxID = 0;
        public static SqlConnection gCnn;

        //Configuration Decleration
        static System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

        //Returns Settings Paramaters
        public static KeyValueConfigurationCollection getAccessParameters()
        {
            System.Configuration.Configuration conf = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            return conf.AppSettings.Settings;
        }//getAccessParameters

        //Returns Default Computer Name
        public static String getDefaultComputerName()
        {
            KeyValueConfigurationCollection Settings = getAccessParameters();
            //Index to navigate Computer Name
            int i = 0;
            String CN = "-1";
            //Displaying Defaults Values
            foreach (KeyValueConfigurationElement keyValueElement in Settings)
            {
                //Computer Name Location
                if (i == 0)
                {
                    CN = keyValueElement.Value.ToString();
                    break;
                }        
            }//foreach
            return CN;
        }//getDefaultComputerName
        
        //Returns Default Server Name
        public static String getDefaultServerName()
        {
            KeyValueConfigurationCollection Settings = getAccessParameters();
            //Index to navigate Computer Name
            int i = 0;
            String SN = "-1";
            //Displaying Defaults Values
            foreach (KeyValueConfigurationElement keyValueElement in Settings)
            {
                //Server Name Location
                if (i == 1)
                {
                    SN = keyValueElement.Value.ToString();
                    break;
                }                  
                i++;
            }//foreach
            return SN;
        }//getDefaultServerName

        //Returns Default Backup Folder Path
        public static String getDefaultBackupFolderPath()
        {
            KeyValueConfigurationCollection Settings = getAccessParameters();
            //Index to navigate Backup Folder Path
            int i = 0;
            String BFP = "-1";
            //Displaying Defaults Values
            foreach (KeyValueConfigurationElement keyValueElement in Settings)
            {
                //Backup Folder Path Location
                if (i == 2)
                {
                    BFP = keyValueElement.Value.ToString();
                    break;
                }
                    
                i++;
            }//foreach
            return BFP;
        }//getBackupFolderPath

        //Returns Default Database Folder Path
        public static String getDefaultDatabeseFolderPath()
        {
            KeyValueConfigurationCollection Settings = getAccessParameters();
            //Index to navigate Database Folder Path
            int i = 0;
            String DBF = "-1";
            //Displaying Defaults Values
            foreach (KeyValueConfigurationElement keyValueElement in Settings)
            {
                //DB Folder Path Location
                if (i == 3)
                {
                    DBF = keyValueElement.Value.ToString();
                    break;
                }  
                i++;
            }//foreach
            return DBF;
        }//getDatabeseFolderPath


        //Returns Default Backup Time Interval
        public static String getDefaultBackupTimeInterval()
        {
            KeyValueConfigurationCollection Settings = getAccessParameters();
            //Index to navigate Backup Time Interval
            int i = 0;
            String BTI = "-1";
            //Displaying Defaults Values
            foreach (KeyValueConfigurationElement keyValueElement in Settings)
            {
                //Backup Time Interval Location
                if (i == 4)
                {
                    BTI = keyValueElement.Value.ToString();
                    break;
                }                    
                i++;
            }//foreach
            return BTI;
        }//getDefaultBackupTimeInterval


        //Need to add Remove + Add Commands for each Global Variable & Connect to Form Text Fields + Check Per each attribute
        public static void setAccessParameters(String CN, String SN, String BF, String DBF, String BT, String IP,
            String P)
        {
            /////If check all attributes is fine then{
            config.AppSettings.Settings.Remove("ComputerName");
            config.AppSettings.Settings.Remove("ServerName");
            config.AppSettings.Settings.Remove("BackupFolder");
            config.AppSettings.Settings.Remove("DBFolder");
            config.AppSettings.Settings.Remove("BackupTime");
            config.AppSettings.Settings.Remove("IP");
            config.AppSettings.Settings.Remove("Port");
            config.AppSettings.Settings.Add("ComputerName", CN);
            config.AppSettings.Settings.Add("ServerName", SN);
            config.AppSettings.Settings.Add("BackupFolder", BF);
            config.AppSettings.Settings.Add("DBFolder", DBF);
            config.AppSettings.Settings.Add("BackupTime", BT);
            config.AppSettings.Settings.Add("IP", IP);
            config.AppSettings.Settings.Add("Port", P);
            config.Save(ConfigurationSaveMode.Minimal);
            MessageBox.Show("Settings Saved Successfully!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);
        }//setAccessParameters

        //Backup Timer Global Variables
        static System.Windows.Forms.Timer backupTimer = new System.Windows.Forms.Timer();
        static int alarmCounter = 1;
        static bool exitFlag = false;


        //RemoteFolderCopy - Copy Folder From Source Folder Path to Target Folder Path => Both Paths Must be Shareable!!!!!!
        public static Boolean RemoteFolderCopy(String SourceFolderPath, String TargetFolderPath)
        {
            Boolean Success = true;
            //Editing Strings By Path Format
            String SourcePath = SourceFolderPath;
            String DestinationPath = TargetFolderPath + @"\Backup-"+ DateTime.Now.ToShortDateString().Replace('/','.')+@"\";
            try
            {
                Directory.CreateDirectory(DestinationPath);
                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                    SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

                //Copy all the files & Replaces any files with the same name
                foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                    SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
            }//try
            catch (Exception ex)
            {
                Success = false;
            }//catch

            return Success;
        }//RemoteFolderCopy

        //Backup DB - First Creates New DB by DBName Input then Copies Video ANalysis & Sub DB Tables to New Created DB 
        private static Boolean backupSQLDatabase(SqlConnection con, String DBName)
        {
            //SqlConnection myConn = new SqlConnection("Server=TL\\SQLEXPRESS;Integrated security=SSPI;database=master");

            String backupDBString = "Backup database [" + getDefaultServerName() + "] to disk= "+@"'"+getDefaultBackupFolderPath()+ @"\CORPUS-FullBackup.bak' WITH NOFORMAT, NOINIT,  NAME = N'data-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";

            ////Creates SQL Command Object
            SqlCommand backupSQLDB = new SqlCommand(backupDBString, con);

            //Indication for Success
            Boolean Success = true;
            try
            {
                //Open Connection
                con.Open();

                //Executes SQL Commands
                backupSQLDB.ExecuteNonQuery();

                //Close Connection
                con.Close();
            }
            catch (System.Exception ex)
            {
                Success = false;
            }
            return Success;
        }//backupSQLDatabase


        // This is the method to run when the timer is raised.
        private static void TimerEventProcessor(Object myObject,
                                                EventArgs myEventArgs)
        {
            backupTimer.Stop();
            String ErrorMessage= "Backup Error List: " + System.Environment.NewLine;
            int backupErrorCounter = 0;

            /////////////////////////////////////////////////////////////// Backup Operations /////////////////////////////////////////////////////////

            //Temp Connection String
            string connetionString = "Data Source="+getDefaultComputerName()+ @"\"+getDefaultServerName() + ";Initial Catalog=CORPUS;Integrated Security=True";

            //Init Connection
            SqlConnection cnn = new SqlConnection(connetionString);

            //Backup SQL Database & Write error in case that copy failed
            if (!backupSQLDatabase(cnn, getDefaultServerName()))
            {
                ErrorMessage += "Couldn't Backup SQL Database Folder to Backup Folder! Please check if there's enough space!" + System.Environment.NewLine;
                backupErrorCounter++;
            }//If SQL DB Full Backup Failed

            
            //Copy Default Database Folder to selected Default Backup Folder & Write error in case that copy failed
            if (!RemoteFolderCopy(getDefaultDatabeseFolderPath(), getDefaultBackupFolderPath()))
            {
                ErrorMessage += "Couldn't Copy Database Folder to Backup Folder! Please check if there's enough space!" + System.Environment.NewLine;
                backupErrorCounter++;
            }//If DB Folder Copy 


            //If there are Errors - Notify to user
            if (!(backupErrorCounter > 0))
            {
                //Global Backup Folder
                string rootBackupFolder = getDefaultBackupFolderPath();

                //New Backup Subfolder
                String DestinationPath = getDefaultBackupFolderPath() + @"\Backup-" + DateTime.Now.ToShortDateString().Replace('/', '.');

                // Get all subdirectories
                string[] subdirectoryEntries = Directory.GetDirectories(rootBackupFolder);

                //Loop through them to see if they have any other subdirectories
                foreach (string subdirectory in subdirectoryEntries)
                {
                    //if isn't equal to the new performed backup Folder
                    if (!subdirectory.Equals(DestinationPath))
                    {
                        //Delete Backup History Folders
                        try
                        {
                            //Folders to be deleted
                            var dir = new DirectoryInfo(subdirectory);
                            dir.Delete(true);
                        }//try
                        catch (IOException ex)
                        {}//catch

                    }//if Backup History Folder

                }//for each sub folder

                MessageBox.Show("Backup Completed Successfully!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);

            }//If Errors
                
            else MessageBox.Show(ErrorMessage, "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);


            /////////////////////////////////////////////////////////////// Backup Operations /////////////////////////////////////////////////////////

            //New Timer Initialization - Every 24 Hours
            backupTimer.Interval = 24 * 60 * 60 * 1000;
            backupTimer.Start();

            // Restarts the timer and increments the counter.
            alarmCounter += 1;
            backupTimer.Enabled = true;
        }//TimerEventProcessor

        public static void updateBackupSchedule(int HH, int MM)
        {
            //Global Configuration
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

            //Global Settings
            KeyValueConfigurationCollection settings = config.AppSettings.Settings;

            //Setting Backup Today Date
            DateTime setTodayBackup = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            /* Adds the event and the event handler for the method that will 
              process the timer event to the timer. */
            backupTimer.Tick += new EventHandler(TimerEventProcessor);

            //Sets Backup Today Full Time Stamp + Set based on User Backup Time Setting
            int Hour = HH, Minutes = MM, i = 0;

            //Extracting Default backupTime
            foreach (KeyValueConfigurationElement keyValueElement in settings)
            {
                if (i == 4)
                {
                    Hour = Int32.Parse(keyValueElement.Value.ToString().Split(':')[0]);
                    Minutes = Int32.Parse(keyValueElement.Value.ToString().Split(':')[1]);
                    break;
                }//if BackupTime
                i++;
            }//for each

            //Setting Backup Time
            TimeSpan setBackupHour = new TimeSpan(Hour, Minutes, 0);
            setTodayBackup = setTodayBackup.Date + setBackupHour;

            //Now TimeStamp
            DateTime rightNow = DateTime.Now;

            // Sets the timer interval based on User Backup Time Set
            TimeSpan diff = setTodayBackup - rightNow;

            //Total Time till backup start in Milliseconds
            int totalMS = diff.Hours * 3600000 + diff.Minutes * 60000;

            //Check if time inserted is invalid (= Earlier = Negative)
            if (!(totalMS > 0))
                totalMS += 24*3600000;

            //Case of more than 1 day interval
            if (totalMS <= 0)
                totalMS = totalMS * -1;

            //Set Backup Timer
            backupTimer.Interval = totalMS;

            //Start backup Timer
            backupTimer.Start();

            // Runs the timer, and raises the event.
            while (exitFlag == false)
            {
                // Processes all the events in the queue.
                Application.DoEvents();
            }//while

        }//updateBackupSchedule



        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        //Sets Global Server Connection
        public static void setgCnn(SqlConnection con)
        {
            gCnn = con;
            //gCnn.Open();
        }//setgCnn

        //Gets Global Server Connection
        public static SqlConnection getgCnn()
        {
            return gCnn;
        }//getgCnn

        //Create connection to Default Server Name Without Authorization
        public static void connectToServer()
        {
            string connetionString = "Data Source="+getDefaultComputerName()+ @"\"+getDefaultServerName() + "; Initial Catalog=CORPUS;Integrated Security=True";
            //string connetionString = "Data Source= COMP9843" + @"\SQLEXPRESS" + ";Initial Catalog=CORPUS;Integrated Security=True";

            SqlConnection cnn = new SqlConnection(connetionString);
            try
            {
                setgCnn(cnn);
                gCnn.Open();
                MessageBox.Show("Welcome to MetaFace! ","MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);
                //cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check User Name & Password! ","MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }//connectToServer - Without Authorization

        //Create connection to Default Server Name - With Authorization
        public static Boolean connectToServer(String user, String pass)
        {
            Boolean success = true;

            ////////////////////////////////////////////////////////////////////////////////////////In Final Change between strings



            String connectionAuthorString = "Data Source=" + getDefaultComputerName() + @"\" + getDefaultServerName() + "; Initial Catalog=CORPUS;User id =" + user + ";Password=" + pass + ";";
            //string connectionAuthorString = "Data Source=" + getDefaultComputerName() + @"\" + getDefaultServerName() + "; Initial Catalog=CORPUS;Integrated Security=True";
            //String connectionAuthorString = "Data Source="+ " 132.72.161.129,1433;Network Library=DBMSSOCN ; Initial Catalog=CORPUS;User id =" + user + ";Password=" + pass + ";";
            //   String connectionAuthorString = "Data Source=132.72.160.205"+@"\COMP9843,1433;Network Library=DBMSSOCN;Initial Catalog=CORPUS;User id =" + user + ";Password=" + pass + ";";
            // String connectionAuthorString = "Data Source=132.72.160.205,1433;Initial Catalog=CORPUS;User id =" + user + ";Password=" + pass + ";";//Works Good
            // String connectionAuthorString = "Data Source=132.72.161.129,1433;Initial Catalog=CORPUS;User id =" + user + ";Password=" + pass + ";";//Not working - Cyber Security????


             //String connectionAuthorString = "Data Source=132.72.161.129,1433;Initial Catalog=CORPUS;User id =" + "Integrated Security = True";
          //  string connectionAuthorString = "Data Source=132.72.160.205,1433; Initial Catalog=CORPUS;Integrated Security=True";

            ////Data Source = 190.190.200.100\MyInstance,666; Network Library = DBMSSOCN; Initial Catalog = myDataBase; User ID = myUsername; Password = myPassword;

            ////////////////////////////////////////////////////////////////////////////////////////In Final Change between strings

            SqlConnection cnn = new SqlConnection(connectionAuthorString);
            try
            {
                setgCnn(cnn);
                gCnn.Open();
                MessageBox.Show("Welcome to MetaFace! ", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);
                
                //cnn.Close();
            }
            catch (Exception ex)
            {
                success = false;
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Can't Connect to Server! Please Check User Name & Password!  ", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }//catch

            return success;
        }//connectToServer - Without Authorization

        /*Extracts Multimedia Meta data to a String Vector From video File Path input:
 *[Format|FrameRate|Duration|ResolutionHeight|ResolutionWidth|hasVoice]*/
        //    public static String GetVideoProporties(String FilePath,int MaxID)
        public static object[] GetVideoProporties(String FilePath, int MaxID)
        {
            //Init Error Counter
            int ErrorCounter = 0;

            //MediaToolKit Objects - Nuget Installation - Retrieving Audio & Video Data
            var videoFile_MTK = new MediaToolkit.Model.MediaFile { Filename = FilePath };
            using (var engine = new MediaToolkit.Engine())
            {
                engine.GetMetadata(videoFile_MTK);
            }

            //Declaring & Init Variables
            int Height = -1, Width = -1, IDCounter = MaxID;
            Boolean hasVoice = false;
            Double FrameRate = -1;
            Decimal Size = -1, Duration = -1;
            String FileName = "-1", Format = "-1", DurationS = "-1", SizeS = "-1", Name = "-1";

            //Extracting Resolution Attributes 
            try
            {
                //TagLib Objects - Nuget Installation
                TagLib.File videoFile = TagLib.File.Create(@FilePath);
                Height = Int32.Parse(videoFile.Properties.VideoHeight.ToString());
                Width = Int32.Parse(videoFile.Properties.VideoWidth.ToString());
            }//try Resolution Attributes
            
            catch (Exception ex)
            {
                MessageBox.Show(FilePath);
                //If Error then Height=Width=-1
                ErrorCounter++;
            }//catch Resolution Attributes

            //Ext           racting FrameRate
            try
            {
                FrameRate = videoFile_MTK.Metadata.VideoData.Fps;
            }//try FrameRate

            catch (NullReferenceException)
            {
                //If Error then FrameRate=-1
                ErrorCounter++;
            }//catch FrameRate

            //Extracting Duration 
            try
            {
                Duration = Convert.ToDecimal(videoFile_MTK.Metadata.Duration.TotalMinutes);
                //TimeSpan t1 = TimeSpan.FromSeconds(videoFile_MTK.Metadata.Duration.TotalSeconds);             
                //String str1 = t1.ToString(@"hh\:mm\:ss");
                
                 DurationS = Duration.ToString("#.###");
                //DurationS = Duration.ToString("");
            }//try Duration

            catch (NullReferenceException)
            {
                //If Error then Duration=-1
                ErrorCounter++;
            }//catch Duration


            //Extracting hasVoice 
            try
            {
                //If Video has sound then hasVoice = TRUE else hasVoice = False
                if (videoFile_MTK.Metadata.AudioData.BitRateKbs.ToString() != null)
                    hasVoice = Convert.ToBoolean(videoFile_MTK.Metadata.AudioData.BitRateKbs);
            }//try hasVoice

            catch (NullReferenceException)
            {
                ErrorCounter++;
            }//catch hasVoice

            //Extracting FileName & Format 
            try
            {
                //File Name
                FileName = videoFile_MTK.Filename;
                //Handeling Format
                Format = Path.GetExtension(FileName).Substring(1);
            }//try File Name & Format

            catch (NullReferenceException)
            {
                //If Error then FileName=Format=-1
                ErrorCounter++;
            }//catch File Name & Format


            //Extracting File Size 
            try
            {
                FileInfo file = new FileInfo(FilePath);
                
                //File Size
                Size = ((Convert.ToDecimal(Convert.ToDecimal(file.Length)/ Convert.ToDecimal(1024000))));
                SizeS = Size.ToString("#.##");
            }//try File Size

            catch (NullReferenceException)
            {
                //If Error then Size=-1
                ErrorCounter++;
            }//catch File Size

            //Extracting File Name 
            try
            {
                FileInfo file = new FileInfo(FilePath);
                //File Name
                Name = file.Name.Split('.')[0];
            }//try File Name

            catch (NullReferenceException)
            {
                //If Error then Name=-1
                ErrorCounter++;
            }//catch File Name


            //initializing array of Objects that later will store the metadata info
            object[] fileMetadataInfo = new object[9];
            fileMetadataInfo[0] = Name;
            fileMetadataInfo[1] = FilePath;
            fileMetadataInfo[2] = SizeS;
            fileMetadataInfo[3] = Format;
            fileMetadataInfo[4] = FrameRate;
            fileMetadataInfo[5] = DurationS;
            fileMetadataInfo[6] = Height;
            fileMetadataInfo[7] = Width;
            fileMetadataInfo[8] = hasVoice;


            //String VideoMeta = Name + "|" + FilePath + "|" + SizeS + "|" + Format + "|" + FrameRate + "|" + DurationS +
            //                         "|" + Height + "|" + Width + "|" + hasVoice + "|";

            if (ErrorCounter > 1)
                //   return FileName + " Has " + (ErrorCounter - 1) + " Errors!!";
                return fileMetadataInfo;

            else
            {
                IDCounter++;
                //   return IDCounter + "|" + VideoMeta;
                return fileMetadataInfo;
            }//else

        }//GetVideoProporties

        //Increase Progree Bar
        public void increaseProgressBar(int Max, ProgressBar ProgressBar)
        {
            ProgressBar PB = ProgressBar;
            PB.Maximum = Max;
            PB.Value++;
            //insertProgressBar.Value=1;
            //setProgressBar(1);
        }

        public static void insertFilesInfo(String dirPath, int flag)
        {
            var ext = new List<string> {".avi", ".mp4", ".mpg" ,"flv","mpeg","vob","mov","asf","bgp","wmv","rm"};
            // 
            string[] files = Directory.GetFiles(dirPath, "*.*", SearchOption.AllDirectories);
            //    .Where(s => ext.Contains(Path.GetExtension(s)))

            //If Videos Only
            if(flag==0)
                insertUploadForm.insertProgressBar.Maximum = files.Length;

            //Videos & Analysis
            else insertUploadForm.insertProgressBar.Maximum = files.Length*2;

            //Show Progress bar
            insertUploadForm.insertProgressBar.Show();

            int maxID=0,VideoCounter=0;
            SqlCommand findMaxIDSqlCmd = new SqlCommand();
            findMaxIDSqlCmd.Connection = gCnn;
            findMaxIDSqlCmd.CommandText = "SELECT MAX(dbo.Video_Analysis.VideoID) FROM CORPUS.DBO.Video_Analysis";
            if (findMaxIDSqlCmd.ExecuteScalar().GetType() == typeof(int))
                maxID = Int32.Parse(findMaxIDSqlCmd.ExecuteScalar().ToString());
            VideoCounter = maxID;
            
            //loop through files in the distantion
            foreach (String filename in files)
                {
                if (ext.Contains(Path.GetExtension(filename)))
                {

                    //Increase ProgressBar Value
                    insertUploadForm.insertProgressBar.Value += 1;

                    VideoCounter++;
                    object[] fileDetails = GetVideoProporties(filename, VideoCounter);

                    //Checking if video is in DB Others Folder else copy file
                    String sourcePath = fileDetails[1].ToString();
                    String targetPath= sourcePath, fileName="", DBFold="";

                    //If Not Contains DB Folder Path
                    if (!(sourcePath.Contains(getDefaultDatabeseFolderPath())|| containSubDB(sourcePath)))
                    {
                        targetPath = Path.Combine(Path.Combine(getDefaultDatabeseFolderPath(),"Others"), fileDetails[0].ToString() + "." + fileDetails[3]);
                        File.Copy(sourcePath, targetPath,true);
                    }//if not exist in DB Folder

                    else if (containSubDB(sourcePath))
                    {
                        fileName = sourcePath.Substring(sourcePath.LastIndexOf("DB") + 3);
                        DBFold = getDefaultDatabeseFolderPath();
                        targetPath = "";
                        targetPath = Path.Combine(@DBFold, fileName);
                    }
                    

                    //sql comand to insert the metadata to the DB
                    SqlCommand insertSqlCmd = new SqlCommand();
                    insertSqlCmd.Connection = gCnn;
                    insertSqlCmd.CommandText = "INSERT INTO CORPUS.dbo.SubDB VALUES(" + fileDetails[0].ToString() + ")";
                    insertSqlCmd.Parameters.AddWithValue("@VideoID", VideoCounter);
                    insertSqlCmd.Parameters.AddWithValue("@Name", fileDetails[0]);
                    insertSqlCmd.Parameters.AddWithValue("@FilePath", targetPath);
                    insertSqlCmd.Parameters.AddWithValue("@SizeS", fileDetails[2]);
                    insertSqlCmd.Parameters.AddWithValue("@Format", fileDetails[3]);
                    insertSqlCmd.Parameters.AddWithValue("@FrameRate", fileDetails[4]);
                    insertSqlCmd.Parameters.AddWithValue("@DurationS", fileDetails[5]);
                    insertSqlCmd.Parameters.AddWithValue("@Height", fileDetails[6]);
                    insertSqlCmd.Parameters.AddWithValue("@Width", fileDetails[7]);
                    insertSqlCmd.Parameters.AddWithValue("@hasVoice", fileDetails[8]);
                    insertSqlCmd.Parameters.AddWithValue("@FromSubDB", 10);                                       
                    insertSqlCmd.CommandText = "INSERT INTO CORPUS.dbo.Video_Analysis(VideoID,Name,Path,Size,Format,FrameRate,Duration,ResHeight,ResWidth,hasVoice,FromSubDB) VALUES(@VideoID,@Name,@FilePath,@SizeS,@Format,@FrameRate,@DurationS,@Height,@Width,@hasVoice,@FromSubDB)";

                    try
                    {
                        insertSqlCmd.ExecuteNonQuery();
                        //inserting Name-Path-Size-Format-hasVoice-Duration- Resolution-Frame Rate
                        String[] row = {fileDetails[0].ToString(), fileDetails[1].ToString(), fileDetails[2].ToString(), fileDetails[3].ToString(), fileDetails[8].ToString() , fileDetails[5].ToString(), fileDetails[6].ToString()+"x"+fileDetails[7].ToString(), fileDetails[4].ToString()};
                        var listViewItem = new ListViewItem(row);
                        insertUploadForm.insertVideosListView.Items.Add(listViewItem);
                        
                    }
                    catch (Exception ex)
                    {
                        VideoCounter--;
                    }
                }
            }//foreach

            if(flag==0)

            MessageBox.Show((VideoCounter-maxID) + " New Videos were inserted into MetaFace Corpus!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);

        }//insertFilesInfo

        [STAThread]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Checks if classification value is valid
        public static Boolean isValidClassification(String classification)
        {
            if ((classification == "FIT_FAILED") || (classification == "FIND_FAILED"))
                return false;

            else return true;
        }//isValidClassification

        //Checks if path contain subDB
        public static Boolean containSubDB(String path)
        {
            if (path.Contains("Affectiva")|| path.Contains("Belfast")|| path.Contains("Biwi")|| path.Contains("Kanade")|| path.Contains("Denver")
                || path.Contains("FEED")|| path.Contains("MMI") || path.Contains("FABO") || path.Contains("UNBC"))
                return true;

            else return false;
        }//containSubDB


        /// <summary>
        /// Extract Metadata from FR Detailed Log files
        /// </summary>
        /// 
        public static void getClassificationFromDetailedAnalysis(string FolderPath)
        {
            DirectoryInfo d = new DirectoryInfo(FolderPath);
            long LogsCounter = 0;
            long IDCounter = 0;
            long ErrorCounter = 0;
            String newStateLogName = "", newDetLogName = "", detAnalysisFName = "", stateAnalysisFName = "";

            //Error String
            String errorLog = "Videos Without Classification: " + System.Environment.NewLine;

            try         
            {
                
                //For every Detailed Analysis log perform...
                foreach (var file in d.GetFiles("*detailed.txt", SearchOption.AllDirectories))
            {

                    //Increase ProgressBar Value
                    insertUploadForm.insertProgressBar.Value += 1;
                    LogsCounter++;
                detAnalysisFName = file.FullName;
                stateAnalysisFName = detAnalysisFName.Replace("detailed", "state");
                using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        //Read Log Files Loop
                        while (!sr.EndOfStream)
                        {
                            //Order By Lines Array
                            String[] lines = sr.ReadToEnd().Split(new char[] { '\n' });


                                String sourceVideoPath = lines[5].Split('\t')[1], VideoPath="";

                                //VideoPath = "\132.72.160.213\workingdir\workingdir\carmel\MetaFace\DB\"+lines[5].Split('\t')[1].Substring(10);
                                //String vidPathName = lines[5].Split('\t')[1].Substring(9);

                                //If From DB Folder
                                if (sourceVideoPath.Contains(getDefaultDatabeseFolderPath()))
                                    VideoPath = sourceVideoPath;

                                //If From SubDB Folder
                                else if (containSubDB(sourceVideoPath))
                                {
                                    String vidPathName = lines[5].Split('\t')[1].Substring(lines[5].Split('\t')[1].LastIndexOf("DB") + 7);
                                    VideoPath = Path.Combine(getDefaultDatabeseFolderPath(), vidPathName);
                                    ////Temporary
                                    VideoPath=VideoPath.Replace("Movies", "Videos");
                                     /////
                                }

                                else
                                {
                                    String vidPathName = lines[5].Split('\t')[1].Substring(9);
                                    VideoPath = Path.Combine(Path.Combine(getDefaultDatabeseFolderPath(), "Others"), vidPathName.Substring(vidPathName.LastIndexOf(@"\") + 1));
                                }
                           

                                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                String Gender ="",Range="",hasBeared = "", hasMoustache = "", hasGlasses = "", Ethnicity = "", IQ = "", ImageQuality = "";
                                
                                /////////
                                Boolean Gend = false, Ag = false,hasB=false,hasMous=false,Ethnic=false,ImagQ=false, hasGlass=false;
                                for(int i = lines.Length - 1; i > 10; i--)
                                {
                                    
                                    try
                                    {

                                        //If Gender is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[8]) && !Gend)
                                        {
                                            Gender = lines[i].Split('\t')[8];
                                            Gend = true;
                                        }//Gender is Valid

                                        //If Age is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[9]) && !Ag)
                                        {
                                            Range = lines[i].Split('\t')[9];
                                            String[] separators = { "-" };
                                            String[] SplitedRange = { };
                                            SplitedRange = Range.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                                            int AgeL = Int32.Parse(SplitedRange[0]);
                                            int AgeU = Int32.Parse(SplitedRange[1]);
                                            Ag = true;
                                        }//Age is Valid

                                        //If hasBeard is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[10]) && !hasB)
                                        {
                                            hasBeared = lines[i].Split('\t')[10];
                                            hasB = true;
                                        }//hasBeared is Valid

                                        //If hasMoustache is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[11]) && !hasMous)
                                        {
                                            hasMoustache = lines[i].Split('\t')[11];
                                            hasMous = true;
                                        }//hasMoustache is Valid

                                        //If hasGlasses is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[12]) && !hasGlass)
                                        {
                                            hasGlasses = lines[i].Split('\t')[12];
                                            hasGlass = true;
                                        }//hasGlasses is Valid

                                        //If Ethnicity is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[13]) && !Ethnic)
                                        {
                                            Ethnicity = lines[i].Split('\t')[13];
                                            Ethnic = true;
                                        }//Ethnicity is Valid

                                        //If FR Image Quality is Valid & not Assigned yet
                                        if (isValidClassification(lines[i].Split('\t')[14]) && !ImagQ)
                                        {
                                            IQ = lines[i].Split('\t')[14];
                                            Double IQD = Double.Parse(IQ) * 100;
                                            ImageQuality = IQD.ToString();
                                            ImagQ = true;
                                        }//FR Image Quality is Valid
                                    }//try
                                    catch (Exception ex)
                                    {
                                        
                                    }
                                }//for each line in Detailed Log

                                //If All classifications were not extracted
                                if(!(Gend || Ag || hasB || hasMous || hasGlass || Ethnic || ImagQ))
                                {
                                    ErrorCounter++;
                                    //Updating Error String 
                                    errorLog += "Error: " + ErrorCounter + " | Problem In Analysis: " + detAnalysisFName + " On All Classifications Attributes! ";
                                    errorLog += System.Environment.NewLine + "Source Video File: " + VideoPath + System.Environment.NewLine + System.Environment.NewLine;
                                }//if 
                                
                                
                                //If there are No Errors or some Errors regarding FaceReader Classification extractions
                                else 
                                {
                                    //If there are Some Errors
                                    if (!(Gend && Ag && hasB && hasMous && hasGlass && Ethnic && ImagQ))
                                    {
                                        ErrorCounter++;

                                        //Updating Error String 
                                        errorLog += "Error: " + ErrorCounter + " | Problem In Analysis: " + detAnalysisFName + " On these attributes: ";
                                        if (Gend)
                                            errorLog += "|Gender| ";
                                        if (Ag)
                                            errorLog += "|Age| ";
                                        if (hasB)
                                            errorLog += "|Has Beared?| ";
                                        if (hasMous)
                                            errorLog += "|Has Moustache?| ";
                                        if (hasGlass)
                                            errorLog += "|Has Glasses?| ";
                                        if (Ethnic)
                                            errorLog += "|Ethnicity| ";
                                        if (ImagQ)
                                            errorLog += "|Image Quality| ";

                                        errorLog += System.Environment.NewLine + "Source Video File: " + VideoPath + System.Environment.NewLine + System.Environment.NewLine;

                                    }//If there are Some Errors

                                    //int totalUpdates = 0;
                                    
                                    //Analysis Directory
                                    String AnalysisDirectory =  getDefaultDatabeseFolderPath() + "Analysis";
                                    MessageBox.Show(AnalysisDirectory);   
                                    //Rename Detailed Log                            
                                    newDetLogName = AnalysisDirectory + "\\" + VideoPath.Substring(0, VideoPath.LastIndexOf('.')).Replace('\\', '#').Replace(':', '#') + "_detailed.txt";
                                    //newDetLogName = AnalysisDirectory + "\\" + VideoPath.Split('.')[0].Replace('\\', '#').Replace(':', '#') + "_detailed.txt";
                                    MessageBox.Show("msg2"+newDetLogName);


                                    if (!File.Exists(newDetLogName))
                                        System.IO.File.Copy(detAnalysisFName, newDetLogName);

                                    //Rename State Log by Path
                                    newStateLogName = newDetLogName.Replace("_detailed", "_state");

                                    if (!File.Exists(newStateLogName))
                                         System.IO.File.Copy(stateAnalysisFName, newStateLogName);

                                    IDCounter++;                     

                                    //Writing each Metadata Vector to Output Text File

                                    //SQL Update Existed Record
                                    SqlCommand updateSqlCommand = new SqlCommand();
                                    updateSqlCommand.Connection = gCnn;
                                    //if (VideoPath.Contains("\\\\"))
                                    //{
                                    //    VideoPath = VideoPath.Replace("\\\\", "\\");
                                    //    VideoPath = VideoPath.Replace("\\", @"\");
                                    //}
                                
                                    updateSqlCommand.Parameters.AddWithValue("@Path", VideoPath);
                                    updateSqlCommand.Parameters.AddWithValue("@Gender", Gender);
                                    updateSqlCommand.Parameters.AddWithValue("@EstimatedAge", Range);
                                    updateSqlCommand.Parameters.AddWithValue("@Ethnicity", Ethnicity);
                                    updateSqlCommand.Parameters.AddWithValue("@hasGlasses", hasGlasses);
                                    updateSqlCommand.Parameters.AddWithValue("@hasMoustache", hasMoustache);
                                    updateSqlCommand.Parameters.AddWithValue("@hasBeard", hasBeared);
                                    updateSqlCommand.Parameters.AddWithValue("@ImageQuality", ImageQuality);
                                    updateSqlCommand.Parameters.AddWithValue("@StateAnalysisPath", newStateLogName);
                                    updateSqlCommand.Parameters.AddWithValue("@DetailedAnalysisPath", newDetLogName);
                                    updateSqlCommand.CommandText = "UPDATE CORPUS.dbo.Video_Analysis " +
                                            "SET Gender=@Gender,EstimatedAge=@EstimatedAge," +
                                            "Ethnicity=@Ethnicity,hasGlasses=@hasGlasses,hasMoustache=@hasMoustache," +
                                            "hasBeard=@hasBeard,ImageQuality=@ImageQuality,StateAnalysis=@StateAnalysisPath ," +
                                                "DetailedAnalysis=@DetailedAnalysisPath   WHERE Path=@Path";
                             
                                    //SQL UPDATE Query Execution
                                    try
                                    {
                                        updateSqlCommand.ExecuteNonQuery();
                                    }

                                    catch (Exception ER)
                                    {
                                    }
                                }//Else 
                                

                            }//While End Of Stream

                    }//StreamReader

                }//Using FileStream

                //Delete old Logs
                //System.IO.File.Delete(detAnalysisFName);
                //System.IO.File.Delete(stateAnalysisFName);


            }//For Each Text File
             

                //Printing Total Checked Logs- Error Log Summary
                errorLog += "Total Checked Logs: " + LogsCounter+ System.Environment.NewLine;
                errorLog += "Total Logs With Errors: " + ErrorCounter+ System.Environment.NewLine;
                errorLog += "Total Inserted Logs: " + (LogsCounter - ErrorCounter);

                //If ther are no errors
                if (!(ErrorCounter>0))
                    MessageBox.Show(IDCounter + " New Videos and Analysis were inserted into MetaFace Corpus!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);

                //There are Errors
                else
                {   
                    MessageBox.Show("Process Completed - But There are problem in some detailed log files!"  , "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    String errorLogPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + "-ErrorLog #" + DateTime.Now.ToShortDateString().Replace('/', '.') + " " + DateTime.Now.ToShortTimeString().Replace(':','-')+".txt";
            
                    //Create New Error Log File
                    TextWriter tw = File.CreateText(errorLogPath);
                    tw.Close();
                    System.IO.File.WriteAllText(errorLogPath, errorLog);
                    System.Diagnostics.Process.Start("notepad.exe", errorLogPath);
                }//else
                    
            }//try
            
            //If Process has Errors Create & Open Error Log Text file
            catch (Exception ex)
            {
                MessageBox.Show("Process Stopped Because There are problem in some detailed log files!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Warning);
             
                String errorLogPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + "-ErrorLog #" + DateTime.Now.ToShortDateString().Replace('/', '.') + " " + DateTime.Now.ToShortTimeString().Replace(':', '-') + ".txt";

                //Create New Error Log File
                TextWriter tw = File.CreateText(errorLogPath);
                tw.Close();
                System.IO.File.WriteAllText(errorLogPath, errorLog);
                System.Diagnostics.Process.Start("notepad.exe", errorLogPath);
            }//catch
        }//getClassificationFromDetailedAnalysis


        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new accessForm());

            //Global Configuration
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

            //Global Settings
            KeyValueConfigurationCollection settings = config.AppSettings.Settings;

        }//Main
    }//Program
}//MetaFace