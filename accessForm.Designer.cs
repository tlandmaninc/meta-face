﻿namespace MetaFace
{
    partial class accessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userName = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.labPassword = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SettingsButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.Settings = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.materialRaisedButtonBack = new MaterialSkin.Controls.MaterialRaisedButton();
            this.DBFilesFolder = new System.Windows.Forms.GroupBox();
            this.uploadDBFolder = new System.Windows.Forms.Button();
            this.DBFtextBox = new System.Windows.Forms.TextBox();
            this.ApplyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.BTimeInterval = new System.Windows.Forms.DateTimePicker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ChooseBackFol = new System.Windows.Forms.Button();
            this.BFtextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SNtextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CNtextBox = new System.Windows.Forms.TextBox();
            this.BackupFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.DBFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1.SuspendLayout();
            this.Settings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.DBFilesFolder.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // userName
            // 
            this.userName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userName.Location = new System.Drawing.Point(181, 25);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(205, 31);
            this.userName.TabIndex = 2;
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.password.Location = new System.Drawing.Point(181, 87);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(205, 31);
            this.password.TabIndex = 3;
            this.password.UseSystemPasswordChar = true;
            // 
            // labPassword
            // 
            this.labPassword.AutoSize = true;
            this.labPassword.BackColor = System.Drawing.Color.Transparent;
            this.labPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labPassword.Location = new System.Drawing.Point(32, 87);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(108, 26);
            this.labPassword.TabIndex = 1;
            this.labPassword.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(32, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "User Name";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.labPassword);
            this.panel1.Controls.Add(this.password);
            this.panel1.Controls.Add(this.userName);
            this.panel1.Location = new System.Drawing.Point(164, 221);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(428, 150);
            this.panel1.TabIndex = 7;
            // 
            // SettingsButton
            // 
            this.SettingsButton.AutoSize = true;
            this.SettingsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SettingsButton.Depth = 0;
            this.SettingsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.SettingsButton.Icon = null;
            this.SettingsButton.Location = new System.Drawing.Point(590, 102);
            this.SettingsButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.SettingsButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Primary = false;
            this.SettingsButton.Size = new System.Drawing.Size(85, 36);
            this.SettingsButton.TabIndex = 8;
            this.SettingsButton.Text = "Settings";
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.materialFlatButton2_Click);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(318, 432);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(120, 36);
            this.materialRaisedButton1.TabIndex = 9;
            this.materialRaisedButton1.Text = "Enter System";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // Settings
            // 
            this.Settings.BackColor = System.Drawing.Color.White;
            this.Settings.Controls.Add(this.groupBox1);
            this.Settings.Location = new System.Drawing.Point(81, 102);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(594, 366);
            this.Settings.TabIndex = 11;
            this.Settings.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.materialRaisedButtonBack);
            this.groupBox1.Controls.Add(this.DBFilesFolder);
            this.groupBox1.Controls.Add(this.ApplyButton);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(19, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(556, 291);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // materialRaisedButtonBack
            // 
            this.materialRaisedButtonBack.AutoSize = true;
            this.materialRaisedButtonBack.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButtonBack.Depth = 0;
            this.materialRaisedButtonBack.Icon = null;
            this.materialRaisedButtonBack.Location = new System.Drawing.Point(43, 236);
            this.materialRaisedButtonBack.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButtonBack.Name = "materialRaisedButtonBack";
            this.materialRaisedButtonBack.Primary = true;
            this.materialRaisedButtonBack.Size = new System.Drawing.Size(56, 36);
            this.materialRaisedButtonBack.TabIndex = 13;
            this.materialRaisedButtonBack.Text = "Back";
            this.materialRaisedButtonBack.UseVisualStyleBackColor = true;
            this.materialRaisedButtonBack.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // DBFilesFolder
            // 
            this.DBFilesFolder.Controls.Add(this.uploadDBFolder);
            this.DBFilesFolder.Controls.Add(this.DBFtextBox);
            this.DBFilesFolder.Location = new System.Drawing.Point(319, 99);
            this.DBFilesFolder.Name = "DBFilesFolder";
            this.DBFilesFolder.Size = new System.Drawing.Size(190, 53);
            this.DBFilesFolder.TabIndex = 3;
            this.DBFilesFolder.TabStop = false;
            this.DBFilesFolder.Text = "DB Folder";
            // 
            // uploadDBFolder
            // 
            this.uploadDBFolder.Location = new System.Drawing.Point(142, 18);
            this.uploadDBFolder.Name = "uploadDBFolder";
            this.uploadDBFolder.Size = new System.Drawing.Size(37, 21);
            this.uploadDBFolder.TabIndex = 2;
            this.uploadDBFolder.Text = "...";
            this.uploadDBFolder.UseVisualStyleBackColor = true;
            this.uploadDBFolder.Click += new System.EventHandler(this.uploadDBFolder_Click);
            // 
            // DBFtextBox
            // 
            this.DBFtextBox.Location = new System.Drawing.Point(7, 19);
            this.DBFtextBox.Name = "DBFtextBox";
            this.DBFtextBox.Size = new System.Drawing.Size(133, 20);
            this.DBFtextBox.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.AutoSize = true;
            this.ApplyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ApplyButton.Depth = 0;
            this.ApplyButton.Icon = null;
            this.ApplyButton.Location = new System.Drawing.Point(446, 236);
            this.ApplyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Primary = true;
            this.ApplyButton.Size = new System.Drawing.Size(63, 36);
            this.ApplyButton.TabIndex = 12;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click_1);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.BTimeInterval);
            this.groupBox5.Location = new System.Drawing.Point(43, 161);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(190, 53);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Scheduled Backup Time";
            // 
            // BTimeInterval
            // 
            this.BTimeInterval.CustomFormat = "HH:mm tt";
            this.BTimeInterval.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.BTimeInterval.Location = new System.Drawing.Point(10, 19);
            this.BTimeInterval.Name = "BTimeInterval";
            this.BTimeInterval.Size = new System.Drawing.Size(112, 20);
            this.BTimeInterval.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ChooseBackFol);
            this.groupBox4.Controls.Add(this.BFtextBox);
            this.groupBox4.Location = new System.Drawing.Point(43, 99);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(190, 53);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "BackupFolder";
            // 
            // ChooseBackFol
            // 
            this.ChooseBackFol.Location = new System.Drawing.Point(142, 18);
            this.ChooseBackFol.Name = "ChooseBackFol";
            this.ChooseBackFol.Size = new System.Drawing.Size(37, 21);
            this.ChooseBackFol.TabIndex = 2;
            this.ChooseBackFol.Text = "...";
            this.ChooseBackFol.UseVisualStyleBackColor = true;
            this.ChooseBackFol.Click += new System.EventHandler(this.button1_Click);
            // 
            // BFtextBox
            // 
            this.BFtextBox.Location = new System.Drawing.Point(7, 19);
            this.BFtextBox.Name = "BFtextBox";
            this.BFtextBox.Size = new System.Drawing.Size(133, 20);
            this.BFtextBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SNtextBox);
            this.groupBox3.Location = new System.Drawing.Point(319, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(190, 53);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Server Name";
            // 
            // SNtextBox
            // 
            this.SNtextBox.Location = new System.Drawing.Point(7, 18);
            this.SNtextBox.Name = "SNtextBox";
            this.SNtextBox.Size = new System.Drawing.Size(165, 20);
            this.SNtextBox.TabIndex = 0;
            this.SNtextBox.TextChanged += new System.EventHandler(this.SNtextBox_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CNtextBox);
            this.groupBox2.Location = new System.Drawing.Point(43, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(190, 53);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Computer Name";
            // 
            // CNtextBox
            // 
            this.CNtextBox.Location = new System.Drawing.Point(7, 18);
            this.CNtextBox.Name = "CNtextBox";
            this.CNtextBox.Size = new System.Drawing.Size(165, 20);
            this.CNtextBox.TabIndex = 0;
            // 
            // BackupFolder
            // 
            this.BackupFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // DBFolder
            // 
            this.DBFolder.SelectedPath = "\\\\132.72.160.213\\workingdir\\workingdir\\carmel\\MetaFace\\DB";
            // 
            // accessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(750, 518);
            this.Controls.Add(this.Settings);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.SettingsButton);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "accessForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MetaFace Access";
            this.Load += new System.EventHandler(this.accessForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Settings.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.DBFilesFolder.ResumeLayout(false);
            this.DBFilesFolder.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialFlatButton SettingsButton;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.Panel Settings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox SNtextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox CNtextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox BFtextBox;
        private System.Windows.Forms.Button ChooseBackFol;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker BTimeInterval;
        private System.Windows.Forms.GroupBox DBFilesFolder;
        private System.Windows.Forms.Button uploadDBFolder;
        private System.Windows.Forms.TextBox DBFtextBox;
        private System.Windows.Forms.FolderBrowserDialog DBFolder;
        private MaterialSkin.Controls.MaterialRaisedButton ApplyButton;
        public System.Windows.Forms.FolderBrowserDialog BackupFolder;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButtonBack;
    }
}