﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using Shell32;
using System.IO;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Collections;
using System.Configuration.Assemblies;
using System.Configuration;
using System.Globalization;
using System.Collections.Specialized;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
namespace MetaFace
{
    //Access Form 
    public partial class accessForm : MaterialForm
    {
        //accessForm Constructor
        public accessForm()
        {
            InitializeComponent();
            this.Sizable = false;
            Settings.Hide();
            materialRaisedButtonBack.AutoSize = false;
            materialRaisedButtonBack.Size = new System.Drawing.Size(63, 36);
            //All Settings TextBoxes Array
            TextBox[] TextBoxArray = { CNtextBox, SNtextBox, BFtextBox, DBFtextBox };

            //Global Settings Collection
            KeyValueConfigurationCollection settings = Program.getAccessParameters();

            //SHow Default values on all text box
            CNtextBox.AppendText(Program.getDefaultComputerName());
            SNtextBox.AppendText(Program.getDefaultServerName());
            BFtextBox.AppendText(Program.getDefaultBackupFolderPath());
            DBFtextBox.AppendText(Program.getDefaultDatabeseFolderPath());

            // Global Hours - Backup
            String GlobalHour = Program.getDefaultBackupTimeInterval().Split(':')[0];

            //Global Minutes - Backup
            String GlobalMinute = Program.getDefaultBackupTimeInterval().Split(':')[1];
            BTimeInterval.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
            Int32.Parse(GlobalHour), Int32.Parse(GlobalMinute), 0);
        }//accessForm Constructor

        //Clicking on Settings Button
        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            this.Settings.Show();
        }//materialFlatButton2_Click

        //start Backup based on User Time Interval [Settings]
        private void startBackup()
        {
            String BackupTime = BTimeInterval.Value.ToString().Split(' ')[1];
            int HH = Int32.Parse(BackupTime.Split(':')[0]);
            int MM = Int32.Parse(BackupTime.Split(':')[1]);
            Program.updateBackupSchedule(HH, MM);
        }//startBackup

        //Clicking on Apply Button - Check new Settings & Update Accordingly
        private void ApplyButton_Click_1(object sender, EventArgs e)
        {
            //If New Settings are Legal - Set New Global Variables for the next Operations
            if (CheckSettings())
            {
                String BackupTime = BTimeInterval.Value.ToString().Split(' ')[1];
                Program.setAccessParameters(CNtextBox.Text, SNtextBox.Text, BFtextBox.Text, DBFtextBox.Text, BackupTime, "IP", "Port");
                this.Settings.Hide();
            }//if Settings are valid 
        }//ApplyButton_Click

        //Upload Backup Folder Button
        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog BackupFolder = new FolderBrowserDialog();
            BackupFolder.ShowDialog();
            this.BFtextBox.Clear();
            this.BFtextBox.AppendText(BackupFolder.SelectedPath);
        }//button1_Click

        //Upload DB Folder Button
        private void uploadDBFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog DBFilesFolder = new FolderBrowserDialog();
            DBFilesFolder.ShowDialog();
            this.DBFtextBox.Clear();
            this.DBFtextBox.AppendText(DBFilesFolder.SelectedPath);
        }//uploadDBFolder_Click

        //Clicking on Enter System Button
        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            //If Connection Created successfully
            if (Program.connectToServer(userName.Text, password.Text))
            {
                mainForm MF = new mainForm();
                MF.Show();
                this.Hide();
                startBackup();
            }//if           
        }//Enter System Button

        //Checks If IP is Correct
        private Boolean CheckIPAddress(string server)
        {
            IPAddress address;
            if (IPAddress.TryParse(server, out address))
            {
                try
                {
                    System.Text.ASCIIEncoding ASCII = new System.Text.ASCIIEncoding();
                    // Get server related information.
                    IPHostEntry heserver = Dns.GetHostEntry(server);
                    return true;
                }//try
                catch (Exception e)
                {
                    return false;
                }//catch
            }//If
            else return false;
        }//CheckIPAddress

        //All Settings Validation Indication & Error Message
        private Boolean CheckSettings()
        {
            int ErrorCounter = 0;
            String ErrorString = "Error List: " + System.Environment.NewLine;

            //Checks If Backup Folder Exist
            if (!Directory.Exists(BFtextBox.Text))
            {
                ErrorCounter++;
                ErrorString += ErrorCounter + "- Backup Folder isn't Exist! " + System.Environment.NewLine;

            }//Backup Folder Validation check

            //Checks If Database Folder Exist
            if (!Directory.Exists(DBFtextBox.Text))
            {
                ErrorCounter++;
                ErrorString += ErrorCounter + "- Database Folder isn't Exist!  " + System.Environment.NewLine;
            }//DB Folder Validation check

          
            //Now TimeStamp
            DateTime rightNow = DateTime.Now;

            // Sets the timer interval based on User Backup Time Set
            TimeSpan diff = BTimeInterval.Value - rightNow;


            int totalMS = diff.Hours * 3600000 + diff.Minutes * 60000;

            //Check if time inserted is valid
            if (totalMS < 0)
            {
                ErrorCounter++;
                ErrorString += ErrorCounter + "- Please insert Futured Time Only!  " + System.Environment.NewLine;
            }//if


            //Database Connection validation
            String testConnetionString = "";
            if (CNtextBox.Text.Contains(','))
                 testConnetionString = "Data Source=" + Program.getDefaultComputerName() + @"\" + Program.getDefaultServerName() + "; Initial Catalog=CORPUS;Integrated Security=True";
            else 
                testConnetionString = "Data Source=" + CNtextBox.Text+",1433" + @"\" + SNtextBox.Text + "; Initial Catalog=CORPUS;Integrated Security=True";
         
            SqlConnection cnn = new SqlConnection(testConnetionString);
            try
            {
                cnn.Open();
                cnn.Close();
            }//try connection

            catch (Exception ex)
            {
                ErrorCounter++;
                ErrorString += ErrorCounter + "- Can't Create Connection to DB! Check Computer & Server Name! " + System.Environment.NewLine;
            }//catch


            //Checks If Database Folder Exist
            if (ErrorCounter > 0)
            {
                MessageBox.Show(ErrorString, "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }//DB Folder Validation check

            else return true;           
        }//CheckSettings

        //Clicking "Back" Button on Settings
        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            Settings.Hide();
        }//materialRaisedButton2_Click

        private void accessForm_Load(object sender, EventArgs e)
        {

        }

        private void SNtextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }//AccessForm
}//MetaFace

