﻿namespace MetaFace
{
    partial class editDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ETHNICITY = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GENDER = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.AGE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.HASGLASSES = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.HASMOUSTACHE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.HADBEARED = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SUB_DATABASE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DURATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCLUDE_VOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.butPrev1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.butDelete1 = new MaterialSkin.Controls.MaterialRaisedButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.NAME,
            this.ETHNICITY,
            this.GENDER,
            this.AGE,
            this.HASGLASSES,
            this.HASMOUSTACHE,
            this.HADBEARED,
            this.SUB_DATABASE,
            this.DURATION,
            this.INCLUDE_VOICE,
            this.FORMAT});
            this.dataGridView1.Location = new System.Drawing.Point(38, 101);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(924, 514);
            this.dataGridView1.TabIndex = 7;
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ID.DataPropertyName = "VideoID";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ID.Width = 43;
            // 
            // NAME
            // 
            this.NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.NAME.DataPropertyName = "Name";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.NAME.DefaultCellStyle = dataGridViewCellStyle2;
            this.NAME.HeaderText = "Name                                  ";
            this.NAME.Name = "NAME";
            this.NAME.ReadOnly = true;
            this.NAME.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NAME.Width = 99;
            // 
            // ETHNICITY
            // 
            this.ETHNICITY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ETHNICITY.DataPropertyName = "Ethnicity";
            this.ETHNICITY.HeaderText = "Ethnicity";
            this.ETHNICITY.Items.AddRange(new object[] {
            "Unknown",
            "SouthAsian",
            "African",
            "Caucasian",
            "EasternAsian",
            "Other"});
            this.ETHNICITY.Name = "ETHNICITY";
            this.ETHNICITY.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ETHNICITY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ETHNICITY.Width = 72;
            // 
            // GENDER
            // 
            this.GENDER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.GENDER.DataPropertyName = "Gender";
            this.GENDER.HeaderText = "Gender";
            this.GENDER.Items.AddRange(new object[] {
            "Male",
            "Female",
            "FIND_FAILED",
            "FIT_FAILED",
            "NULL"});
            this.GENDER.Name = "GENDER";
            this.GENDER.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GENDER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.GENDER.Width = 67;
            // 
            // AGE
            // 
            this.AGE.DataPropertyName = "EstimatedAge";
            this.AGE.HeaderText = "Estimated Age";
            this.AGE.Items.AddRange(new object[] {
            "0 - 10",
            "10 - 20",
            "20 - 30",
            "30 - 40",
            "40 - 50",
            "50 - 60",
            "60 - 70",
            "70 - 80",
            "Over 80",
            "5 - 15",
            "15 - 25",
            "25 - 35",
            "35 - 45",
            "45 - 55",
            "55 - 65",
            "65 - 75",
            "FIT_FAILED",
            "FIND_FAILED",
            "NULL"});
            this.AGE.Name = "AGE";
            this.AGE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AGE.Width = 80;
            // 
            // HASGLASSES
            // 
            this.HASGLASSES.DataPropertyName = "HasGlasses";
            this.HASGLASSES.HeaderText = "Has Glasses?";
            this.HASGLASSES.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.HASGLASSES.Name = "HASGLASSES";
            this.HASGLASSES.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HASGLASSES.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.HASGLASSES.Width = 80;
            // 
            // HASMOUSTACHE
            // 
            this.HASMOUSTACHE.DataPropertyName = "HasMoustache";
            this.HASMOUSTACHE.HeaderText = "Has Moustache?";
            this.HASMOUSTACHE.Items.AddRange(new object[] {
            "None",
            "Some",
            "Heavy"});
            this.HASMOUSTACHE.Name = "HASMOUSTACHE";
            this.HASMOUSTACHE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HASMOUSTACHE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.HASMOUSTACHE.Width = 80;
            // 
            // HADBEARED
            // 
            this.HADBEARED.DataPropertyName = "hasBeard";
            this.HADBEARED.HeaderText = "Has Beared?";
            this.HADBEARED.Items.AddRange(new object[] {
            "None",
            "Some",
            "Heavy"});
            this.HADBEARED.Name = "HADBEARED";
            this.HADBEARED.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // SUB_DATABASE
            // 
            this.SUB_DATABASE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SUB_DATABASE.DataPropertyName = "FromSubDB";
            this.SUB_DATABASE.HeaderText = "From Sub DB";
            this.SUB_DATABASE.Name = "SUB_DATABASE";
            this.SUB_DATABASE.ReadOnly = true;
            this.SUB_DATABASE.Width = 74;
            // 
            // DURATION
            // 
            this.DURATION.DataPropertyName = "Duration";
            this.DURATION.HeaderText = "Duration";
            this.DURATION.Name = "DURATION";
            this.DURATION.ReadOnly = true;
            this.DURATION.Width = 60;
            // 
            // INCLUDE_VOICE
            // 
            this.INCLUDE_VOICE.DataPropertyName = "HasVoice";
            this.INCLUDE_VOICE.HeaderText = "Has Voice?";
            this.INCLUDE_VOICE.Name = "INCLUDE_VOICE";
            this.INCLUDE_VOICE.ReadOnly = true;
            this.INCLUDE_VOICE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.INCLUDE_VOICE.Width = 80;
            // 
            // FORMAT
            // 
            this.FORMAT.DataPropertyName = "Format";
            this.FORMAT.HeaderText = "Format";
            this.FORMAT.Name = "FORMAT";
            this.FORMAT.ReadOnly = true;
            this.FORMAT.Width = 60;
            // 
            // butPrev1
            // 
            this.butPrev1.AutoSize = true;
            this.butPrev1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.butPrev1.Depth = 0;
            this.butPrev1.Icon = null;
            this.butPrev1.Location = new System.Drawing.Point(38, 706);
            this.butPrev1.MouseState = MaterialSkin.MouseState.HOVER;
            this.butPrev1.Name = "butPrev1";
            this.butPrev1.Primary = true;
            this.butPrev1.Size = new System.Drawing.Size(86, 36);
            this.butPrev1.TabIndex = 8;
            this.butPrev1.Text = "Previous";
            this.butPrev1.UseVisualStyleBackColor = true;
            this.butPrev1.Click += new System.EventHandler(this.butPrev1_Click);
            // 
            // butDelete1
            // 
            this.butDelete1.AutoSize = true;
            this.butDelete1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.butDelete1.Depth = 0;
            this.butDelete1.Icon = null;
            this.butDelete1.Location = new System.Drawing.Point(38, 634);
            this.butDelete1.MouseState = MaterialSkin.MouseState.HOVER;
            this.butDelete1.Name = "butDelete1";
            this.butDelete1.Primary = true;
            this.butDelete1.Size = new System.Drawing.Size(69, 36);
            this.butDelete1.TabIndex = 9;
            this.butDelete1.Text = "Delete";
            this.butDelete1.UseVisualStyleBackColor = true;
            this.butDelete1.Click += new System.EventHandler(this.butDelete1_Click);
            // 
            // editDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MetaFace.Properties.Resources.pic1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1000, 800);
            this.Controls.Add(this.butDelete1);
            this.Controls.Add(this.butPrev1);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "editDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Data";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private MaterialSkin.Controls.MaterialRaisedButton butPrev1;
        private MaterialSkin.Controls.MaterialRaisedButton butDelete1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NAME;
        private System.Windows.Forms.DataGridViewComboBoxColumn ETHNICITY;
        private System.Windows.Forms.DataGridViewComboBoxColumn GENDER;
        private System.Windows.Forms.DataGridViewComboBoxColumn AGE;
        private System.Windows.Forms.DataGridViewComboBoxColumn HASGLASSES;
        private System.Windows.Forms.DataGridViewComboBoxColumn HASMOUSTACHE;
        private System.Windows.Forms.DataGridViewComboBoxColumn HADBEARED;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUB_DATABASE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DURATION;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCLUDE_VOICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FORMAT;
    }
}