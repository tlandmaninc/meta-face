﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using System.IO;


namespace MetaFace
{
    public partial class editDataForm : MaterialForm
    {
        private DataSet data;

        //defult form Constructor
        public editDataForm()
        {
            InitializeComponent();
            this.Sizable = false;
        }

        //form Constructor - create the form and hide columns that are not eligible for editing
        public editDataForm(DataSet ds)
        {
            InitializeComponent();
            this.Sizable = false;
            this.data = ds;
            dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.AllowUserToResizeColumns = true;

            for (int i = 2; i < 10; i++)
            {
                dataGridView1.Columns[i].Visible = false;
            }
            for (int i = 16; i < 31; i++)
            {
                dataGridView1.Columns[i].Visible = false;
            }
            for (int i = 10; i < 16; i++)
            {
                dataGridView1.Columns[i].Width = 140;
            }
        }//editDataForm

        

        private void butPrev1_Click(object sender, EventArgs e)
        {
            retriveDataFilterForm retriveDataFilterForm1 = new retriveDataFilterForm(false);
            retriveDataFilterForm1.Show();
            this.Close();
        }

        private void butDelete1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to permanently delete those videos?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) 
            {
                deleteRows();
            }
            else
            {
                MessageBox.Show("Action were canceled");
            }          
        }//butDelete
        
        private void deleteRows()
        {
            // executeQuery(buildDeleteQuery(findSelectedRows()));
            ArrayList videoIDIndexToDelete = findSelectedRowsValues("ID");
            ArrayList videoFilesToDelete = findSelectedRowsValues("Path");
            ArrayList analysisFilesToDelete = findSelectedRowsValues("DetailedAnalysis");
            
            String deleteQuery = buildDeleteQuery(videoIDIndexToDelete);
            if (deleteQuery != "userDidntChooseRowsToDelete")
            {
                executeQuery(deleteQuery);
                removeRowsFromDataset();
                //deleteFile(videoFilesToDelete, analysisFilesToDelete);///disabled- need Carmel inputs if he want this functionalty
                MessageBox.Show("Selected rows were deleted");
            }
            else {MessageBox.Show("Please selected rows to delete");}
            // dataGridView1.Refresh();
        }//deleteRows

        // Delete the Video file from the computer
        //    private void deleteFile(ArrayList videoFilesPath, ArrayList analysisFilePath)
        //    {
        //foreach (string file in videoFilesPath) {
        //            File.Delete(file);                                                       ///disabled just for testing- need to conslut with carmel if he want this functionalty
        //        }
        //        foreach (string file in analysisFilePath)
        //        {
        //            File.Delete(file);
        //        }
        //    }

        //return values from all the rows the user select (for a specific column)
        private ArrayList findSelectedRowsValues(String columnName)
        {
            ArrayList SelectedRowsValueStr = new ArrayList();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {               
                String ValueStr = row.Cells[columnName].Value.ToString();
                SelectedRowsValueStr.Add(ValueStr);
            }//foreach
            return SelectedRowsValueStr;
        }//findSelectedVideoID
        
        //Remove the selected rows from the dataset and datagridview
        private void removeRowsFromDataset()
        {
            ArrayList SelectedRowsStr = new ArrayList();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                this.data.Tables[0].Rows[Convert.ToInt16(row.Index)].Delete();
                }//foreach
        }//removeRowsFromDataset
        // Get list of videoID that need to be deleted from the DB, and build SQL query accordenly 
        private String buildDeleteQuery(ArrayList SelectedVideoIDStr)
        {
            String deleteQuery = "DELETE FROM  CORPUS.DBO.Video_Analysis WHERE ";
            String deleteSubQuery = "";
            if (SelectedVideoIDStr.Count != 0)
            {
                foreach (string i in SelectedVideoIDStr)
                {
                    deleteSubQuery = deleteSubQuery + i + " , ";
                }//foreach
                deleteSubQuery = deleteSubQuery.Substring(0, deleteSubQuery.Length - 2);
                deleteQuery += " VideoID IN (" + deleteSubQuery + ")";
                return deleteQuery;
            }
            else { return "userDidntChooseRowsToDelete"; }
        }//buildDeleteQuery

        // Get query as string and excute the query
        private void executeQuery(String Query)
        {
            if (Query != "userDidntChooseRowsToDelete")
            {
                SqlCommand updateDBSqlCommand = new SqlCommand();
                updateDBSqlCommand.Connection = Program.getgCnn();
                updateDBSqlCommand.CommandText = Query;
                try
                {
                    updateDBSqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    //Exception ex;
                }
            }
            else { }
        }//executeQuery

        //Identefied that the user edit the data
        protected override void OnLoad(EventArgs e)
        {
            dataGridView1.CellValueChanged += new DataGridViewCellEventHandler(dataGridView1_CellValueChanged);
        }//OnLoad

        
        //Extract the user changes and update the DB accordinly 
        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            String editedRowVideoID = Convert.ToString(dataGridView1[0, e.RowIndex].Value);
            String editedCol = dataGridView1.Columns[e.ColumnIndex].Name;
            String newValue = dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString();
            String updateDBQuery = buildEditQuery(editedRowVideoID, editedCol, newValue);
            executeQuery(updateDBQuery);
        }//dataGridView1_CellValueChanged

        //Build string that represent the edit query
        private String buildEditQuery(String editedRowVideoID, String editedCol, String newValue)
        {
            String EditQuery = " UPDATE Video_analysis SET " + editedCol+" = '"+ newValue + "' where VideoID = '"+ editedRowVideoID+"' ";
            return EditQuery;
        }//buildEditQuery
    }
}

