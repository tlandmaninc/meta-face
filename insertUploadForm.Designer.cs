﻿namespace MetaFace
{
    partial class insertUploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxUploadVideos = new System.Windows.Forms.TextBox();
            this.materialRadioButtonInsertOnlyVideos = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRadioButtonInsertVideosAndAnalysis = new MaterialSkin.Controls.MaterialRadioButton();
            this.textBoxUploadAnalysis = new System.Windows.Forms.TextBox();
            this.materialRaisedButtonUploadVideos = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButtonUploadAnalysis = new MaterialSkin.Controls.MaterialRaisedButton();
            this.folderBrowserDialogUploadVideos = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserDialogUploadAnalysis = new System.Windows.Forms.FolderBrowserDialog();
            this.materialRaisedButtonInsertFiles = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButtonPrevious = new MaterialSkin.Controls.MaterialRaisedButton();
            this.uploadVideosGroupBox = new System.Windows.Forms.GroupBox();
            this.uploadVideosGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            // 
            // textBoxUploadVideos
            // 
            this.textBoxUploadVideos.AllowDrop = true;
            this.textBoxUploadVideos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBoxUploadVideos.Location = new System.Drawing.Point(160, 31);
            this.textBoxUploadVideos.Name = "textBoxUploadVideos";
            this.textBoxUploadVideos.Size = new System.Drawing.Size(472, 26);
            this.textBoxUploadVideos.TabIndex = 15;
            // 
            // materialRadioButtonInsertOnlyVideos
            // 
            this.materialRadioButtonInsertOnlyVideos.AutoSize = true;
            this.materialRadioButtonInsertOnlyVideos.BackColor = System.Drawing.Color.White;
            this.materialRadioButtonInsertOnlyVideos.Checked = true;
            this.materialRadioButtonInsertOnlyVideos.Depth = 0;
            this.materialRadioButtonInsertOnlyVideos.Font = new System.Drawing.Font("Roboto", 10F);
            this.materialRadioButtonInsertOnlyVideos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.materialRadioButtonInsertOnlyVideos.Location = new System.Drawing.Point(647, 31);
            this.materialRadioButtonInsertOnlyVideos.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButtonInsertOnlyVideos.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButtonInsertOnlyVideos.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButtonInsertOnlyVideos.Name = "materialRadioButtonInsertOnlyVideos";
            this.materialRadioButtonInsertOnlyVideos.Ripple = true;
            this.materialRadioButtonInsertOnlyVideos.Size = new System.Drawing.Size(102, 30);
            this.materialRadioButtonInsertOnlyVideos.TabIndex = 17;
            this.materialRadioButtonInsertOnlyVideos.TabStop = true;
            this.materialRadioButtonInsertOnlyVideos.Text = "Videos Only ";
            this.materialRadioButtonInsertOnlyVideos.UseVisualStyleBackColor = false;
            this.materialRadioButtonInsertOnlyVideos.CheckedChanged += new System.EventHandler(this.materialRadioButtonInsertOnlyVideos_CheckedChanged);
            // 
            // materialRadioButtonInsertVideosAndAnalysis
            // 
            this.materialRadioButtonInsertVideosAndAnalysis.AutoSize = true;
            this.materialRadioButtonInsertVideosAndAnalysis.BackColor = System.Drawing.Color.White;
            this.materialRadioButtonInsertVideosAndAnalysis.Depth = 0;
            this.materialRadioButtonInsertVideosAndAnalysis.Font = new System.Drawing.Font("Roboto", 10F);
            this.materialRadioButtonInsertVideosAndAnalysis.Location = new System.Drawing.Point(647, 81);
            this.materialRadioButtonInsertVideosAndAnalysis.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButtonInsertVideosAndAnalysis.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButtonInsertVideosAndAnalysis.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButtonInsertVideosAndAnalysis.Name = "materialRadioButtonInsertVideosAndAnalysis";
            this.materialRadioButtonInsertVideosAndAnalysis.Ripple = true;
            this.materialRadioButtonInsertVideosAndAnalysis.Size = new System.Drawing.Size(139, 30);
            this.materialRadioButtonInsertVideosAndAnalysis.TabIndex = 18;
            this.materialRadioButtonInsertVideosAndAnalysis.TabStop = true;
            this.materialRadioButtonInsertVideosAndAnalysis.Text = "Videos & Analysis";
            this.materialRadioButtonInsertVideosAndAnalysis.UseVisualStyleBackColor = false;
            this.materialRadioButtonInsertVideosAndAnalysis.CheckedChanged += new System.EventHandler(this.materialRadioButtonInsertVideosAndAnalysis_CheckedChanged);
            // 
            // textBoxUploadAnalysis
            // 
            this.textBoxUploadAnalysis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBoxUploadAnalysis.Location = new System.Drawing.Point(160, 82);
            this.textBoxUploadAnalysis.Name = "textBoxUploadAnalysis";
            this.textBoxUploadAnalysis.Size = new System.Drawing.Size(472, 26);
            this.textBoxUploadAnalysis.TabIndex = 19;
            // 
            // materialRaisedButtonUploadVideos
            // 
            this.materialRaisedButtonUploadVideos.AutoSize = true;
            this.materialRaisedButtonUploadVideos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButtonUploadVideos.Depth = 0;
            this.materialRaisedButtonUploadVideos.Icon = null;
            this.materialRaisedButtonUploadVideos.Location = new System.Drawing.Point(6, 25);
            this.materialRaisedButtonUploadVideos.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButtonUploadVideos.Name = "materialRaisedButtonUploadVideos";
            this.materialRaisedButtonUploadVideos.Primary = true;
            this.materialRaisedButtonUploadVideos.Size = new System.Drawing.Size(124, 36);
            this.materialRaisedButtonUploadVideos.TabIndex = 21;
            this.materialRaisedButtonUploadVideos.Text = "Upload Videos";
            this.materialRaisedButtonUploadVideos.UseVisualStyleBackColor = true;
            this.materialRaisedButtonUploadVideos.Click += new System.EventHandler(this.materialRaisedButtonUploadVideos_Click);
            // 
            // materialRaisedButtonUploadAnalysis
            // 
            this.materialRaisedButtonUploadAnalysis.AutoSize = true;
            this.materialRaisedButtonUploadAnalysis.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButtonUploadAnalysis.Depth = 0;
            this.materialRaisedButtonUploadAnalysis.Icon = null;
            this.materialRaisedButtonUploadAnalysis.Location = new System.Drawing.Point(6, 77);
            this.materialRaisedButtonUploadAnalysis.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButtonUploadAnalysis.Name = "materialRaisedButtonUploadAnalysis";
            this.materialRaisedButtonUploadAnalysis.Primary = true;
            this.materialRaisedButtonUploadAnalysis.Size = new System.Drawing.Size(141, 36);
            this.materialRaisedButtonUploadAnalysis.TabIndex = 22;
            this.materialRaisedButtonUploadAnalysis.Text = "Upload Analysis";
            this.materialRaisedButtonUploadAnalysis.UseVisualStyleBackColor = true;
            this.materialRaisedButtonUploadAnalysis.Click += new System.EventHandler(this.materialRaisedButtonUploadAnalysis_Click);
            // 
            // materialRaisedButtonInsertFiles
            // 
            this.materialRaisedButtonInsertFiles.AutoSize = true;
            this.materialRaisedButtonInsertFiles.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButtonInsertFiles.Depth = 0;
            this.materialRaisedButtonInsertFiles.Icon = null;
            this.materialRaisedButtonInsertFiles.Location = new System.Drawing.Point(728, 564);
            this.materialRaisedButtonInsertFiles.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButtonInsertFiles.Name = "materialRaisedButtonInsertFiles";
            this.materialRaisedButtonInsertFiles.Primary = true;
            this.materialRaisedButtonInsertFiles.Size = new System.Drawing.Size(106, 36);
            this.materialRaisedButtonInsertFiles.TabIndex = 23;
            this.materialRaisedButtonInsertFiles.Text = "Insert Files";
            this.materialRaisedButtonInsertFiles.UseVisualStyleBackColor = true;
            this.materialRaisedButtonInsertFiles.Click += new System.EventHandler(this.materialRaisedButtonInsertFiles_Click);
            // 
            // materialRaisedButtonPrevious
            // 
            this.materialRaisedButtonPrevious.AutoSize = true;
            this.materialRaisedButtonPrevious.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButtonPrevious.Depth = 0;
            this.materialRaisedButtonPrevious.Icon = null;
            this.materialRaisedButtonPrevious.Location = new System.Drawing.Point(25, 564);
            this.materialRaisedButtonPrevious.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButtonPrevious.Name = "materialRaisedButtonPrevious";
            this.materialRaisedButtonPrevious.Primary = true;
            this.materialRaisedButtonPrevious.Size = new System.Drawing.Size(86, 36);
            this.materialRaisedButtonPrevious.TabIndex = 24;
            this.materialRaisedButtonPrevious.Text = "Previous";
            this.materialRaisedButtonPrevious.UseVisualStyleBackColor = true;
            this.materialRaisedButtonPrevious.Click += new System.EventHandler(this.materialRaisedButtonPrevious_Click);
            // 
            // uploadVideosGroupBox
            // 
            this.uploadVideosGroupBox.BackColor = System.Drawing.Color.White;
            this.uploadVideosGroupBox.Controls.Add(this.textBoxUploadVideos);
            this.uploadVideosGroupBox.Controls.Add(this.materialRaisedButtonUploadVideos);
            this.uploadVideosGroupBox.Controls.Add(this.materialRadioButtonInsertOnlyVideos);
            this.uploadVideosGroupBox.Controls.Add(this.materialRadioButtonInsertVideosAndAnalysis);
            this.uploadVideosGroupBox.Controls.Add(this.textBoxUploadAnalysis);
            this.uploadVideosGroupBox.Controls.Add(this.materialRaisedButtonUploadAnalysis);
            this.uploadVideosGroupBox.Location = new System.Drawing.Point(25, 365);
            this.uploadVideosGroupBox.Name = "uploadVideosGroupBox";
            this.uploadVideosGroupBox.Size = new System.Drawing.Size(809, 130);
            this.uploadVideosGroupBox.TabIndex = 27;
            this.uploadVideosGroupBox.TabStop = false;
            this.uploadVideosGroupBox.Text = "Upload Options";
            // 
            // insertUploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MetaFace.Properties.Resources.pic1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(854, 633);
            this.Controls.Add(this.uploadVideosGroupBox);
            this.Controls.Add(this.materialRaisedButtonPrevious);
            this.Controls.Add(this.materialRaisedButtonInsertFiles);
            this.MaximizeBox = false;
            this.Name = "insertUploadForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selected Videos";
            this.uploadVideosGroupBox.ResumeLayout(false);
            this.uploadVideosGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButtonInsertOnlyVideos;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButtonInsertVideosAndAnalysis;
        private System.Windows.Forms.TextBox textBoxUploadAnalysis;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButtonUploadVideos;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButtonUploadAnalysis;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogUploadVideos;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogUploadAnalysis;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButtonInsertFiles;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButtonPrevious;
        private System.Windows.Forms.GroupBox uploadVideosGroupBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        public System.Windows.Forms.TextBox textBoxUploadVideos;
    }
}