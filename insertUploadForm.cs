﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using System.IO;
using Shell32;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Collections;

namespace MetaFace

{   //insertUploadForm
    public partial class insertUploadForm : MaterialForm
    {
        private String uploadVideosFolder;
        private String uploadAnalysisFolder;
        public static ProgressBar insertProgressBar;
        public static System.Windows.Forms.ListView insertVideosListView;

        ////insertUploadForm Constructor
        public insertUploadForm()
        {
            //Progress Bar Init
            insertProgressBar = new ProgressBar();
            insertProgressBar.Hide();
            insertProgressBar.Location = new System.Drawing.Point(25, 512);
            insertProgressBar.Name = "insertProgressBar";
            insertProgressBar.Size = new System.Drawing.Size(805, 31);
            insertProgressBar.TabIndex = 26;
            this.Sizable = false;
            this.Controls.Add(insertProgressBar);

            //Insert List View - Name-Path-Size-Format-hasVoice-Duration- Resolution-Frame Rate
            insertVideosListView = new System.Windows.Forms.ListView();
            insertVideosListView.Columns.Add("Name",120, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Path", 300, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Size [MB]", 70, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Format", 60, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Has Voice?", 90, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Duration", 70, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Resolution", 80, HorizontalAlignment.Center);
            insertVideosListView.Columns.Add("Frame Rate", 80, HorizontalAlignment.Center);
            insertVideosListView.Columns[1].DisplayIndex = 7;
            insertVideosListView.Location = new System.Drawing.Point(25, 72);
            insertVideosListView.Name = "insertVideosListView";
            insertVideosListView.Size = new System.Drawing.Size(805, 276);
            insertVideosListView.TabIndex = 0;
            insertVideosListView.UseCompatibleStateImageBehavior = false;
            insertVideosListView.GridLines = true;
            insertVideosListView.View = View.Details;
            

            //insertVideosListView.ForeColor = Color.Black;
            insertVideosListView.View = System.Windows.Forms.View.Details;
            this.Controls.Add(insertVideosListView);

            InitializeComponent();
            materialRaisedButtonUploadVideos.AutoSize = false;
            materialRaisedButtonUploadVideos.Size = new System.Drawing.Size(141, 36);

            materialRaisedButtonPrevious.AutoSize = false;
            materialRaisedButtonPrevious.Size = new System.Drawing.Size(106, 36);


            materialRaisedButtonUploadAnalysis.Hide();
            textBoxUploadAnalysis.Hide();
        }//insertUploadForm Constructor

        //Click on Upload Videos Button
        private void materialRaisedButtonUploadVideos_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog UVD = new FolderBrowserDialog();
            UVD.SelectedPath= Program.getDefaultDatabeseFolderPath();    
            UVD.ShowDialog();

            string uploadVideosFolder = UVD.SelectedPath;

            //If directory exist
            if (Directory.Exists(uploadVideosFolder))
                textBoxUploadVideos.AppendText(uploadVideosFolder);
            else if (Directory.Exists(textBoxUploadVideos.Text))
                this.uploadVideosFolder = textBoxUploadVideos.Text;

        }//materialRaisedButtonUploadVideos_Click

        //Click on Upload Analysis Button
        private void materialRaisedButtonUploadAnalysis_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog UAD = new FolderBrowserDialog();
            UAD.SelectedPath = Program.getDefaultDatabeseFolderPath();   
            UAD.ShowDialog();

            string uploadAnalysisFolder = UAD.SelectedPath;
            //If directory exist
            if (Directory.Exists(uploadAnalysisFolder))
                textBoxUploadAnalysis.AppendText(uploadAnalysisFolder);
            else if (Directory.Exists(textBoxUploadAnalysis.Text))
                this.uploadAnalysisFolder = textBoxUploadAnalysis.Text;

        }//materialRaisedButtonUploadAnalysis_Click


        //Videos & Analysis RadioButton Independency
        private void materialRadioButtonInsertVideosAndAnalysis_CheckedChanged(object sender, EventArgs e)
        {
            if (materialRadioButtonInsertVideosAndAnalysis.Checked)
            {
                materialRaisedButtonUploadAnalysis.Show();
                textBoxUploadAnalysis.Show();
            }//if
        }//materialRadioButtonInsertVideosAndAnalysis_CheckedChanged

        private void butPrev_Click(object sender, EventArgs e)
        {

        }

        //Click on Previous Button
        private void materialRaisedButtonPrevious_Click(object sender, EventArgs e)
        {
            mainForm mainForm = new mainForm();
            this.Hide();
            mainForm.Show();
        }//materialRaisedButtonPrevious_Click

        //Clicking On Insert Button
        private void materialRaisedButtonInsertFiles_Click(object sender, EventArgs e)
        {

                //Videos Only
                if (materialRadioButtonInsertOnlyVideos.Checked)
                {

                   //If Videos Folder isn't valid
                   if (!(Directory.Exists(this.uploadVideosFolder)||Directory.Exists(textBoxUploadVideos.Text)))
                       MessageBox.Show("Video Folder Path Isn't Exist!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);

                   //If Videos Folder is valid
                   else Program.insertFilesInfo(textBoxUploadVideos.Text,0);

                }//Videos Only

                //Videos & Analysis
                else
                {

                //If uploadVideosFolder isn't valid
                if (!Directory.Exists(this.uploadVideosFolder) && !Directory.Exists(textBoxUploadVideos.Text))
                        MessageBox.Show("Video Folder Path Isn't Exist!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    //if uploadVideosFolder is valid
                    else
                    {
                        //If uploadAnalysisFolder isn't valid
                        if (!(Directory.Exists(this.uploadAnalysisFolder) || Directory.Exists(textBoxUploadAnalysis.Text)))
                            MessageBox.Show("Analysis Folder Path Isn't Exist!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        // If Both Folders are valid Run Indexing Functions (Video Indexing)
                        else
                        {
                            Program.insertFilesInfo(textBoxUploadVideos.Text,1);
                            Program.getClassificationFromDetailedAnalysis(textBoxUploadAnalysis.Text);
                        }// If Both Folders are valid Run Indexing Functions (Video Indexing)

                    }//if uploadVideosFolder is valid
                    
                }//Videos & Analysis

            //Close insertForm & Back to Main Form
            mainForm mainForm = new mainForm();
            this.Close();
            mainForm.Show();

        }//materialRaisedButtonPrevious_Click


        //Videos Only radio Button checked
        private void materialRadioButtonInsertOnlyVideos_CheckedChanged(object sender, EventArgs e)
        {
            materialRaisedButtonUploadAnalysis.Hide();
            textBoxUploadAnalysis.Hide();
        }//materialRadioButtonInsertOnlyVideos_CheckedChanged

       
    }//insertUploadForm
}//MetaFace
