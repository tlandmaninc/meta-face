﻿namespace MetaFace
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butRetriveData1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.butEditData2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.butInsertNewData3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butExitMain1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // butRetriveData1
            // 
            this.butRetriveData1.AutoSize = true;
            this.butRetriveData1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.butRetriveData1.Depth = 0;
            this.butRetriveData1.Icon = null;
            this.butRetriveData1.Location = new System.Drawing.Point(291, 283);
            this.butRetriveData1.MouseState = MaterialSkin.MouseState.HOVER;
            this.butRetriveData1.Name = "butRetriveData1";
            this.butRetriveData1.Primary = true;
            this.butRetriveData1.Size = new System.Drawing.Size(114, 36);
            this.butRetriveData1.TabIndex = 6;
            this.butRetriveData1.Text = "Retrive Data";
            this.butRetriveData1.UseVisualStyleBackColor = true;
            this.butRetriveData1.Click += new System.EventHandler(this.butRetriveData1_Click);
            // 
            // butEditData2
            // 
            this.butEditData2.AutoSize = true;
            this.butEditData2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.butEditData2.Depth = 0;
            this.butEditData2.Icon = null;
            this.butEditData2.Location = new System.Drawing.Point(291, 376);
            this.butEditData2.MouseState = MaterialSkin.MouseState.HOVER;
            this.butEditData2.Name = "butEditData2";
            this.butEditData2.Primary = true;
            this.butEditData2.Size = new System.Drawing.Size(89, 36);
            this.butEditData2.TabIndex = 7;
            this.butEditData2.Text = "Edit Data";
            this.butEditData2.UseVisualStyleBackColor = true;
            this.butEditData2.Click += new System.EventHandler(this.butEditData2_Click);
            // 
            // butInsertNewData3
            // 
            this.butInsertNewData3.AutoSize = true;
            this.butInsertNewData3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.butInsertNewData3.Depth = 0;
            this.butInsertNewData3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butInsertNewData3.Icon = null;
            this.butInsertNewData3.Location = new System.Drawing.Point(291, 466);
            this.butInsertNewData3.MouseState = MaterialSkin.MouseState.HOVER;
            this.butInsertNewData3.Name = "butInsertNewData3";
            this.butInsertNewData3.Primary = true;
            this.butInsertNewData3.Size = new System.Drawing.Size(140, 36);
            this.butInsertNewData3.TabIndex = 8;
            this.butInsertNewData3.Text = "Insert New Data";
            this.butInsertNewData3.UseVisualStyleBackColor = true;
            this.butInsertNewData3.Click += new System.EventHandler(this.butInsertNewData3_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::MetaFace.Properties.Resources.logo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Location = new System.Drawing.Point(91, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 163);
            this.panel1.TabIndex = 10;
            // 
            // butExitMain1
            // 
            this.butExitMain1.AutoSize = true;
            this.butExitMain1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.butExitMain1.Depth = 0;
            this.butExitMain1.Icon = null;
            this.butExitMain1.Location = new System.Drawing.Point(113, 689);
            this.butExitMain1.MouseState = MaterialSkin.MouseState.HOVER;
            this.butExitMain1.Name = "butExitMain1";
            this.butExitMain1.Primary = true;
            this.butExitMain1.Size = new System.Drawing.Size(56, 36);
            this.butExitMain1.TabIndex = 11;
            this.butExitMain1.Text = "Back";
            this.butExitMain1.UseVisualStyleBackColor = true;
            this.butExitMain1.Click += new System.EventHandler(this.butExitMain1_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MetaFace.Properties.Resources.logo;
            this.ClientSize = new System.Drawing.Size(753, 800);
            this.Controls.Add(this.butExitMain1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.butInsertNewData3);
            this.Controls.Add(this.butEditData2);
            this.Controls.Add(this.butRetriveData1);
            this.MaximizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MetaFace";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialRaisedButton butRetriveData1;
        private MaterialSkin.Controls.MaterialRaisedButton butEditData2;
        private MaterialSkin.Controls.MaterialRaisedButton butInsertNewData3;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialRaisedButton butExitMain1;
    }
}