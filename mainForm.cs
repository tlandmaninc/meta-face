﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;

namespace MetaFace
{
    public partial class mainForm : MaterialForm
    {
        //mainFrom Constructor
        public mainForm()
        {
            InitializeComponent();
            butRetriveData1.AutoSize = false;
            butRetriveData1.Height=50;
            butRetriveData1.Width = 200;
            butEditData2.AutoSize = false;
            butEditData2.Height = 50;
            butEditData2.Width = 200;
            butInsertNewData3.AutoSize = false;
            butInsertNewData3.Height = 50;
            butInsertNewData3.Width = 200;
            butExitMain1.AutoSize = false;
            butExitMain1.Height = 50;
            butExitMain1.Width = 100;
            this.Sizable = false;
        }//mainForm

        private void butRetriveData_Click(object sender, EventArgs e)
        {
            retriveDataFilterForm retriveDataFilterForm = new retriveDataFilterForm(true);
            this.Hide();
            retriveDataFilterForm.Show();
       
        }

        private void butEditData_Click(object sender, EventArgs e)
        {
            retriveDataFilterForm retriveDataFilterForm1 = new retriveDataFilterForm(false);
            this.Hide();
            retriveDataFilterForm1.Show();         
        }

        private void butInsertNewData_Click(object sender, EventArgs e)
        {
            insertUploadForm insertUploadForm = new insertUploadForm();
            this.Hide();
            insertUploadForm.Show();
        }

        //return to access form
        private void butExitMain1_Click(object sender, EventArgs e)
        {
            accessForm accessForm1 = new accessForm();
            accessForm1.Show();
            this.Hide();
        }

        private void butRetriveData1_Click(object sender, EventArgs e)
        {
            retriveDataFilterForm retriveDataFilterForm = new retriveDataFilterForm(true);
            this.Hide();
            retriveDataFilterForm.Show();
        }

        private void butEditData2_Click(object sender, EventArgs e)
        {
            retriveDataFilterForm retriveDataFilterForm1 = new retriveDataFilterForm(false);
            this.Hide();
            retriveDataFilterForm1.Show();
        }

        private void butInsertNewData3_Click(object sender, EventArgs e)
        {
            insertUploadForm insertUploadForm = new insertUploadForm();
            this.Hide();
            insertUploadForm.Show();
        }
        
    }//Main Form
}//MetaFace
