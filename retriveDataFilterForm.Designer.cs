﻿namespace MetaFace
{
    partial class retriveDataFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.DurationUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.DurationUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.QualityUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.QualityUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.FrameRatecomboBox2 = new System.Windows.Forms.ComboBox();
            this.FrameRatecomboBox1 = new System.Windows.Forms.ComboBox();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.WearingGlassesRadioButtonA = new MaterialSkin.Controls.MaterialRadioButton();
            this.WearingGlassesRadioButtonY = new MaterialSkin.Controls.MaterialRadioButton();
            this.WearingGlassesRadioButtonN = new MaterialSkin.Controls.MaterialRadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AgeRangeCheckBoxOVER80 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox5160 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox6170 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox7180 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox4150 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox3140 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox2130 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox1120 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox010 = new MaterialSkin.Controls.MaterialCheckBox();
            this.AgeRangeCheckBox1 = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GenderRadioButtonA = new MaterialSkin.Controls.MaterialRadioButton();
            this.GenderRadioButtonM = new MaterialSkin.Controls.MaterialRadioButton();
            this.GenderRadioButtonF = new MaterialSkin.Controls.MaterialRadioButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SubDatabaseCheckBoxCK = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxMMI = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxOther = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxUNBC = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxBimodal = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxFEED = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxDenver = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBox3 = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBox4 = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBox5 = new MaterialSkin.Controls.MaterialCheckBox();
            this.SubDatabaseCheckBoxALL = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.EthnicityCheckBoxOther = new MaterialSkin.Controls.MaterialCheckBox();
            this.EthnicityCheckBoxSouthAsia = new MaterialSkin.Controls.MaterialCheckBox();
            this.EthnicityCheckBoxAfrican = new MaterialSkin.Controls.MaterialCheckBox();
            this.EthnicityCheckBoxEasternAsia = new MaterialSkin.Controls.MaterialCheckBox();
            this.EthnicityCheckBoxCaucasian = new MaterialSkin.Controls.MaterialCheckBox();
            this.EthnicityCheckBoxALL = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.FormatCheckBoxFLV = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxRM = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxWMV = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxBGP = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxASF = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxAVI = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxMOV = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxMP4 = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxMOB = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxMPG = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormatCheckBoxALL = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.WithMoustachelRadioButtonA = new MaterialSkin.Controls.MaterialRadioButton();
            this.WithMoustacheRadioButtonY = new MaterialSkin.Controls.MaterialRadioButton();
            this.WithMoustacheRadioButtonN = new MaterialSkin.Controls.MaterialRadioButton();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.WithBearedRadioButtonA = new MaterialSkin.Controls.MaterialRadioButton();
            this.WithBearedRadioButtonY = new MaterialSkin.Controls.MaterialRadioButton();
            this.WithBearedRadioButtonN = new MaterialSkin.Controls.MaterialRadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.IncludeVoiceRadioButtonA = new MaterialSkin.Controls.MaterialRadioButton();
            this.IncludeVoiceRadioButtonY = new MaterialSkin.Controls.MaterialRadioButton();
            this.IncludeVoiceRadioButtonN = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ResolutionCheckBoxHIGH = new MaterialSkin.Controls.MaterialCheckBox();
            this.ResolutionCheckBoxMID = new MaterialSkin.Controls.MaterialCheckBox();
            this.ResolutionCheckBoxLOW = new MaterialSkin.Controls.MaterialCheckBox();
            this.ResolutionCheckBoxALL = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DurationUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurationUpDown1)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QualityUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualityUpDown1)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.DurationUpDown2);
            this.groupBox8.Controls.Add(this.materialLabel2);
            this.groupBox8.Controls.Add(this.materialLabel1);
            this.groupBox8.Controls.Add(this.DurationUpDown1);
            this.groupBox8.Location = new System.Drawing.Point(313, 190);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(179, 46);
            this.groupBox8.TabIndex = 48;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Duration";
            // 
            // DurationUpDown2
            // 
            this.DurationUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DurationUpDown2.Location = new System.Drawing.Point(121, 20);
            this.DurationUpDown2.Name = "DurationUpDown2";
            this.DurationUpDown2.Size = new System.Drawing.Size(47, 22);
            this.DurationUpDown2.TabIndex = 80;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(85, 21);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(37, 19);
            this.materialLabel2.TabIndex = 79;
            this.materialLabel2.Text = "Max";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(7, 20);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(34, 19);
            this.materialLabel1.TabIndex = 78;
            this.materialLabel1.Text = "Min";
            // 
            // DurationUpDown1
            // 
            this.DurationUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DurationUpDown1.Location = new System.Drawing.Point(45, 21);
            this.DurationUpDown1.Name = "DurationUpDown1";
            this.DurationUpDown1.Size = new System.Drawing.Size(36, 22);
            this.DurationUpDown1.TabIndex = 17;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.materialLabel3);
            this.groupBox9.Controls.Add(this.materialLabel4);
            this.groupBox9.Controls.Add(this.QualityUpDown2);
            this.groupBox9.Controls.Add(this.QualityUpDown1);
            this.groupBox9.Location = new System.Drawing.Point(313, 250);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(179, 46);
            this.groupBox9.TabIndex = 49;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Quality";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(84, 19);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(37, 19);
            this.materialLabel3.TabIndex = 81;
            this.materialLabel3.Text = "Max";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(6, 18);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(34, 19);
            this.materialLabel4.TabIndex = 80;
            this.materialLabel4.Text = "Min";
            // 
            // QualityUpDown2
            // 
            this.QualityUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.QualityUpDown2.Location = new System.Drawing.Point(121, 15);
            this.QualityUpDown2.Name = "QualityUpDown2";
            this.QualityUpDown2.Size = new System.Drawing.Size(47, 22);
            this.QualityUpDown2.TabIndex = 22;
            // 
            // QualityUpDown1
            // 
            this.QualityUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.QualityUpDown1.Location = new System.Drawing.Point(43, 17);
            this.QualityUpDown1.Name = "QualityUpDown1";
            this.QualityUpDown1.Size = new System.Drawing.Size(36, 22);
            this.QualityUpDown1.TabIndex = 21;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Transparent;
            this.groupBox10.Controls.Add(this.FrameRatecomboBox2);
            this.groupBox10.Controls.Add(this.FrameRatecomboBox1);
            this.groupBox10.Controls.Add(this.materialLabel5);
            this.groupBox10.Controls.Add(this.materialLabel6);
            this.groupBox10.Location = new System.Drawing.Point(313, 312);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(179, 53);
            this.groupBox10.TabIndex = 50;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Frame Rate";
            // 
            // FrameRatecomboBox2
            // 
            this.FrameRatecomboBox2.FormattingEnabled = true;
            this.FrameRatecomboBox2.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "60"});
            this.FrameRatecomboBox2.Location = new System.Drawing.Point(129, 20);
            this.FrameRatecomboBox2.Name = "FrameRatecomboBox2";
            this.FrameRatecomboBox2.Size = new System.Drawing.Size(39, 21);
            this.FrameRatecomboBox2.TabIndex = 84;
            // 
            // FrameRatecomboBox1
            // 
            this.FrameRatecomboBox1.FormattingEnabled = true;
            this.FrameRatecomboBox1.Items.AddRange(new object[] {
            "0",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55",
            "60"});
            this.FrameRatecomboBox1.Location = new System.Drawing.Point(49, 20);
            this.FrameRatecomboBox1.Name = "FrameRatecomboBox1";
            this.FrameRatecomboBox1.Size = new System.Drawing.Size(39, 21);
            this.FrameRatecomboBox1.TabIndex = 83;
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(86, 19);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(37, 19);
            this.materialLabel5.TabIndex = 81;
            this.materialLabel5.Text = "Max";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(9, 18);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(34, 19);
            this.materialLabel6.TabIndex = 80;
            this.materialLabel6.Text = "Min";
            // 
            // WearingGlassesRadioButtonA
            // 
            this.WearingGlassesRadioButtonA.AccessibleName = "";
            this.WearingGlassesRadioButtonA.AutoSize = true;
            this.WearingGlassesRadioButtonA.Checked = true;
            this.WearingGlassesRadioButtonA.Depth = 0;
            this.WearingGlassesRadioButtonA.Font = new System.Drawing.Font("Roboto", 10F);
            this.WearingGlassesRadioButtonA.Location = new System.Drawing.Point(8, 16);
            this.WearingGlassesRadioButtonA.Margin = new System.Windows.Forms.Padding(0);
            this.WearingGlassesRadioButtonA.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WearingGlassesRadioButtonA.MouseState = MaterialSkin.MouseState.HOVER;
            this.WearingGlassesRadioButtonA.Name = "WearingGlassesRadioButtonA";
            this.WearingGlassesRadioButtonA.Ripple = true;
            this.WearingGlassesRadioButtonA.Size = new System.Drawing.Size(45, 30);
            this.WearingGlassesRadioButtonA.TabIndex = 0;
            this.WearingGlassesRadioButtonA.TabStop = true;
            this.WearingGlassesRadioButtonA.Text = "All";
            this.WearingGlassesRadioButtonA.UseVisualStyleBackColor = true;
            this.WearingGlassesRadioButtonA.CheckedChanged += new System.EventHandler(this.WearingGlassesRadioButtonA_CheckedChanged);
            // 
            // WearingGlassesRadioButtonY
            // 
            this.WearingGlassesRadioButtonY.AccessibleName = "Gender";
            this.WearingGlassesRadioButtonY.AutoSize = true;
            this.WearingGlassesRadioButtonY.BackColor = System.Drawing.Color.Black;
            this.WearingGlassesRadioButtonY.Depth = 0;
            this.WearingGlassesRadioButtonY.Font = new System.Drawing.Font("Roboto", 10F);
            this.WearingGlassesRadioButtonY.Location = new System.Drawing.Point(60, 16);
            this.WearingGlassesRadioButtonY.Margin = new System.Windows.Forms.Padding(0);
            this.WearingGlassesRadioButtonY.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WearingGlassesRadioButtonY.MouseState = MaterialSkin.MouseState.HOVER;
            this.WearingGlassesRadioButtonY.Name = "WearingGlassesRadioButtonY";
            this.WearingGlassesRadioButtonY.Ripple = true;
            this.WearingGlassesRadioButtonY.Size = new System.Drawing.Size(52, 30);
            this.WearingGlassesRadioButtonY.TabIndex = 2;
            this.WearingGlassesRadioButtonY.Text = "Yes";
            this.WearingGlassesRadioButtonY.UseVisualStyleBackColor = false;
            this.WearingGlassesRadioButtonY.CheckedChanged += new System.EventHandler(this.WearingGlassesRadioButtonY_CheckedChanged);
            // 
            // WearingGlassesRadioButtonN
            // 
            this.WearingGlassesRadioButtonN.AccessibleName = "Gender";
            this.WearingGlassesRadioButtonN.AutoSize = true;
            this.WearingGlassesRadioButtonN.BackColor = System.Drawing.Color.Black;
            this.WearingGlassesRadioButtonN.Depth = 0;
            this.WearingGlassesRadioButtonN.Font = new System.Drawing.Font("Roboto", 10F);
            this.WearingGlassesRadioButtonN.Location = new System.Drawing.Point(121, 16);
            this.WearingGlassesRadioButtonN.Margin = new System.Windows.Forms.Padding(0);
            this.WearingGlassesRadioButtonN.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WearingGlassesRadioButtonN.MouseState = MaterialSkin.MouseState.HOVER;
            this.WearingGlassesRadioButtonN.Name = "WearingGlassesRadioButtonN";
            this.WearingGlassesRadioButtonN.Ripple = true;
            this.WearingGlassesRadioButtonN.Size = new System.Drawing.Size(47, 30);
            this.WearingGlassesRadioButtonN.TabIndex = 3;
            this.WearingGlassesRadioButtonN.Text = "No";
            this.WearingGlassesRadioButtonN.UseVisualStyleBackColor = false;
            this.WearingGlassesRadioButtonN.CheckedChanged += new System.EventHandler(this.WearingGlassesRadioButtonN_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Location = new System.Drawing.Point(55, 376);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 174);
            this.groupBox2.TabIndex = 67;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Age Range";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.AgeRangeCheckBoxOVER80);
            this.panel1.Controls.Add(this.AgeRangeCheckBox5160);
            this.panel1.Controls.Add(this.AgeRangeCheckBox6170);
            this.panel1.Controls.Add(this.AgeRangeCheckBox7180);
            this.panel1.Controls.Add(this.AgeRangeCheckBox4150);
            this.panel1.Controls.Add(this.AgeRangeCheckBox3140);
            this.panel1.Controls.Add(this.AgeRangeCheckBox2130);
            this.panel1.Controls.Add(this.AgeRangeCheckBox1120);
            this.panel1.Controls.Add(this.AgeRangeCheckBox010);
            this.panel1.Controls.Add(this.AgeRangeCheckBox1);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 149);
            this.panel1.TabIndex = 0;
            // 
            // AgeRangeCheckBoxOVER80
            // 
            this.AgeRangeCheckBoxOVER80.AutoSize = true;
            this.AgeRangeCheckBoxOVER80.Depth = 0;
            this.AgeRangeCheckBoxOVER80.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBoxOVER80.Location = new System.Drawing.Point(13, 272);
            this.AgeRangeCheckBoxOVER80.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBoxOVER80.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBoxOVER80.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBoxOVER80.Name = "AgeRangeCheckBoxOVER80";
            this.AgeRangeCheckBoxOVER80.Ripple = true;
            this.AgeRangeCheckBoxOVER80.Size = new System.Drawing.Size(77, 30);
            this.AgeRangeCheckBoxOVER80.TabIndex = 26;
            this.AgeRangeCheckBoxOVER80.Text = "Over 80";
            this.AgeRangeCheckBoxOVER80.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBoxOVER80.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBoxOVER80_CheckedChanged);
            // 
            // AgeRangeCheckBox5160
            // 
            this.AgeRangeCheckBox5160.AutoSize = true;
            this.AgeRangeCheckBox5160.Depth = 0;
            this.AgeRangeCheckBox5160.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox5160.Location = new System.Drawing.Point(13, 185);
            this.AgeRangeCheckBox5160.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox5160.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox5160.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox5160.Name = "AgeRangeCheckBox5160";
            this.AgeRangeCheckBox5160.Ripple = true;
            this.AgeRangeCheckBox5160.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox5160.TabIndex = 25;
            this.AgeRangeCheckBox5160.Text = "51-60";
            this.AgeRangeCheckBox5160.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox5160.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox5160_CheckedChanged);
            // 
            // AgeRangeCheckBox6170
            // 
            this.AgeRangeCheckBox6170.AutoSize = true;
            this.AgeRangeCheckBox6170.Depth = 0;
            this.AgeRangeCheckBox6170.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox6170.Location = new System.Drawing.Point(13, 212);
            this.AgeRangeCheckBox6170.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox6170.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox6170.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox6170.Name = "AgeRangeCheckBox6170";
            this.AgeRangeCheckBox6170.Ripple = true;
            this.AgeRangeCheckBox6170.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox6170.TabIndex = 24;
            this.AgeRangeCheckBox6170.Text = "61-70";
            this.AgeRangeCheckBox6170.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox6170.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox6170_CheckedChanged);
            // 
            // AgeRangeCheckBox7180
            // 
            this.AgeRangeCheckBox7180.AutoSize = true;
            this.AgeRangeCheckBox7180.Depth = 0;
            this.AgeRangeCheckBox7180.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox7180.Location = new System.Drawing.Point(13, 242);
            this.AgeRangeCheckBox7180.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox7180.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox7180.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox7180.Name = "AgeRangeCheckBox7180";
            this.AgeRangeCheckBox7180.Ripple = true;
            this.AgeRangeCheckBox7180.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox7180.TabIndex = 23;
            this.AgeRangeCheckBox7180.Text = "71-80";
            this.AgeRangeCheckBox7180.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox7180.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox7180_CheckedChanged);
            // 
            // AgeRangeCheckBox4150
            // 
            this.AgeRangeCheckBox4150.AutoSize = true;
            this.AgeRangeCheckBox4150.Depth = 0;
            this.AgeRangeCheckBox4150.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox4150.Location = new System.Drawing.Point(13, 155);
            this.AgeRangeCheckBox4150.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox4150.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox4150.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox4150.Name = "AgeRangeCheckBox4150";
            this.AgeRangeCheckBox4150.Ripple = true;
            this.AgeRangeCheckBox4150.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox4150.TabIndex = 22;
            this.AgeRangeCheckBox4150.Text = "41-50";
            this.AgeRangeCheckBox4150.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox4150.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox4150_CheckedChanged);
            // 
            // AgeRangeCheckBox3140
            // 
            this.AgeRangeCheckBox3140.AutoSize = true;
            this.AgeRangeCheckBox3140.Depth = 0;
            this.AgeRangeCheckBox3140.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox3140.Location = new System.Drawing.Point(13, 125);
            this.AgeRangeCheckBox3140.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox3140.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox3140.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox3140.Name = "AgeRangeCheckBox3140";
            this.AgeRangeCheckBox3140.Ripple = true;
            this.AgeRangeCheckBox3140.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox3140.TabIndex = 21;
            this.AgeRangeCheckBox3140.Text = "31-40";
            this.AgeRangeCheckBox3140.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox3140.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox3140_CheckedChanged);
            // 
            // AgeRangeCheckBox2130
            // 
            this.AgeRangeCheckBox2130.AutoSize = true;
            this.AgeRangeCheckBox2130.Depth = 0;
            this.AgeRangeCheckBox2130.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox2130.Location = new System.Drawing.Point(13, 95);
            this.AgeRangeCheckBox2130.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox2130.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox2130.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox2130.Name = "AgeRangeCheckBox2130";
            this.AgeRangeCheckBox2130.Ripple = true;
            this.AgeRangeCheckBox2130.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox2130.TabIndex = 20;
            this.AgeRangeCheckBox2130.Text = "21-30";
            this.AgeRangeCheckBox2130.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox2130.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox2130_CheckedChanged);
            // 
            // AgeRangeCheckBox1120
            // 
            this.AgeRangeCheckBox1120.AutoSize = true;
            this.AgeRangeCheckBox1120.Depth = 0;
            this.AgeRangeCheckBox1120.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox1120.Location = new System.Drawing.Point(13, 65);
            this.AgeRangeCheckBox1120.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox1120.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox1120.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox1120.Name = "AgeRangeCheckBox1120";
            this.AgeRangeCheckBox1120.Ripple = true;
            this.AgeRangeCheckBox1120.Size = new System.Drawing.Size(66, 30);
            this.AgeRangeCheckBox1120.TabIndex = 19;
            this.AgeRangeCheckBox1120.Text = "11-20";
            this.AgeRangeCheckBox1120.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox1120.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox1120_CheckedChanged);
            // 
            // AgeRangeCheckBox010
            // 
            this.AgeRangeCheckBox010.AutoSize = true;
            this.AgeRangeCheckBox010.Depth = 0;
            this.AgeRangeCheckBox010.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox010.Location = new System.Drawing.Point(13, 35);
            this.AgeRangeCheckBox010.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox010.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox010.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox010.Name = "AgeRangeCheckBox010";
            this.AgeRangeCheckBox010.Ripple = true;
            this.AgeRangeCheckBox010.Size = new System.Drawing.Size(58, 30);
            this.AgeRangeCheckBox010.TabIndex = 18;
            this.AgeRangeCheckBox010.Text = "0-10";
            this.AgeRangeCheckBox010.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox010.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox010_CheckedChanged_1);
            // 
            // AgeRangeCheckBox1
            // 
            this.AgeRangeCheckBox1.AutoSize = true;
            this.AgeRangeCheckBox1.Depth = 0;
            this.AgeRangeCheckBox1.Font = new System.Drawing.Font("Roboto", 10F);
            this.AgeRangeCheckBox1.Location = new System.Drawing.Point(13, 5);
            this.AgeRangeCheckBox1.Margin = new System.Windows.Forms.Padding(0);
            this.AgeRangeCheckBox1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AgeRangeCheckBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.AgeRangeCheckBox1.Name = "AgeRangeCheckBox1";
            this.AgeRangeCheckBox1.Ripple = true;
            this.AgeRangeCheckBox1.Size = new System.Drawing.Size(46, 30);
            this.AgeRangeCheckBox1.TabIndex = 17;
            this.AgeRangeCheckBox1.Text = "All";
            this.AgeRangeCheckBox1.UseVisualStyleBackColor = true;
            this.AgeRangeCheckBox1.CheckedChanged += new System.EventHandler(this.AgeRangeCheckBox1_CheckedChanged_1);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.CausesValidation = false;
            this.groupBox1.Controls.Add(this.GenderRadioButtonA);
            this.groupBox1.Controls.Add(this.GenderRadioButtonM);
            this.groupBox1.Controls.Add(this.GenderRadioButtonF);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.groupBox1.Location = new System.Drawing.Point(54, 129);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 50);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gender";
            // 
            // GenderRadioButtonA
            // 
            this.GenderRadioButtonA.AccessibleName = "";
            this.GenderRadioButtonA.AutoSize = true;
            this.GenderRadioButtonA.Checked = true;
            this.GenderRadioButtonA.Depth = 0;
            this.GenderRadioButtonA.Font = new System.Drawing.Font("Roboto", 10F);
            this.GenderRadioButtonA.Location = new System.Drawing.Point(7, 16);
            this.GenderRadioButtonA.Margin = new System.Windows.Forms.Padding(0);
            this.GenderRadioButtonA.MouseLocation = new System.Drawing.Point(-1, -1);
            this.GenderRadioButtonA.MouseState = MaterialSkin.MouseState.HOVER;
            this.GenderRadioButtonA.Name = "GenderRadioButtonA";
            this.GenderRadioButtonA.Ripple = true;
            this.GenderRadioButtonA.Size = new System.Drawing.Size(45, 30);
            this.GenderRadioButtonA.TabIndex = 4;
            this.GenderRadioButtonA.TabStop = true;
            this.GenderRadioButtonA.Text = "All";
            this.GenderRadioButtonA.UseVisualStyleBackColor = true;
            // 
            // GenderRadioButtonM
            // 
            this.GenderRadioButtonM.AccessibleName = "Gender";
            this.GenderRadioButtonM.AutoSize = true;
            this.GenderRadioButtonM.BackColor = System.Drawing.Color.Black;
            this.GenderRadioButtonM.Depth = 0;
            this.GenderRadioButtonM.Font = new System.Drawing.Font("Roboto", 10F);
            this.GenderRadioButtonM.Location = new System.Drawing.Point(56, 16);
            this.GenderRadioButtonM.Margin = new System.Windows.Forms.Padding(0);
            this.GenderRadioButtonM.MouseLocation = new System.Drawing.Point(-1, -1);
            this.GenderRadioButtonM.MouseState = MaterialSkin.MouseState.HOVER;
            this.GenderRadioButtonM.Name = "GenderRadioButtonM";
            this.GenderRadioButtonM.Ripple = true;
            this.GenderRadioButtonM.Size = new System.Drawing.Size(59, 30);
            this.GenderRadioButtonM.TabIndex = 5;
            this.GenderRadioButtonM.Text = "Male";
            this.GenderRadioButtonM.UseVisualStyleBackColor = false;
            this.GenderRadioButtonM.CheckedChanged += new System.EventHandler(this.GenderRadioButtonM_CheckedChanged);
            // 
            // GenderRadioButtonF
            // 
            this.GenderRadioButtonF.AccessibleName = "Gender";
            this.GenderRadioButtonF.AutoSize = true;
            this.GenderRadioButtonF.BackColor = System.Drawing.Color.Black;
            this.GenderRadioButtonF.Depth = 0;
            this.GenderRadioButtonF.Font = new System.Drawing.Font("Roboto", 10F);
            this.GenderRadioButtonF.Location = new System.Drawing.Point(113, 16);
            this.GenderRadioButtonF.Margin = new System.Windows.Forms.Padding(0);
            this.GenderRadioButtonF.MouseLocation = new System.Drawing.Point(-1, -1);
            this.GenderRadioButtonF.MouseState = MaterialSkin.MouseState.HOVER;
            this.GenderRadioButtonF.Name = "GenderRadioButtonF";
            this.GenderRadioButtonF.Ripple = true;
            this.GenderRadioButtonF.Size = new System.Drawing.Size(74, 30);
            this.GenderRadioButtonF.TabIndex = 6;
            this.GenderRadioButtonF.Text = "Female";
            this.GenderRadioButtonF.UseVisualStyleBackColor = false;
            this.GenderRadioButtonF.CheckedChanged += new System.EventHandler(this.GenderRadioButtonF_CheckedChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.White;
            this.groupBox13.Controls.Add(this.WearingGlassesRadioButtonA);
            this.groupBox13.Controls.Add(this.WearingGlassesRadioButtonY);
            this.groupBox13.Controls.Add(this.WearingGlassesRadioButtonN);
            this.groupBox13.Location = new System.Drawing.Point(55, 190);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(206, 50);
            this.groupBox13.TabIndex = 70;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Wearing Glasses";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.White;
            this.groupBox6.Controls.Add(this.panel2);
            this.groupBox6.Location = new System.Drawing.Point(535, 129);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(405, 589);
            this.groupBox6.TabIndex = 68;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Sub Database";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxCK);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxMMI);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxOther);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxUNBC);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxBimodal);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxFEED);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxDenver);
            this.panel2.Controls.Add(this.SubDatabaseCheckBox3);
            this.panel2.Controls.Add(this.SubDatabaseCheckBox4);
            this.panel2.Controls.Add(this.SubDatabaseCheckBox5);
            this.panel2.Controls.Add(this.SubDatabaseCheckBoxALL);
            this.panel2.Location = new System.Drawing.Point(6, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(394, 564);
            this.panel2.TabIndex = 0;
            // 
            // SubDatabaseCheckBoxCK
            // 
            this.SubDatabaseCheckBoxCK.AutoSize = true;
            this.SubDatabaseCheckBoxCK.Depth = 0;
            this.SubDatabaseCheckBoxCK.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxCK.Location = new System.Drawing.Point(13, 95);
            this.SubDatabaseCheckBoxCK.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxCK.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxCK.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxCK.Name = "SubDatabaseCheckBoxCK";
            this.SubDatabaseCheckBoxCK.Ripple = true;
            this.SubDatabaseCheckBoxCK.Size = new System.Drawing.Size(143, 30);
            this.SubDatabaseCheckBoxCK.TabIndex = 29;
            this.SubDatabaseCheckBoxCK.Text = "Cohn-Kanade (CK)";
            this.SubDatabaseCheckBoxCK.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxCK.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxCK_CheckedChanged_1);
            // 
            // SubDatabaseCheckBoxMMI
            // 
            this.SubDatabaseCheckBoxMMI.AutoSize = true;
            this.SubDatabaseCheckBoxMMI.Depth = 0;
            this.SubDatabaseCheckBoxMMI.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxMMI.Location = new System.Drawing.Point(13, 214);
            this.SubDatabaseCheckBoxMMI.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxMMI.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxMMI.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxMMI.Name = "SubDatabaseCheckBoxMMI";
            this.SubDatabaseCheckBoxMMI.Ripple = true;
            this.SubDatabaseCheckBoxMMI.Size = new System.Drawing.Size(169, 30);
            this.SubDatabaseCheckBoxMMI.TabIndex = 28;
            this.SubDatabaseCheckBoxMMI.Text = "MMI Facial Expression";
            this.SubDatabaseCheckBoxMMI.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxMMI.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxMMI_CheckedChanged);
            // 
            // SubDatabaseCheckBoxOther
            // 
            this.SubDatabaseCheckBoxOther.AutoSize = true;
            this.SubDatabaseCheckBoxOther.Depth = 0;
            this.SubDatabaseCheckBoxOther.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxOther.Location = new System.Drawing.Point(13, 305);
            this.SubDatabaseCheckBoxOther.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxOther.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxOther.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxOther.Name = "SubDatabaseCheckBoxOther";
            this.SubDatabaseCheckBoxOther.Ripple = true;
            this.SubDatabaseCheckBoxOther.Size = new System.Drawing.Size(64, 30);
            this.SubDatabaseCheckBoxOther.TabIndex = 27;
            this.SubDatabaseCheckBoxOther.Text = "Other";
            this.SubDatabaseCheckBoxOther.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxOther.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxOther_CheckedChanged);
            // 
            // SubDatabaseCheckBoxUNBC
            // 
            this.SubDatabaseCheckBoxUNBC.AutoSize = true;
            this.SubDatabaseCheckBoxUNBC.Depth = 0;
            this.SubDatabaseCheckBoxUNBC.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxUNBC.Location = new System.Drawing.Point(13, 274);
            this.SubDatabaseCheckBoxUNBC.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxUNBC.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxUNBC.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxUNBC.Name = "SubDatabaseCheckBoxUNBC";
            this.SubDatabaseCheckBoxUNBC.Ripple = true;
            this.SubDatabaseCheckBoxUNBC.Size = new System.Drawing.Size(343, 30);
            this.SubDatabaseCheckBoxUNBC.TabIndex = 26;
            this.SubDatabaseCheckBoxUNBC.Text = "UNBC-McMaster Shoulder Pain Expression Archive ";
            this.SubDatabaseCheckBoxUNBC.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxUNBC.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxUNBC_CheckedChanged);
            // 
            // SubDatabaseCheckBoxBimodal
            // 
            this.SubDatabaseCheckBoxBimodal.AutoSize = true;
            this.SubDatabaseCheckBoxBimodal.Depth = 0;
            this.SubDatabaseCheckBoxBimodal.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxBimodal.Location = new System.Drawing.Point(13, 244);
            this.SubDatabaseCheckBoxBimodal.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxBimodal.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxBimodal.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxBimodal.Name = "SubDatabaseCheckBoxBimodal";
            this.SubDatabaseCheckBoxBimodal.Ripple = true;
            this.SubDatabaseCheckBoxBimodal.Size = new System.Drawing.Size(300, 30);
            this.SubDatabaseCheckBoxBimodal.TabIndex = 25;
            this.SubDatabaseCheckBoxBimodal.Text = "The Bimodal Face and Body Gesture (FABO) ";
            this.SubDatabaseCheckBoxBimodal.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxBimodal.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxBimodal_CheckedChanged);
            // 
            // SubDatabaseCheckBoxFEED
            // 
            this.SubDatabaseCheckBoxFEED.AutoSize = true;
            this.SubDatabaseCheckBoxFEED.Depth = 0;
            this.SubDatabaseCheckBoxFEED.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxFEED.Location = new System.Drawing.Point(13, 181);
            this.SubDatabaseCheckBoxFEED.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxFEED.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxFEED.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxFEED.Name = "SubDatabaseCheckBoxFEED";
            this.SubDatabaseCheckBoxFEED.Ripple = true;
            this.SubDatabaseCheckBoxFEED.Size = new System.Drawing.Size(271, 30);
            this.SubDatabaseCheckBoxFEED.TabIndex = 22;
            this.SubDatabaseCheckBoxFEED.Text = "Facial Expressions and Emotion (FEED)";
            this.SubDatabaseCheckBoxFEED.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxFEED.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxFEED_CheckedChanged);
            // 
            // SubDatabaseCheckBoxDenver
            // 
            this.SubDatabaseCheckBoxDenver.AutoSize = true;
            this.SubDatabaseCheckBoxDenver.Depth = 0;
            this.SubDatabaseCheckBoxDenver.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxDenver.Location = new System.Drawing.Point(13, 151);
            this.SubDatabaseCheckBoxDenver.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxDenver.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxDenver.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxDenver.Name = "SubDatabaseCheckBoxDenver";
            this.SubDatabaseCheckBoxDenver.Ripple = true;
            this.SubDatabaseCheckBoxDenver.Size = new System.Drawing.Size(371, 30);
            this.SubDatabaseCheckBoxDenver.TabIndex = 21;
            this.SubDatabaseCheckBoxDenver.Text = "Denver Intensity of Spontaneous Facial Actions (DISFA)";
            this.SubDatabaseCheckBoxDenver.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxDenver.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxDenver_CheckedChanged);
            // 
            // SubDatabaseCheckBox3
            // 
            this.SubDatabaseCheckBox3.AutoSize = true;
            this.SubDatabaseCheckBox3.Depth = 0;
            this.SubDatabaseCheckBox3.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBox3.Location = new System.Drawing.Point(13, 121);
            this.SubDatabaseCheckBox3.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBox3.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBox3.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBox3.Name = "SubDatabaseCheckBox3";
            this.SubDatabaseCheckBox3.Ripple = true;
            this.SubDatabaseCheckBox3.Size = new System.Drawing.Size(375, 30);
            this.SubDatabaseCheckBox3.TabIndex = 20;
            this.SubDatabaseCheckBox3.Text = "Biwi 3D Audiovisual Corpus of Affective Communication ";
            this.SubDatabaseCheckBox3.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBox3.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBox3_CheckedChanged);
            // 
            // SubDatabaseCheckBox4
            // 
            this.SubDatabaseCheckBox4.AutoSize = true;
            this.SubDatabaseCheckBox4.Depth = 0;
            this.SubDatabaseCheckBox4.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBox4.Location = new System.Drawing.Point(13, 65);
            this.SubDatabaseCheckBox4.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBox4.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBox4.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBox4.Name = "SubDatabaseCheckBox4";
            this.SubDatabaseCheckBox4.Ripple = true;
            this.SubDatabaseCheckBox4.Size = new System.Drawing.Size(148, 30);
            this.SubDatabaseCheckBox4.TabIndex = 19;
            this.SubDatabaseCheckBox4.Text = "Belfast Naturalistic ";
            this.SubDatabaseCheckBox4.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBox4.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBox4_CheckedChanged);
            // 
            // SubDatabaseCheckBox5
            // 
            this.SubDatabaseCheckBox5.AutoSize = true;
            this.SubDatabaseCheckBox5.Depth = 0;
            this.SubDatabaseCheckBox5.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBox5.Location = new System.Drawing.Point(13, 35);
            this.SubDatabaseCheckBox5.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBox5.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBox5.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBox5.Name = "SubDatabaseCheckBox5";
            this.SubDatabaseCheckBox5.Ripple = true;
            this.SubDatabaseCheckBox5.Size = new System.Drawing.Size(86, 30);
            this.SubDatabaseCheckBox5.TabIndex = 18;
            this.SubDatabaseCheckBox5.Text = "Affectiva ";
            this.SubDatabaseCheckBox5.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBox5.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBox5_CheckedChanged);
            // 
            // SubDatabaseCheckBoxALL
            // 
            this.SubDatabaseCheckBoxALL.AutoSize = true;
            this.SubDatabaseCheckBoxALL.Depth = 0;
            this.SubDatabaseCheckBoxALL.Font = new System.Drawing.Font("Roboto", 10F);
            this.SubDatabaseCheckBoxALL.Location = new System.Drawing.Point(13, 5);
            this.SubDatabaseCheckBoxALL.Margin = new System.Windows.Forms.Padding(0);
            this.SubDatabaseCheckBoxALL.MouseLocation = new System.Drawing.Point(-1, -1);
            this.SubDatabaseCheckBoxALL.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubDatabaseCheckBoxALL.Name = "SubDatabaseCheckBoxALL";
            this.SubDatabaseCheckBoxALL.Ripple = true;
            this.SubDatabaseCheckBoxALL.Size = new System.Drawing.Size(46, 30);
            this.SubDatabaseCheckBoxALL.TabIndex = 17;
            this.SubDatabaseCheckBoxALL.Text = "All";
            this.SubDatabaseCheckBoxALL.UseVisualStyleBackColor = true;
            this.SubDatabaseCheckBoxALL.CheckedChanged += new System.EventHandler(this.SubDatabaseCheckBoxALL_CheckedChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.White;
            this.groupBox12.Controls.Add(this.panel3);
            this.groupBox12.Location = new System.Drawing.Point(55, 562);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(206, 156);
            this.groupBox12.TabIndex = 73;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Ethnicity";
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.EthnicityCheckBoxOther);
            this.panel3.Controls.Add(this.EthnicityCheckBoxSouthAsia);
            this.panel3.Controls.Add(this.EthnicityCheckBoxAfrican);
            this.panel3.Controls.Add(this.EthnicityCheckBoxEasternAsia);
            this.panel3.Controls.Add(this.EthnicityCheckBoxCaucasian);
            this.panel3.Controls.Add(this.EthnicityCheckBoxALL);
            this.panel3.Location = new System.Drawing.Point(8, 17);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(192, 133);
            this.panel3.TabIndex = 0;
            // 
            // EthnicityCheckBoxOther
            // 
            this.EthnicityCheckBoxOther.AutoSize = true;
            this.EthnicityCheckBoxOther.Depth = 0;
            this.EthnicityCheckBoxOther.Font = new System.Drawing.Font("Roboto", 10F);
            this.EthnicityCheckBoxOther.Location = new System.Drawing.Point(13, 155);
            this.EthnicityCheckBoxOther.Margin = new System.Windows.Forms.Padding(0);
            this.EthnicityCheckBoxOther.MouseLocation = new System.Drawing.Point(-1, -1);
            this.EthnicityCheckBoxOther.MouseState = MaterialSkin.MouseState.HOVER;
            this.EthnicityCheckBoxOther.Name = "EthnicityCheckBoxOther";
            this.EthnicityCheckBoxOther.Ripple = true;
            this.EthnicityCheckBoxOther.Size = new System.Drawing.Size(64, 30);
            this.EthnicityCheckBoxOther.TabIndex = 22;
            this.EthnicityCheckBoxOther.Text = "Other";
            this.EthnicityCheckBoxOther.UseVisualStyleBackColor = true;
            this.EthnicityCheckBoxOther.CheckedChanged += new System.EventHandler(this.EthnicityCheckBoxOther_CheckedChanged);
            // 
            // EthnicityCheckBoxSouthAsia
            // 
            this.EthnicityCheckBoxSouthAsia.AutoSize = true;
            this.EthnicityCheckBoxSouthAsia.Depth = 0;
            this.EthnicityCheckBoxSouthAsia.Font = new System.Drawing.Font("Roboto", 10F);
            this.EthnicityCheckBoxSouthAsia.Location = new System.Drawing.Point(13, 125);
            this.EthnicityCheckBoxSouthAsia.Margin = new System.Windows.Forms.Padding(0);
            this.EthnicityCheckBoxSouthAsia.MouseLocation = new System.Drawing.Point(-1, -1);
            this.EthnicityCheckBoxSouthAsia.MouseState = MaterialSkin.MouseState.HOVER;
            this.EthnicityCheckBoxSouthAsia.Name = "EthnicityCheckBoxSouthAsia";
            this.EthnicityCheckBoxSouthAsia.Ripple = true;
            this.EthnicityCheckBoxSouthAsia.Size = new System.Drawing.Size(96, 30);
            this.EthnicityCheckBoxSouthAsia.TabIndex = 21;
            this.EthnicityCheckBoxSouthAsia.Text = "South Asia";
            this.EthnicityCheckBoxSouthAsia.UseVisualStyleBackColor = true;
            this.EthnicityCheckBoxSouthAsia.CheckedChanged += new System.EventHandler(this.EthnicityCheckBoxSouthAsia_CheckedChanged);
            // 
            // EthnicityCheckBoxAfrican
            // 
            this.EthnicityCheckBoxAfrican.AutoSize = true;
            this.EthnicityCheckBoxAfrican.Depth = 0;
            this.EthnicityCheckBoxAfrican.Font = new System.Drawing.Font("Roboto", 10F);
            this.EthnicityCheckBoxAfrican.Location = new System.Drawing.Point(13, 95);
            this.EthnicityCheckBoxAfrican.Margin = new System.Windows.Forms.Padding(0);
            this.EthnicityCheckBoxAfrican.MouseLocation = new System.Drawing.Point(-1, -1);
            this.EthnicityCheckBoxAfrican.MouseState = MaterialSkin.MouseState.HOVER;
            this.EthnicityCheckBoxAfrican.Name = "EthnicityCheckBoxAfrican";
            this.EthnicityCheckBoxAfrican.Ripple = true;
            this.EthnicityCheckBoxAfrican.Size = new System.Drawing.Size(75, 30);
            this.EthnicityCheckBoxAfrican.TabIndex = 20;
            this.EthnicityCheckBoxAfrican.Text = "African";
            this.EthnicityCheckBoxAfrican.UseVisualStyleBackColor = true;
            this.EthnicityCheckBoxAfrican.CheckedChanged += new System.EventHandler(this.EthnicityCheckBoxAfrican_CheckedChanged);
            // 
            // EthnicityCheckBoxEasternAsia
            // 
            this.EthnicityCheckBoxEasternAsia.AutoSize = true;
            this.EthnicityCheckBoxEasternAsia.Depth = 0;
            this.EthnicityCheckBoxEasternAsia.Font = new System.Drawing.Font("Roboto", 10F);
            this.EthnicityCheckBoxEasternAsia.Location = new System.Drawing.Point(13, 65);
            this.EthnicityCheckBoxEasternAsia.Margin = new System.Windows.Forms.Padding(0);
            this.EthnicityCheckBoxEasternAsia.MouseLocation = new System.Drawing.Point(-1, -1);
            this.EthnicityCheckBoxEasternAsia.MouseState = MaterialSkin.MouseState.HOVER;
            this.EthnicityCheckBoxEasternAsia.Name = "EthnicityCheckBoxEasternAsia";
            this.EthnicityCheckBoxEasternAsia.Ripple = true;
            this.EthnicityCheckBoxEasternAsia.Size = new System.Drawing.Size(107, 30);
            this.EthnicityCheckBoxEasternAsia.TabIndex = 19;
            this.EthnicityCheckBoxEasternAsia.Text = "Eastern Asia";
            this.EthnicityCheckBoxEasternAsia.UseVisualStyleBackColor = true;
            this.EthnicityCheckBoxEasternAsia.CheckedChanged += new System.EventHandler(this.EthnicityCheckBoxEasternAsia_CheckedChanged);
            // 
            // EthnicityCheckBoxCaucasian
            // 
            this.EthnicityCheckBoxCaucasian.AutoSize = true;
            this.EthnicityCheckBoxCaucasian.Depth = 0;
            this.EthnicityCheckBoxCaucasian.Font = new System.Drawing.Font("Roboto", 10F);
            this.EthnicityCheckBoxCaucasian.Location = new System.Drawing.Point(13, 35);
            this.EthnicityCheckBoxCaucasian.Margin = new System.Windows.Forms.Padding(0);
            this.EthnicityCheckBoxCaucasian.MouseLocation = new System.Drawing.Point(-1, -1);
            this.EthnicityCheckBoxCaucasian.MouseState = MaterialSkin.MouseState.HOVER;
            this.EthnicityCheckBoxCaucasian.Name = "EthnicityCheckBoxCaucasian";
            this.EthnicityCheckBoxCaucasian.Ripple = true;
            this.EthnicityCheckBoxCaucasian.Size = new System.Drawing.Size(94, 30);
            this.EthnicityCheckBoxCaucasian.TabIndex = 18;
            this.EthnicityCheckBoxCaucasian.Text = "Caucasian";
            this.EthnicityCheckBoxCaucasian.UseVisualStyleBackColor = true;
            this.EthnicityCheckBoxCaucasian.CheckedChanged += new System.EventHandler(this.EthnicityCheckBoxCaucasian_CheckedChanged);
            // 
            // EthnicityCheckBoxALL
            // 
            this.EthnicityCheckBoxALL.AutoSize = true;
            this.EthnicityCheckBoxALL.Depth = 0;
            this.EthnicityCheckBoxALL.Font = new System.Drawing.Font("Roboto", 10F);
            this.EthnicityCheckBoxALL.Location = new System.Drawing.Point(13, 5);
            this.EthnicityCheckBoxALL.Margin = new System.Windows.Forms.Padding(0);
            this.EthnicityCheckBoxALL.MouseLocation = new System.Drawing.Point(-1, -1);
            this.EthnicityCheckBoxALL.MouseState = MaterialSkin.MouseState.HOVER;
            this.EthnicityCheckBoxALL.Name = "EthnicityCheckBoxALL";
            this.EthnicityCheckBoxALL.Ripple = true;
            this.EthnicityCheckBoxALL.Size = new System.Drawing.Size(46, 30);
            this.EthnicityCheckBoxALL.TabIndex = 17;
            this.EthnicityCheckBoxALL.Text = "All";
            this.EthnicityCheckBoxALL.UseVisualStyleBackColor = true;
            this.EthnicityCheckBoxALL.CheckedChanged += new System.EventHandler(this.EthnicityCheckBoxALL_CheckedChanged_1);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.White;
            this.groupBox7.Controls.Add(this.panel4);
            this.groupBox7.Location = new System.Drawing.Point(314, 562);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(178, 156);
            this.groupBox7.TabIndex = 74;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Format";
            // 
            // panel4
            // 
            this.panel4.AutoScroll = true;
            this.panel4.Controls.Add(this.FormatCheckBoxFLV);
            this.panel4.Controls.Add(this.FormatCheckBoxRM);
            this.panel4.Controls.Add(this.FormatCheckBoxWMV);
            this.panel4.Controls.Add(this.FormatCheckBoxBGP);
            this.panel4.Controls.Add(this.FormatCheckBoxASF);
            this.panel4.Controls.Add(this.FormatCheckBoxAVI);
            this.panel4.Controls.Add(this.FormatCheckBoxMOV);
            this.panel4.Controls.Add(this.FormatCheckBoxMP4);
            this.panel4.Controls.Add(this.FormatCheckBoxMOB);
            this.panel4.Controls.Add(this.FormatCheckBoxMPG);
            this.panel4.Controls.Add(this.FormatCheckBoxALL);
            this.panel4.Location = new System.Drawing.Point(6, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(166, 131);
            this.panel4.TabIndex = 0;
            // 
            // FormatCheckBoxFLV
            // 
            this.FormatCheckBoxFLV.AutoSize = true;
            this.FormatCheckBoxFLV.Depth = 0;
            this.FormatCheckBoxFLV.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxFLV.Location = new System.Drawing.Point(12, 302);
            this.FormatCheckBoxFLV.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxFLV.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxFLV.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxFLV.Name = "FormatCheckBoxFLV";
            this.FormatCheckBoxFLV.Ripple = true;
            this.FormatCheckBoxFLV.Size = new System.Drawing.Size(54, 30);
            this.FormatCheckBoxFLV.TabIndex = 27;
            this.FormatCheckBoxFLV.Text = "FLV";
            this.FormatCheckBoxFLV.UseVisualStyleBackColor = true;
            this.FormatCheckBoxFLV.CheckedChanged += new System.EventHandler(this.FormatCheckBoxFLV_CheckedChanged);
            // 
            // FormatCheckBoxRM
            // 
            this.FormatCheckBoxRM.AutoSize = true;
            this.FormatCheckBoxRM.Depth = 0;
            this.FormatCheckBoxRM.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxRM.Location = new System.Drawing.Point(12, 272);
            this.FormatCheckBoxRM.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxRM.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxRM.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxRM.Name = "FormatCheckBoxRM";
            this.FormatCheckBoxRM.Ripple = true;
            this.FormatCheckBoxRM.Size = new System.Drawing.Size(51, 30);
            this.FormatCheckBoxRM.TabIndex = 26;
            this.FormatCheckBoxRM.Text = "RM";
            this.FormatCheckBoxRM.UseVisualStyleBackColor = true;
            this.FormatCheckBoxRM.CheckedChanged += new System.EventHandler(this.FormatCheckBoxRM_CheckedChanged);
            // 
            // FormatCheckBoxWMV
            // 
            this.FormatCheckBoxWMV.AutoSize = true;
            this.FormatCheckBoxWMV.Depth = 0;
            this.FormatCheckBoxWMV.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxWMV.Location = new System.Drawing.Point(12, 242);
            this.FormatCheckBoxWMV.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxWMV.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxWMV.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxWMV.Name = "FormatCheckBoxWMV";
            this.FormatCheckBoxWMV.Ripple = true;
            this.FormatCheckBoxWMV.Size = new System.Drawing.Size(63, 30);
            this.FormatCheckBoxWMV.TabIndex = 25;
            this.FormatCheckBoxWMV.Text = "WMV";
            this.FormatCheckBoxWMV.UseVisualStyleBackColor = true;
            this.FormatCheckBoxWMV.CheckedChanged += new System.EventHandler(this.FormatCheckBoxWMV_CheckedChanged);
            // 
            // FormatCheckBoxBGP
            // 
            this.FormatCheckBoxBGP.AutoSize = true;
            this.FormatCheckBoxBGP.Depth = 0;
            this.FormatCheckBoxBGP.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxBGP.Location = new System.Drawing.Point(12, 212);
            this.FormatCheckBoxBGP.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxBGP.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxBGP.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxBGP.Name = "FormatCheckBoxBGP";
            this.FormatCheckBoxBGP.Ripple = true;
            this.FormatCheckBoxBGP.Size = new System.Drawing.Size(57, 30);
            this.FormatCheckBoxBGP.TabIndex = 24;
            this.FormatCheckBoxBGP.Text = "BGP";
            this.FormatCheckBoxBGP.UseVisualStyleBackColor = true;
            this.FormatCheckBoxBGP.CheckedChanged += new System.EventHandler(this.FormatCheckBoxBGP_CheckedChanged);
            // 
            // FormatCheckBoxASF
            // 
            this.FormatCheckBoxASF.AutoSize = true;
            this.FormatCheckBoxASF.Depth = 0;
            this.FormatCheckBoxASF.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxASF.Location = new System.Drawing.Point(12, 182);
            this.FormatCheckBoxASF.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxASF.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxASF.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxASF.Name = "FormatCheckBoxASF";
            this.FormatCheckBoxASF.Ripple = true;
            this.FormatCheckBoxASF.Size = new System.Drawing.Size(55, 30);
            this.FormatCheckBoxASF.TabIndex = 23;
            this.FormatCheckBoxASF.Text = "ASF";
            this.FormatCheckBoxASF.UseVisualStyleBackColor = true;
            this.FormatCheckBoxASF.CheckedChanged += new System.EventHandler(this.FormatCheckBoxASF_CheckedChanged);
            // 
            // FormatCheckBoxAVI
            // 
            this.FormatCheckBoxAVI.AutoSize = true;
            this.FormatCheckBoxAVI.Depth = 0;
            this.FormatCheckBoxAVI.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxAVI.Location = new System.Drawing.Point(13, 155);
            this.FormatCheckBoxAVI.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxAVI.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxAVI.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxAVI.Name = "FormatCheckBoxAVI";
            this.FormatCheckBoxAVI.Ripple = true;
            this.FormatCheckBoxAVI.Size = new System.Drawing.Size(52, 30);
            this.FormatCheckBoxAVI.TabIndex = 22;
            this.FormatCheckBoxAVI.Text = "AVI";
            this.FormatCheckBoxAVI.UseVisualStyleBackColor = true;
            this.FormatCheckBoxAVI.CheckedChanged += new System.EventHandler(this.FormatCheckBoxAVI_CheckedChanged);
            // 
            // FormatCheckBoxMOV
            // 
            this.FormatCheckBoxMOV.AutoSize = true;
            this.FormatCheckBoxMOV.Depth = 0;
            this.FormatCheckBoxMOV.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxMOV.Location = new System.Drawing.Point(13, 125);
            this.FormatCheckBoxMOV.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxMOV.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxMOV.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxMOV.Name = "FormatCheckBoxMOV";
            this.FormatCheckBoxMOV.Ripple = true;
            this.FormatCheckBoxMOV.Size = new System.Drawing.Size(60, 30);
            this.FormatCheckBoxMOV.TabIndex = 21;
            this.FormatCheckBoxMOV.Text = "MOV";
            this.FormatCheckBoxMOV.UseVisualStyleBackColor = true;
            // 
            // FormatCheckBoxMP4
            // 
            this.FormatCheckBoxMP4.AutoSize = true;
            this.FormatCheckBoxMP4.Depth = 0;
            this.FormatCheckBoxMP4.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxMP4.Location = new System.Drawing.Point(13, 95);
            this.FormatCheckBoxMP4.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxMP4.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxMP4.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxMP4.Name = "FormatCheckBoxMP4";
            this.FormatCheckBoxMP4.Ripple = true;
            this.FormatCheckBoxMP4.Size = new System.Drawing.Size(59, 30);
            this.FormatCheckBoxMP4.TabIndex = 20;
            this.FormatCheckBoxMP4.Text = "MP4";
            this.FormatCheckBoxMP4.UseVisualStyleBackColor = true;
            this.FormatCheckBoxMP4.CheckedChanged += new System.EventHandler(this.FormatCheckBoxMP4_CheckedChanged_1);
            // 
            // FormatCheckBoxMOB
            // 
            this.FormatCheckBoxMOB.AutoSize = true;
            this.FormatCheckBoxMOB.Depth = 0;
            this.FormatCheckBoxMOB.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxMOB.Location = new System.Drawing.Point(13, 65);
            this.FormatCheckBoxMOB.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxMOB.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxMOB.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxMOB.Name = "FormatCheckBoxMOB";
            this.FormatCheckBoxMOB.Ripple = true;
            this.FormatCheckBoxMOB.Size = new System.Drawing.Size(60, 30);
            this.FormatCheckBoxMOB.TabIndex = 19;
            this.FormatCheckBoxMOB.Text = "MOB";
            this.FormatCheckBoxMOB.UseVisualStyleBackColor = true;
            this.FormatCheckBoxMOB.CheckedChanged += new System.EventHandler(this.FormatCheckBoxMOB_CheckedChanged);
            // 
            // FormatCheckBoxMPG
            // 
            this.FormatCheckBoxMPG.AutoSize = true;
            this.FormatCheckBoxMPG.Depth = 0;
            this.FormatCheckBoxMPG.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxMPG.Location = new System.Drawing.Point(13, 35);
            this.FormatCheckBoxMPG.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxMPG.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxMPG.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxMPG.Name = "FormatCheckBoxMPG";
            this.FormatCheckBoxMPG.Ripple = true;
            this.FormatCheckBoxMPG.Size = new System.Drawing.Size(60, 30);
            this.FormatCheckBoxMPG.TabIndex = 18;
            this.FormatCheckBoxMPG.Text = "MPG";
            this.FormatCheckBoxMPG.UseVisualStyleBackColor = true;
            this.FormatCheckBoxMPG.CheckedChanged += new System.EventHandler(this.FormatCheckBoxMPG_CheckedChanged);
            // 
            // FormatCheckBoxALL
            // 
            this.FormatCheckBoxALL.AutoSize = true;
            this.FormatCheckBoxALL.Depth = 0;
            this.FormatCheckBoxALL.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormatCheckBoxALL.Location = new System.Drawing.Point(13, 5);
            this.FormatCheckBoxALL.Margin = new System.Windows.Forms.Padding(0);
            this.FormatCheckBoxALL.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormatCheckBoxALL.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormatCheckBoxALL.Name = "FormatCheckBoxALL";
            this.FormatCheckBoxALL.Ripple = true;
            this.FormatCheckBoxALL.Size = new System.Drawing.Size(46, 30);
            this.FormatCheckBoxALL.TabIndex = 17;
            this.FormatCheckBoxALL.Text = "All";
            this.FormatCheckBoxALL.UseVisualStyleBackColor = true;
            this.FormatCheckBoxALL.CheckedChanged += new System.EventHandler(this.FormatCheckBoxALL_CheckedChanged_1);
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.White;
            this.groupBox11.Controls.Add(this.WithMoustachelRadioButtonA);
            this.groupBox11.Controls.Add(this.WithMoustacheRadioButtonY);
            this.groupBox11.Controls.Add(this.WithMoustacheRadioButtonN);
            this.groupBox11.Location = new System.Drawing.Point(55, 250);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(206, 50);
            this.groupBox11.TabIndex = 75;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "With Moustache";
            // 
            // WithMoustachelRadioButtonA
            // 
            this.WithMoustachelRadioButtonA.AccessibleName = "";
            this.WithMoustachelRadioButtonA.AutoSize = true;
            this.WithMoustachelRadioButtonA.Checked = true;
            this.WithMoustachelRadioButtonA.Depth = 0;
            this.WithMoustachelRadioButtonA.Font = new System.Drawing.Font("Roboto", 10F);
            this.WithMoustachelRadioButtonA.Location = new System.Drawing.Point(8, 16);
            this.WithMoustachelRadioButtonA.Margin = new System.Windows.Forms.Padding(0);
            this.WithMoustachelRadioButtonA.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WithMoustachelRadioButtonA.MouseState = MaterialSkin.MouseState.HOVER;
            this.WithMoustachelRadioButtonA.Name = "WithMoustachelRadioButtonA";
            this.WithMoustachelRadioButtonA.Ripple = true;
            this.WithMoustachelRadioButtonA.Size = new System.Drawing.Size(45, 30);
            this.WithMoustachelRadioButtonA.TabIndex = 0;
            this.WithMoustachelRadioButtonA.TabStop = true;
            this.WithMoustachelRadioButtonA.Text = "All";
            this.WithMoustachelRadioButtonA.UseVisualStyleBackColor = true;
            // 
            // WithMoustacheRadioButtonY
            // 
            this.WithMoustacheRadioButtonY.AccessibleName = "Gender";
            this.WithMoustacheRadioButtonY.AutoSize = true;
            this.WithMoustacheRadioButtonY.BackColor = System.Drawing.Color.Black;
            this.WithMoustacheRadioButtonY.Depth = 0;
            this.WithMoustacheRadioButtonY.Font = new System.Drawing.Font("Roboto", 10F);
            this.WithMoustacheRadioButtonY.Location = new System.Drawing.Point(60, 16);
            this.WithMoustacheRadioButtonY.Margin = new System.Windows.Forms.Padding(0);
            this.WithMoustacheRadioButtonY.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WithMoustacheRadioButtonY.MouseState = MaterialSkin.MouseState.HOVER;
            this.WithMoustacheRadioButtonY.Name = "WithMoustacheRadioButtonY";
            this.WithMoustacheRadioButtonY.Ripple = true;
            this.WithMoustacheRadioButtonY.Size = new System.Drawing.Size(52, 30);
            this.WithMoustacheRadioButtonY.TabIndex = 2;
            this.WithMoustacheRadioButtonY.Text = "Yes";
            this.WithMoustacheRadioButtonY.UseVisualStyleBackColor = false;
            this.WithMoustacheRadioButtonY.CheckedChanged += new System.EventHandler(this.WithMoustacheRadioButtonY_CheckedChanged);
            // 
            // WithMoustacheRadioButtonN
            // 
            this.WithMoustacheRadioButtonN.AccessibleName = "Gender";
            this.WithMoustacheRadioButtonN.AutoSize = true;
            this.WithMoustacheRadioButtonN.BackColor = System.Drawing.Color.Black;
            this.WithMoustacheRadioButtonN.Depth = 0;
            this.WithMoustacheRadioButtonN.Font = new System.Drawing.Font("Roboto", 10F);
            this.WithMoustacheRadioButtonN.Location = new System.Drawing.Point(121, 16);
            this.WithMoustacheRadioButtonN.Margin = new System.Windows.Forms.Padding(0);
            this.WithMoustacheRadioButtonN.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WithMoustacheRadioButtonN.MouseState = MaterialSkin.MouseState.HOVER;
            this.WithMoustacheRadioButtonN.Name = "WithMoustacheRadioButtonN";
            this.WithMoustacheRadioButtonN.Ripple = true;
            this.WithMoustacheRadioButtonN.Size = new System.Drawing.Size(47, 30);
            this.WithMoustacheRadioButtonN.TabIndex = 3;
            this.WithMoustacheRadioButtonN.Text = "No";
            this.WithMoustacheRadioButtonN.UseVisualStyleBackColor = false;
            this.WithMoustacheRadioButtonN.CheckedChanged += new System.EventHandler(this.WithMoustacheRadioButtonN_CheckedChanged);
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.White;
            this.groupBox14.Controls.Add(this.WithBearedRadioButtonA);
            this.groupBox14.Controls.Add(this.WithBearedRadioButtonY);
            this.groupBox14.Controls.Add(this.WithBearedRadioButtonN);
            this.groupBox14.Location = new System.Drawing.Point(55, 315);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(206, 50);
            this.groupBox14.TabIndex = 76;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "With Beared";
            // 
            // WithBearedRadioButtonA
            // 
            this.WithBearedRadioButtonA.AccessibleName = "";
            this.WithBearedRadioButtonA.AutoSize = true;
            this.WithBearedRadioButtonA.Checked = true;
            this.WithBearedRadioButtonA.Depth = 0;
            this.WithBearedRadioButtonA.Font = new System.Drawing.Font("Roboto", 10F);
            this.WithBearedRadioButtonA.Location = new System.Drawing.Point(8, 16);
            this.WithBearedRadioButtonA.Margin = new System.Windows.Forms.Padding(0);
            this.WithBearedRadioButtonA.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WithBearedRadioButtonA.MouseState = MaterialSkin.MouseState.HOVER;
            this.WithBearedRadioButtonA.Name = "WithBearedRadioButtonA";
            this.WithBearedRadioButtonA.Ripple = true;
            this.WithBearedRadioButtonA.Size = new System.Drawing.Size(45, 30);
            this.WithBearedRadioButtonA.TabIndex = 0;
            this.WithBearedRadioButtonA.TabStop = true;
            this.WithBearedRadioButtonA.Text = "All";
            this.WithBearedRadioButtonA.UseVisualStyleBackColor = true;
            // 
            // WithBearedRadioButtonY
            // 
            this.WithBearedRadioButtonY.AccessibleName = "Gender";
            this.WithBearedRadioButtonY.AutoSize = true;
            this.WithBearedRadioButtonY.BackColor = System.Drawing.Color.Black;
            this.WithBearedRadioButtonY.Depth = 0;
            this.WithBearedRadioButtonY.Font = new System.Drawing.Font("Roboto", 10F);
            this.WithBearedRadioButtonY.Location = new System.Drawing.Point(60, 16);
            this.WithBearedRadioButtonY.Margin = new System.Windows.Forms.Padding(0);
            this.WithBearedRadioButtonY.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WithBearedRadioButtonY.MouseState = MaterialSkin.MouseState.HOVER;
            this.WithBearedRadioButtonY.Name = "WithBearedRadioButtonY";
            this.WithBearedRadioButtonY.Ripple = true;
            this.WithBearedRadioButtonY.Size = new System.Drawing.Size(52, 30);
            this.WithBearedRadioButtonY.TabIndex = 2;
            this.WithBearedRadioButtonY.Text = "Yes";
            this.WithBearedRadioButtonY.UseVisualStyleBackColor = false;
            this.WithBearedRadioButtonY.CheckedChanged += new System.EventHandler(this.WithBearedRadioButtonY_CheckedChanged);
            // 
            // WithBearedRadioButtonN
            // 
            this.WithBearedRadioButtonN.AccessibleName = "Gender";
            this.WithBearedRadioButtonN.AutoSize = true;
            this.WithBearedRadioButtonN.BackColor = System.Drawing.Color.Black;
            this.WithBearedRadioButtonN.Depth = 0;
            this.WithBearedRadioButtonN.Font = new System.Drawing.Font("Roboto", 10F);
            this.WithBearedRadioButtonN.Location = new System.Drawing.Point(121, 16);
            this.WithBearedRadioButtonN.Margin = new System.Windows.Forms.Padding(0);
            this.WithBearedRadioButtonN.MouseLocation = new System.Drawing.Point(-1, -1);
            this.WithBearedRadioButtonN.MouseState = MaterialSkin.MouseState.HOVER;
            this.WithBearedRadioButtonN.Name = "WithBearedRadioButtonN";
            this.WithBearedRadioButtonN.Ripple = true;
            this.WithBearedRadioButtonN.Size = new System.Drawing.Size(47, 30);
            this.WithBearedRadioButtonN.TabIndex = 3;
            this.WithBearedRadioButtonN.Text = "No";
            this.WithBearedRadioButtonN.UseVisualStyleBackColor = false;
            this.WithBearedRadioButtonN.CheckedChanged += new System.EventHandler(this.WithBearedRadioButtonN_CheckedChanged);
            // 
            // groupBox15
            // 
            this.groupBox15.BackColor = System.Drawing.Color.White;
            this.groupBox15.Controls.Add(this.IncludeVoiceRadioButtonA);
            this.groupBox15.Controls.Add(this.IncludeVoiceRadioButtonY);
            this.groupBox15.Controls.Add(this.IncludeVoiceRadioButtonN);
            this.groupBox15.Location = new System.Drawing.Point(310, 129);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(182, 50);
            this.groupBox15.TabIndex = 77;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Include Voice";
            // 
            // IncludeVoiceRadioButtonA
            // 
            this.IncludeVoiceRadioButtonA.AccessibleName = "";
            this.IncludeVoiceRadioButtonA.AutoSize = true;
            this.IncludeVoiceRadioButtonA.Checked = true;
            this.IncludeVoiceRadioButtonA.Depth = 0;
            this.IncludeVoiceRadioButtonA.Font = new System.Drawing.Font("Roboto", 10F);
            this.IncludeVoiceRadioButtonA.Location = new System.Drawing.Point(8, 16);
            this.IncludeVoiceRadioButtonA.Margin = new System.Windows.Forms.Padding(0);
            this.IncludeVoiceRadioButtonA.MouseLocation = new System.Drawing.Point(-1, -1);
            this.IncludeVoiceRadioButtonA.MouseState = MaterialSkin.MouseState.HOVER;
            this.IncludeVoiceRadioButtonA.Name = "IncludeVoiceRadioButtonA";
            this.IncludeVoiceRadioButtonA.Ripple = true;
            this.IncludeVoiceRadioButtonA.Size = new System.Drawing.Size(45, 30);
            this.IncludeVoiceRadioButtonA.TabIndex = 0;
            this.IncludeVoiceRadioButtonA.TabStop = true;
            this.IncludeVoiceRadioButtonA.Text = "All";
            this.IncludeVoiceRadioButtonA.UseVisualStyleBackColor = true;
            // 
            // IncludeVoiceRadioButtonY
            // 
            this.IncludeVoiceRadioButtonY.AccessibleName = "Gender";
            this.IncludeVoiceRadioButtonY.AutoSize = true;
            this.IncludeVoiceRadioButtonY.BackColor = System.Drawing.Color.Black;
            this.IncludeVoiceRadioButtonY.Depth = 0;
            this.IncludeVoiceRadioButtonY.Font = new System.Drawing.Font("Roboto", 10F);
            this.IncludeVoiceRadioButtonY.Location = new System.Drawing.Point(60, 16);
            this.IncludeVoiceRadioButtonY.Margin = new System.Windows.Forms.Padding(0);
            this.IncludeVoiceRadioButtonY.MouseLocation = new System.Drawing.Point(-1, -1);
            this.IncludeVoiceRadioButtonY.MouseState = MaterialSkin.MouseState.HOVER;
            this.IncludeVoiceRadioButtonY.Name = "IncludeVoiceRadioButtonY";
            this.IncludeVoiceRadioButtonY.Ripple = true;
            this.IncludeVoiceRadioButtonY.Size = new System.Drawing.Size(52, 30);
            this.IncludeVoiceRadioButtonY.TabIndex = 2;
            this.IncludeVoiceRadioButtonY.Text = "Yes";
            this.IncludeVoiceRadioButtonY.UseVisualStyleBackColor = false;
            this.IncludeVoiceRadioButtonY.CheckedChanged += new System.EventHandler(this.IncludeVoiceRadioButtonY_CheckedChanged);
            // 
            // IncludeVoiceRadioButtonN
            // 
            this.IncludeVoiceRadioButtonN.AccessibleName = "Gender";
            this.IncludeVoiceRadioButtonN.AutoSize = true;
            this.IncludeVoiceRadioButtonN.BackColor = System.Drawing.Color.Black;
            this.IncludeVoiceRadioButtonN.Depth = 0;
            this.IncludeVoiceRadioButtonN.Font = new System.Drawing.Font("Roboto", 10F);
            this.IncludeVoiceRadioButtonN.Location = new System.Drawing.Point(121, 16);
            this.IncludeVoiceRadioButtonN.Margin = new System.Windows.Forms.Padding(0);
            this.IncludeVoiceRadioButtonN.MouseLocation = new System.Drawing.Point(-1, -1);
            this.IncludeVoiceRadioButtonN.MouseState = MaterialSkin.MouseState.HOVER;
            this.IncludeVoiceRadioButtonN.Name = "IncludeVoiceRadioButtonN";
            this.IncludeVoiceRadioButtonN.Ripple = true;
            this.IncludeVoiceRadioButtonN.Size = new System.Drawing.Size(47, 30);
            this.IncludeVoiceRadioButtonN.TabIndex = 3;
            this.IncludeVoiceRadioButtonN.Text = "No";
            this.IncludeVoiceRadioButtonN.UseVisualStyleBackColor = false;
            this.IncludeVoiceRadioButtonN.CheckedChanged += new System.EventHandler(this.IncludeVoiceRadioButtonN_CheckedChanged);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(54, 734);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(86, 36);
            this.materialRaisedButton1.TabIndex = 78;
            this.materialRaisedButton1.Text = "Previous";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.AutoSize = true;
            this.materialRaisedButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Icon = null;
            this.materialRaisedButton2.Location = new System.Drawing.Point(867, 734);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(73, 36);
            this.materialRaisedButton2.TabIndex = 79;
            this.materialRaisedButton2.Text = "Search";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(59, 98);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(110, 19);
            this.materialLabel7.TabIndex = 80;
            this.materialLabel7.Text = "Subject Details";
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(311, 98);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(107, 19);
            this.materialLabel8.TabIndex = 81;
            this.materialLabel8.Text = "Videos Details";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.panel5);
            this.groupBox3.Location = new System.Drawing.Point(313, 376);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(179, 168);
            this.groupBox3.TabIndex = 82;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resolution";
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.ResolutionCheckBoxHIGH);
            this.panel5.Controls.Add(this.ResolutionCheckBoxMID);
            this.panel5.Controls.Add(this.ResolutionCheckBoxLOW);
            this.panel5.Controls.Add(this.ResolutionCheckBoxALL);
            this.panel5.Location = new System.Drawing.Point(6, 16);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(166, 128);
            this.panel5.TabIndex = 0;
            // 
            // ResolutionCheckBoxHIGH
            // 
            this.ResolutionCheckBoxHIGH.AutoSize = true;
            this.ResolutionCheckBoxHIGH.Depth = 0;
            this.ResolutionCheckBoxHIGH.Font = new System.Drawing.Font("Roboto", 10F);
            this.ResolutionCheckBoxHIGH.Location = new System.Drawing.Point(14, 95);
            this.ResolutionCheckBoxHIGH.Margin = new System.Windows.Forms.Padding(0);
            this.ResolutionCheckBoxHIGH.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ResolutionCheckBoxHIGH.MouseState = MaterialSkin.MouseState.HOVER;
            this.ResolutionCheckBoxHIGH.Name = "ResolutionCheckBoxHIGH";
            this.ResolutionCheckBoxHIGH.Ripple = true;
            this.ResolutionCheckBoxHIGH.Size = new System.Drawing.Size(59, 30);
            this.ResolutionCheckBoxHIGH.TabIndex = 20;
            this.ResolutionCheckBoxHIGH.Text = "High";
            this.ResolutionCheckBoxHIGH.UseVisualStyleBackColor = true;
            this.ResolutionCheckBoxHIGH.CheckedChanged += new System.EventHandler(this.ResolutionCheckBoxHIGH_CheckedChanged);
            // 
            // ResolutionCheckBoxMID
            // 
            this.ResolutionCheckBoxMID.AutoSize = true;
            this.ResolutionCheckBoxMID.Depth = 0;
            this.ResolutionCheckBoxMID.Font = new System.Drawing.Font("Roboto", 10F);
            this.ResolutionCheckBoxMID.Location = new System.Drawing.Point(13, 65);
            this.ResolutionCheckBoxMID.Margin = new System.Windows.Forms.Padding(0);
            this.ResolutionCheckBoxMID.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ResolutionCheckBoxMID.MouseState = MaterialSkin.MouseState.HOVER;
            this.ResolutionCheckBoxMID.Name = "ResolutionCheckBoxMID";
            this.ResolutionCheckBoxMID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ResolutionCheckBoxMID.Ripple = true;
            this.ResolutionCheckBoxMID.Size = new System.Drawing.Size(80, 30);
            this.ResolutionCheckBoxMID.TabIndex = 19;
            this.ResolutionCheckBoxMID.Text = "Medium";
            this.ResolutionCheckBoxMID.UseVisualStyleBackColor = true;
            this.ResolutionCheckBoxMID.CheckedChanged += new System.EventHandler(this.ResolutionCheckBoxMID_CheckedChanged);
            // 
            // ResolutionCheckBoxLOW
            // 
            this.ResolutionCheckBoxLOW.AutoSize = true;
            this.ResolutionCheckBoxLOW.Depth = 0;
            this.ResolutionCheckBoxLOW.Font = new System.Drawing.Font("Roboto", 10F);
            this.ResolutionCheckBoxLOW.Location = new System.Drawing.Point(13, 35);
            this.ResolutionCheckBoxLOW.Margin = new System.Windows.Forms.Padding(0);
            this.ResolutionCheckBoxLOW.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ResolutionCheckBoxLOW.MouseState = MaterialSkin.MouseState.HOVER;
            this.ResolutionCheckBoxLOW.Name = "ResolutionCheckBoxLOW";
            this.ResolutionCheckBoxLOW.Ripple = true;
            this.ResolutionCheckBoxLOW.Size = new System.Drawing.Size(55, 30);
            this.ResolutionCheckBoxLOW.TabIndex = 18;
            this.ResolutionCheckBoxLOW.Text = "Low";
            this.ResolutionCheckBoxLOW.UseVisualStyleBackColor = true;
            this.ResolutionCheckBoxLOW.CheckedChanged += new System.EventHandler(this.ResolutionCheckBoxLOW_CheckedChanged_1);
            // 
            // ResolutionCheckBoxALL
            // 
            this.ResolutionCheckBoxALL.AutoSize = true;
            this.ResolutionCheckBoxALL.Depth = 0;
            this.ResolutionCheckBoxALL.Font = new System.Drawing.Font("Roboto", 10F);
            this.ResolutionCheckBoxALL.Location = new System.Drawing.Point(13, 5);
            this.ResolutionCheckBoxALL.Margin = new System.Windows.Forms.Padding(0);
            this.ResolutionCheckBoxALL.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ResolutionCheckBoxALL.MouseState = MaterialSkin.MouseState.HOVER;
            this.ResolutionCheckBoxALL.Name = "ResolutionCheckBoxALL";
            this.ResolutionCheckBoxALL.Ripple = true;
            this.ResolutionCheckBoxALL.Size = new System.Drawing.Size(46, 30);
            this.ResolutionCheckBoxALL.TabIndex = 17;
            this.ResolutionCheckBoxALL.Text = "All";
            this.ResolutionCheckBoxALL.UseVisualStyleBackColor = true;
            this.ResolutionCheckBoxALL.CheckedChanged += new System.EventHandler(this.ResolutionCheckBoxALL_CheckedChanged);
            // 
            // retriveDataFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1000, 800);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.groupBox14);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox10);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.MaximizeBox = false;
            this.Name = "retriveDataFilterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Corpus Filter";
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DurationUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurationUpDown1)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QualityUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualityUpDown1)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown DurationUpDown1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.NumericUpDown QualityUpDown2;
        private System.Windows.Forms.NumericUpDown QualityUpDown1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private MaterialSkin.Controls.MaterialRadioButton WearingGlassesRadioButtonA;
        private MaterialSkin.Controls.MaterialRadioButton WearingGlassesRadioButtonY;
        private MaterialSkin.Controls.MaterialRadioButton WearingGlassesRadioButtonN;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox4150;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox3140;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox2130;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox1120;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox010;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox1;
        private MaterialSkin.Controls.MaterialRadioButton GenderRadioButtonA;
        private MaterialSkin.Controls.MaterialRadioButton GenderRadioButtonM;
        private MaterialSkin.Controls.MaterialRadioButton GenderRadioButtonF;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel2;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxFEED;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxDenver;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBox3;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBox4;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBox5;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxALL;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Panel panel3;
        private MaterialSkin.Controls.MaterialCheckBox EthnicityCheckBoxOther;
        private MaterialSkin.Controls.MaterialCheckBox EthnicityCheckBoxSouthAsia;
        private MaterialSkin.Controls.MaterialCheckBox EthnicityCheckBoxAfrican;
        private MaterialSkin.Controls.MaterialCheckBox EthnicityCheckBoxALL;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel panel4;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxAVI;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxMOV;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxMP4;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxMOB;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxMPG;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxALL;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private System.Windows.Forms.GroupBox groupBox11;
        private MaterialSkin.Controls.MaterialRadioButton WithMoustachelRadioButtonA;
        private MaterialSkin.Controls.MaterialRadioButton WithMoustacheRadioButtonY;
        private MaterialSkin.Controls.MaterialRadioButton WithMoustacheRadioButtonN;
        private System.Windows.Forms.GroupBox groupBox14;
        private MaterialSkin.Controls.MaterialRadioButton WithBearedRadioButtonA;
        private MaterialSkin.Controls.MaterialRadioButton WithBearedRadioButtonY;
        private MaterialSkin.Controls.MaterialRadioButton WithBearedRadioButtonN;
        private System.Windows.Forms.GroupBox groupBox15;
        private MaterialSkin.Controls.MaterialRadioButton IncludeVoiceRadioButtonA;
        private MaterialSkin.Controls.MaterialRadioButton IncludeVoiceRadioButtonY;
        private MaterialSkin.Controls.MaterialRadioButton IncludeVoiceRadioButtonN;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBoxOVER80;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox5160;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox6170;
        private MaterialSkin.Controls.MaterialCheckBox AgeRangeCheckBox7180;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxFLV;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxRM;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxWMV;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxBGP;
        private MaterialSkin.Controls.MaterialCheckBox FormatCheckBoxASF;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxUNBC;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxBimodal;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxOther;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialCheckBox EthnicityCheckBoxEasternAsia;
        private MaterialSkin.Controls.MaterialCheckBox EthnicityCheckBoxCaucasian;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel5;
        private MaterialSkin.Controls.MaterialCheckBox ResolutionCheckBoxHIGH;
        private MaterialSkin.Controls.MaterialCheckBox ResolutionCheckBoxMID;
        private MaterialSkin.Controls.MaterialCheckBox ResolutionCheckBoxLOW;
        private MaterialSkin.Controls.MaterialCheckBox ResolutionCheckBoxALL;
        private System.Windows.Forms.ComboBox FrameRatecomboBox2;
        private System.Windows.Forms.ComboBox FrameRatecomboBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxMMI;
        private System.Windows.Forms.NumericUpDown DurationUpDown2;
        private MaterialSkin.Controls.MaterialCheckBox SubDatabaseCheckBoxCK;
    }
}