﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace MetaFace
{
    public partial class retriveDataFilterForm : MaterialForm
    {
        private Boolean isRetriveForm = true;
        private String Gender = "0";
        private String hasGlasses = "0";
        private String hasMoustache = "0";
        private String hasBeard = "0";
        private String EstimatedAge = "0";
        private String Ethnicity = "0";
        private String hasVoice = "0";

        private String Duration = "0";
        private String ImageQuality = "0";
        private String FrameRate = "0";

        //private Double Duration = 0;
        //private Double ImageQuality = 0;
        //private Double FrameRate = 0;
        private String Resolution = "0";
        private String Format = "0";
        private String FromSubDB = "0";

        //form Constructor - intiat the form and set defult valuse
        public retriveDataFilterForm(bool isRetriveForm)
        {
            InitializeComponent();
            this.isRetriveForm = isRetriveForm;
            ResolutionCheckBoxALL.Checked = true;
            ResolutionCheckBoxHIGH.Checked = true;
            ResolutionCheckBoxMID.Checked = true;
            ResolutionCheckBoxLOW.Checked = true;
            AgeRangeCheckBox1.Checked = true;
            AgeRangeCheckBox010.Checked = true;
            AgeRangeCheckBox1120.Checked = true;
            AgeRangeCheckBox2130.Checked = true;
            AgeRangeCheckBox3140.Checked = true;
            AgeRangeCheckBox4150.Checked = true;
            AgeRangeCheckBox5160.Checked = true;
            AgeRangeCheckBox6170.Checked = true;
            AgeRangeCheckBox7180.Checked = true;
            AgeRangeCheckBoxOVER80.Checked = true;
            EthnicityCheckBoxALL.Checked = true;
            EthnicityCheckBoxAfrican.Checked = true;
            EthnicityCheckBoxCaucasian.Checked = true;
            EthnicityCheckBoxEasternAsia.Checked = true;
            EthnicityCheckBoxSouthAsia.Checked = true;
            EthnicityCheckBoxOther.Checked = true;
            FormatCheckBoxALL.Checked = true;
            FormatCheckBoxASF.Checked = true;
            FormatCheckBoxAVI.Checked = true;
            FormatCheckBoxBGP.Checked = true;
            FormatCheckBoxFLV.Checked = true;
            FormatCheckBoxMOB.Checked = true;
            FormatCheckBoxMP4.Checked = true;
            FormatCheckBoxMPG.Checked = true;
            FormatCheckBoxRM.Checked = true;
            FormatCheckBoxWMV.Checked = true;
            SubDatabaseCheckBoxALL.Checked = true;
            SubDatabaseCheckBox3.Checked = true;
            SubDatabaseCheckBox4.Checked = true;
            SubDatabaseCheckBox5.Checked = true;
            SubDatabaseCheckBoxCK.Checked = true;
            SubDatabaseCheckBoxBimodal.Checked = true;
            SubDatabaseCheckBoxDenver.Checked = true;
            SubDatabaseCheckBoxFEED.Checked = true;
            SubDatabaseCheckBoxUNBC.Checked = true;
            SubDatabaseCheckBoxMMI.Checked = true;
            SubDatabaseCheckBoxOther.Checked = true;
            FrameRatecomboBox1.SelectedIndex = 0;
            FrameRatecomboBox2.SelectedIndex = 9;
            QualityUpDown1.Value = 0;
            QualityUpDown2.Value = 100;
            DurationUpDown1.Value = 0;
            DurationUpDown2.Value = 10;
            this.Sizable = false;

        }//retriveDataFilterForm


        //"Search" button- run the select queary and open next windows
        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            if (!isAllUnchecked())
            {
               //Program.connectToServer();//just for testing

                SqlCommand selectSqlCmd = new SqlCommand();
                selectSqlCmd.Connection = Program.getgCnn();//just for testing
                                                            //selectSqlCmd.Connection = Program.gCnn;           //replace to this line after testing 
         
                var select = "SELECT * , SubDB.Name AS subDBName FROM [CORPUS].[dbo].[Video_Analysis] JOIN SubDB ON(Video_Analysis.FromSubDB= SubDB.ID)" + this.stringBuilder();

               // var select = "SELECT * , SubDB.Name AS subDBName FROM [CORPUS].[dbo].[Video_Analysis]" + this.stringBuilder();       
                var dataAdapter = new SqlDataAdapter(select, Program.getgCnn());
                var commandBuilder = new SqlCommandBuilder(dataAdapter);
                var ds = new DataSet();

                dataAdapter.FillSchema(ds, SchemaType.Mapped);
                dataAdapter.Fill(ds);

                if (isRetriveForm)
                {
                    
                   
                    retriveExportForm retriveExportForm1 = new retriveExportForm(ds);
                    retriveExportForm1.Show();
                    this.Hide();
                }
                else
                {
                    editDataForm editDataForm1 = new editDataForm(ds);
                    editDataForm1.Show();
                    this.Hide();
                }
            }
            else
            {
            }
        }//search button

        //Test whether the users unchecked all the filters in a specific criteria and ask the user to fill if needed
        public bool isAllUnchecked()
        {
            bool allUnChecked = false;
            string[] AllUncheckedFilters = new string[5];

            if ((!ResolutionCheckBoxALL.Checked) && (!ResolutionCheckBoxHIGH.Checked) && (!ResolutionCheckBoxMID.Checked) && (!ResolutionCheckBoxLOW.Checked))
            { AllUncheckedFilters[0] = "Resolution"; allUnChecked = true; }
            if ((!AgeRangeCheckBox010.Checked) && (!AgeRangeCheckBox1120.Checked) && (!AgeRangeCheckBox2130.Checked) && (!AgeRangeCheckBox3140.Checked) 
                && (!AgeRangeCheckBox4150.Checked) && (!AgeRangeCheckBox5160.Checked) && (!AgeRangeCheckBox6170.Checked) && (!AgeRangeCheckBox7180.Checked) && (!AgeRangeCheckBoxOVER80.Checked))
            { AllUncheckedFilters[1] = "Age Range"; allUnChecked = true;}
            if ((!EthnicityCheckBoxALL.Checked) && (!EthnicityCheckBoxAfrican.Checked) && (!EthnicityCheckBoxCaucasian.Checked) && (!EthnicityCheckBoxEasternAsia.Checked)
                && (!EthnicityCheckBoxSouthAsia.Checked) && (!EthnicityCheckBoxOther.Checked))
            { AllUncheckedFilters[2] = "Ethnicity"; allUnChecked = true;}
            if ((!FormatCheckBoxALL.Checked) && (!FormatCheckBoxASF.Checked) && (!FormatCheckBoxAVI.Checked) && (!FormatCheckBoxBGP.Checked)
                && (!FormatCheckBoxFLV.Checked) && (!FormatCheckBoxMOB.Checked) && (!FormatCheckBoxMOB.Checked) && (!FormatCheckBoxMOV.Checked)
                && (!FormatCheckBoxMP4.Checked) && (!FormatCheckBoxMPG.Checked) && (!FormatCheckBoxRM.Checked) && (!FormatCheckBoxWMV.Checked))
            { AllUncheckedFilters[3] = "Format"; allUnChecked = true;}
            if ((!SubDatabaseCheckBoxALL.Checked) && (!SubDatabaseCheckBox3.Checked) && (!SubDatabaseCheckBox4.Checked) && (!SubDatabaseCheckBox5.Checked)
                && (!SubDatabaseCheckBoxBimodal.Checked) && (!SubDatabaseCheckBoxDenver.Checked) && (!SubDatabaseCheckBoxFEED.Checked) && (!SubDatabaseCheckBoxMMI.Checked)
                && (!SubDatabaseCheckBoxUNBC.Checked) && (!SubDatabaseCheckBoxOther.Checked) && (!SubDatabaseCheckBoxCK.Checked))
            { AllUncheckedFilters[4] = "Sub Database"; allUnChecked = true;}

            if(allUnChecked)
            {
                string emptyCriteria = "";
                for (int i = 0; i <= AllUncheckedFilters.Length - 1; i++)
                   if (AllUncheckedFilters[i]!= null)
                    emptyCriteria += AllUncheckedFilters[i] + "\n";

                    MessageBox.Show("Please fill the following criteria:" + "\n" + emptyCriteria);
            }//if allUnChecked
            return allUnChecked;
        }//isAllUnchecked

        //create string for the WHERE condition which will take place in the SELECT SQL query
        public string stringBuilder()
        {
            string query = "";
            bool hasFilters = false;
            string[] filters = new string[13];  //creating string array that contain all the filters cretiria 
                                                //testing
                                                //this.Gender = "Gender='Male'";
            this.Format = BuildFormatQuery();                                    // this.hasGlasses = "hasGlasses=yes";
            this.Resolution = BuildResolutioQuery();
            this.Ethnicity = BuildEthnicityQuery();

            this.Duration = BuildDurationQuery();
            this.ImageQuality = BuildQualityQuery();                     
            this.FrameRate = BuildFrameRateQuery();
            this.EstimatedAge = BuildAgeWhereQuery();          
            this.FromSubDB=BuildSubDBQuery();                  
            filters[0] = this.Gender;
            filters[1] = this.hasBeard;
            filters[2] = this.EstimatedAge;
            filters[3] = this.Ethnicity;
            filters[4] = this.hasVoice;
            filters[5] = this.hasGlasses;
            filters[6] = this.Resolution;
            filters[7] = this.Format;
            filters[8] = this.FromSubDB;
            filters[9] = this.hasMoustache;
            filters[10] = this.Duration;
            filters[11] = this.ImageQuality;
            filters[12] = this.FrameRate;

            //loop through all the filters and add the relevante filters            
            foreach (string filter in filters)
            {
                if (filter != "0")
                {
                    query = query + filter + " AND ";
                    hasFilters = true;
                }
            }//foreach
            if (hasFilters)
            {
                query = " WHERE " + query.Substring(0, query.Length - 4); 
            }
            return query;
        }//String Builder

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private String BuildDurationQuery()
        {           
            String DurationQuery = "(Duration >"+ DurationUpDown1.Value+ " AND " + "Duration < " + (DurationUpDown2.Value)+")";
            return DurationQuery;
            //  Duration > 2 AND Duration< 100

        }//BuildDurationQuery

        private String BuildQualityQuery()
        {           
            return "(ImageQuality >" + "'"+ QualityUpDown1.Value + "'"+" AND " + "ImageQuality < " + "'"  + Decimal.Subtract(QualityUpDown2.Value, (decimal)0.0001) + "'"+ " OR(ImageQuality IS NULL) "+ ")"; 
        }//BuildDurationQuery

        private String BuildFrameRateQuery()
        {
            return "(FrameRate >" + FrameRatecomboBox1.SelectedItem + " AND " + "FrameRate < " + FrameRatecomboBox2.SelectedItem + ")";
        }//BuildDurationQuery

        //build the sub DB WHERE statement according to the user inputs
        private string BuildSubDBQuery()
        {
            string SubDBQuery = "";
            Boolean hasFilters = false;
            if (!this.SubDatabaseCheckBoxALL.Checked)
            {
                string tmp = "0";
                String[] SubDBFilters = new String[10];
                for (int i = 0; i <= SubDBFilters.Length - 1; i++)
                { SubDBFilters[i] = ""; }
                if (SubDatabaseCheckBox5.Checked)
                {
                    tmp = "'1'";
                    SubDBFilters[0] = tmp;
                }
                if (SubDatabaseCheckBox4.Checked)
                {
                    tmp = "'2'";
                    SubDBFilters[1] = tmp;
                }
                if (SubDatabaseCheckBox5.Checked)
                {
                    tmp = "'3'";
                    SubDBFilters[2] = tmp;
                }
                if (SubDatabaseCheckBoxCK.Checked)
                {
                    tmp = "'4'";
                    SubDBFilters[3] = tmp;
                }
                if (SubDatabaseCheckBoxDenver.Checked)
                {
                    tmp = "'5'";
                    SubDBFilters[4] = tmp;
                }
                if (SubDatabaseCheckBoxFEED.Checked)
                {
                    tmp = "'6'";
                    SubDBFilters[5] = tmp;
                }
                if (SubDatabaseCheckBoxMMI.Checked)
                {
                    tmp = "'7'";
                    SubDBFilters[6] = tmp;
                }
                if (SubDatabaseCheckBoxBimodal.Checked)
                {
                    tmp = "'8'";
                    SubDBFilters[7] = tmp;
                }
                if (SubDatabaseCheckBoxUNBC.Checked)
                {
                    tmp = "'9'";
                    SubDBFilters[8] = tmp;
                }
                if (SubDatabaseCheckBoxOther.Checked)
                {
                    tmp = "'10'";
                    SubDBFilters[9] = tmp;
                }


                for (int i = 0; i <= SubDBFilters.Length - 1; i++)
                {
                    //SubDBFilters[i].ToString()

                    // if (!SubDBFilters[i].Equals(null) )
                    if (SubDBFilters[i] != "")
                    {
                        SubDBQuery = SubDBQuery + SubDBFilters[i] + " , ";
                        hasFilters = true;
                    }
                }//foreach
                if (hasFilters)
                {
                    SubDBQuery = SubDBQuery.Substring(0, SubDBQuery.Length - 2);
                    SubDBQuery = " FromSubDB IN (" + SubDBQuery + ")";
                }
                return "(" + SubDBQuery + ")";
            }//if "All" checkbox is checked
            else
                return "0";
        }//BuildSubDBQuery

        //build the age WHERE statement according to the user inputs
        public String BuildAgeWhereQuery()
        {
            String output = "";
            if (AgeRangeCheckBox1.Checked) { return "0"; }
            Boolean[] ageChecboxesStatus =
            { AgeRangeCheckBox010.Checked,
              AgeRangeCheckBox1120.Checked,
              AgeRangeCheckBox2130.Checked,
              AgeRangeCheckBox3140.Checked,
              AgeRangeCheckBox4150.Checked,
              AgeRangeCheckBox5160.Checked,
              AgeRangeCheckBox6170.Checked,
              AgeRangeCheckBox7180.Checked,
              AgeRangeCheckBoxOVER80.Checked};
            String[] ageLow = { "0", "11", "21", "31", "41", "51", "61", "71", "81" };
            String[] ageHigh = { "10", "20", "30", "40", "50", "60", "70", "80","120" };
            for (int i = 0; i < ageChecboxesStatus.Length; i++)  
            {
                if (ageChecboxesStatus[i])
                    output += "((SUBSTRING(EstimatedAge, 0, CHARINDEX('-', EstimatedAge)) BETWEEN " + ageLow[i] + " AND " + ageHigh[i] + ")" +
                      "OR SUBSTRING(EstimatedAge,  CHARINDEX('-', EstimatedAge), LEN(EstimatedAge)) BETWEEN " + ageLow[i] + " AND " + ageHigh[i] + ") OR";
            }//for
            if (output != "")
                output = "(" + output.Substring(0, output.Length -2) + ")";
            return output;
        }//BuildAgeWhereQuery

        //build the Ethnicity WHERE statement according to the user inputs
        private string BuildEthnicityQuery()
        {
            string EthnicityQuery = "";
            Boolean hasFilters = false;
            if (!this.EthnicityCheckBoxALL.Checked)
            {
                string tmp = "0";
                String[] EthnicityFilters = new String[10];
                for (int i = 0; i <= EthnicityFilters.Length - 1; i++)
                { EthnicityFilters[i] = "";}
                if (EthnicityCheckBoxSouthAsia.Checked)
                { tmp = "'SouthAsian'";
                    EthnicityFilters[0] = tmp;}
                if (EthnicityCheckBoxCaucasian.Checked)
                {tmp = "'Caucasian'";
                    EthnicityFilters[1] = tmp;}
                if (EthnicityCheckBoxEasternAsia.Checked)
                {tmp = "'EasternAsian'";
                    EthnicityFilters[2] = tmp;}
                if (EthnicityCheckBoxAfrican.Checked)
                {tmp = "'African'";
                    EthnicityFilters[3] = tmp;}
                if (EthnicityCheckBoxOther.Checked)
                {tmp = "'Other'";
                    EthnicityFilters[4] = tmp;}

                for (int i = 0; i <= EthnicityFilters.Length - 1; i++)
                {
                    
                    if (EthnicityFilters[i] != "")
                    {
                        EthnicityQuery = EthnicityQuery + EthnicityFilters[i] + " , ";
                        hasFilters = true;
                    }
                }//foreach
                if (hasFilters)
                {
                    EthnicityQuery = EthnicityQuery.Substring(0, EthnicityQuery.Length - 2);
                    EthnicityQuery = " Ethnicity IN (" + EthnicityQuery + ")";
                }
                if (!hasFilters)
                { return "empty checkboxs in Ethnicity"; }
                return "(" + EthnicityQuery + ")";
            }//if "All" checkbox is checked
            else
                return "0";
        }//BuilEthnicityQuery

        //build the Format WHERE statement according to the user inputs
        private string BuildFormatQuery()
        {
            string FormatQuery = "";
            Boolean hasFilters = false;
            if (!this.FormatCheckBoxALL.Checked)
            {
                string tmp = "0";
                String[] formatFilters = new String[10];
                for (int i = 0; i <= formatFilters.Length - 1; i++)
                { formatFilters[i] = ""; }
                    if (FormatCheckBoxMPG.Checked)
                { tmp = "'MPG'"; 
                formatFilters[0] = tmp; }
                if (FormatCheckBoxMOB.Checked)
                { tmp = "'VOB'"; 
                formatFilters[1] = tmp; }
                if (FormatCheckBoxMP4.Checked)
                { tmp = "'MP4'"; 
                formatFilters[2] = tmp; }
                if (FormatCheckBoxMOV.Checked)
                { tmp = "'MOV'"; 
                formatFilters[3] = tmp; }
                if (FormatCheckBoxAVI.Checked)
                { tmp = "'AVI'"; 
                formatFilters[4] = tmp; }
                if (FormatCheckBoxASF.Checked)
                { tmp = "'ASF'";
                    formatFilters[5] = tmp; }
                if (FormatCheckBoxMP4.Checked)
                { tmp = "'BGP'";
                    formatFilters[6] = tmp; }
                if (FormatCheckBoxWMV.Checked)
                { tmp = "'WMV'";
                    formatFilters[7] = tmp; }
                if (FormatCheckBoxRM.Checked)
                { tmp = "'RM'";
                    formatFilters[8] = tmp; }
                if (FormatCheckBoxFLV.Checked)
                { tmp = "'FLV'"; 
                formatFilters[9] = tmp; }
                
                for (int i = 0; i <= formatFilters.Length-1; i++)
                    {
                    if (formatFilters[i]  != "")
                        {
                        FormatQuery = FormatQuery + formatFilters[i] + " , ";
                        hasFilters = true;
                        }
                    }//foreach
                if (hasFilters)
                {
                    FormatQuery = FormatQuery.Substring(0, FormatQuery.Length - 2);
                    FormatQuery = " Format IN (" + FormatQuery + ")";
                }
                return "(" + FormatQuery + ")";
            }//if "All" checkbox is checked
            else
            return  "0";
        }//BuildFormatQuery

        //build the Resolution WHERE statement according to the user inputs
        private string BuildResolutioQuery()
        {
            string ResolutionQuery = "0";
            Boolean hasFilters = false;
            if (!this.ResolutionCheckBoxALL.Checked)
            {
                string[] formatFilters = new string[10];
                if (ResolutionCheckBoxLOW.Checked)
                { ResolutionQuery = "ResWidth < 641"; hasFilters = true; }
                if (ResolutionCheckBoxMID.Checked)
                { ResolutionQuery = " ResWidth > 640 AND ResWidth < 1281"; hasFilters = true; }
                if (ResolutionCheckBoxHIGH.Checked)
                { ResolutionQuery = " ResWidth > 1280"; hasFilters = true; }             
                return "(" + ResolutionQuery + ")";
            }//if "All" checkbox is checked
            else
                return ResolutionQuery;
        }//BuildResolutioQuery      

        //enable the "ALL" checkbox to change all the sub checkboxes to "checked" status
        private void ResolutionCheckBoxALL_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ResolutionCheckBoxALL.Checked == true)
            {
                this.ResolutionCheckBoxLOW.Checked = true;
                this.ResolutionCheckBoxMID.Checked = true;
                this.ResolutionCheckBoxHIGH.Checked = true;
            }          
        }
       
        //enable the "ALL" checkbox to change all the sub checkboxes to "checked" status
        private void AgeRangeCheckBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (this.AgeRangeCheckBox1.Checked == true)
            {
                this.AgeRangeCheckBox010.Checked = true;
                this.AgeRangeCheckBox1120.Checked = true;
                this.AgeRangeCheckBox2130.Checked = true;
                this.AgeRangeCheckBox3140.Checked = true;
                this.AgeRangeCheckBox4150.Checked = true;
                this.AgeRangeCheckBox5160.Checked = true;
                this.AgeRangeCheckBox6170.Checked = true;
                this.AgeRangeCheckBox7180.Checked = true;
                this.AgeRangeCheckBoxOVER80.Checked = true;
            }           
        }

        //enable the "ALL" checkbox to change all the sub checkboxes to "checked" status
        private void SubDatabaseCheckBoxALL_CheckedChanged(object sender, EventArgs e)
        {
            if (this.SubDatabaseCheckBoxALL.Checked == true)
            {
                this.SubDatabaseCheckBox3.Checked = true;
                this.SubDatabaseCheckBox4.Checked = true;
                this.SubDatabaseCheckBox5.Checked = true;
                this.SubDatabaseCheckBoxBimodal.Checked = true;
                this.SubDatabaseCheckBoxDenver.Checked = true;
                this.SubDatabaseCheckBoxFEED.Checked = true;
                this.SubDatabaseCheckBoxUNBC.Checked = true;
                this.SubDatabaseCheckBoxMMI.Checked = true;
                this.SubDatabaseCheckBoxOther.Checked = true;
                this.SubDatabaseCheckBoxCK.Checked = true;
            }           
        }

        //enable the "ALL" checkbox to change all the sub checkboxes to "checked" status
        private void FormatCheckBoxALL_CheckedChanged_1(object sender, EventArgs e)
        {
            if (this.FormatCheckBoxALL.Checked)
            {
                
                    this.FormatCheckBoxASF.Checked = true;
                    this.FormatCheckBoxAVI.Checked = true;
                    this.FormatCheckBoxBGP.Checked = true;
                    this.FormatCheckBoxFLV.Checked = true;
                    this.FormatCheckBoxMOB.Checked = true;
                    this.FormatCheckBoxMOV.Checked = true;
                    this.FormatCheckBoxMP4.Checked = true;
                    this.FormatCheckBoxMPG.Checked = true;
                    this.FormatCheckBoxRM.Checked = true;
                    this.FormatCheckBoxWMV.Checked = true;
            }
        }

        //enable the "ALL" checkbox to change all the sub checkboxes to "checked" status
        private void EthnicityCheckBoxALL_CheckedChanged_1(object sender, EventArgs e)
        {
            if (this.EthnicityCheckBoxALL.Checked == true)
            {
                this.EthnicityCheckBoxAfrican.Checked = true;
                this.EthnicityCheckBoxCaucasian.Checked = true;
                this.EthnicityCheckBoxEasternAsia.Checked = true;
                this.EthnicityCheckBoxSouthAsia.Checked = true;
                this.EthnicityCheckBoxOther.Checked = true;
            }
        }
       
        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            mainForm mainForm = new mainForm();
            this.Hide();
            mainForm.Show();
        }

        private void butPrev_Click(object sender, EventArgs e)
        {
            mainForm mainForm1 = new mainForm();
            mainForm1.Show();
            this.Close();
        }

        private void FormatCheckBoxMPG_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxMPG.Checked)
            {
                this.FormatCheckBoxALL.Checked = false;
            }
        }
        private void FormatCheckBoxMOB_CheckedChanged(object sender, EventArgs e)
        {
            if(!this.FormatCheckBoxMOB.Checked)
            this.FormatCheckBoxALL.Checked = false;
        }
        private void FormatCheckBoxMP4_CheckedChanged_1(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxMP4.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }
        private void FormatCheckBoxAVI_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxAVI.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }
        private void FormatCheckBoxASF_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxASF.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }

        private void FormatCheckBoxBGP_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxBGP.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }

        private void FormatCheckBoxWMV_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxWMV.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }
        private void FormatCheckBoxRM_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxRM.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }
        private void FormatCheckBoxFLV_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.FormatCheckBoxFLV.Checked)
                this.FormatCheckBoxALL.Checked = false;
        }

        private void ResolutionCheckBoxLOW_CheckedChanged_1(object sender, EventArgs e)
        {
            if (!this.ResolutionCheckBoxLOW.Checked)
                this.ResolutionCheckBoxALL.Checked = false;
        }

        private void ResolutionCheckBoxMID_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ResolutionCheckBoxMID.Checked)
                this.ResolutionCheckBoxALL.Checked = false;
        }

        private void ResolutionCheckBoxHIGH_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ResolutionCheckBoxHIGH.Checked)
                this.ResolutionCheckBoxALL.Checked = false;
        }

        private void AgeRangeCheckBox010_CheckedChanged_1(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox010.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox1120_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox1120.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox2130_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox2130.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox3140_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox3140.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox4150_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox4150.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox5160_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox5160.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox6170_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox6170.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBox7180_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBox7180.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void AgeRangeCheckBoxOVER80_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.AgeRangeCheckBoxOVER80.Checked)
                this.AgeRangeCheckBox1.Checked = false;
        }

        private void EthnicityCheckBoxCaucasian_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.EthnicityCheckBoxCaucasian.Checked)
                this.EthnicityCheckBoxALL.Checked = false;
        }

        private void EthnicityCheckBoxEasternAsia_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.EthnicityCheckBoxEasternAsia.Checked)
                this.EthnicityCheckBoxALL.Checked = false;
        }

        private void EthnicityCheckBoxAfrican_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.EthnicityCheckBoxAfrican.Checked)
                this.EthnicityCheckBoxALL.Checked = false;
        }

        private void EthnicityCheckBoxSouthAsia_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.EthnicityCheckBoxSouthAsia.Checked)
                this.EthnicityCheckBoxALL.Checked = false;
        }

        private void EthnicityCheckBoxOther_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.EthnicityCheckBoxOther.Checked)
                this.EthnicityCheckBoxALL.Checked = false;
        }

        private void SubDatabaseCheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBox5.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }

        private void SubDatabaseCheckBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBox4.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }
           
        private void SubDatabaseCheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBox3.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }
        
        private void SubDatabaseCheckBoxDenver_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxDenver.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }

        private void SubDatabaseCheckBoxFEED_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxFEED.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }
     
        private void SubDatabaseCheckBoxMMI_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxMMI.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }

        private void SubDatabaseCheckBoxBimodal_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxBimodal.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }

        private void SubDatabaseCheckBoxUNBC_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxUNBC.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }

        private void SubDatabaseCheckBoxOther_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxOther.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }
    
        private void WithMoustacheRadioButtonY_CheckedChanged(object sender, EventArgs e)
        {
              this.hasMoustache = "(hasMoustache='Some' OR hasMoustache='Heavy')";
        }

        private void WithMoustacheRadioButtonN_CheckedChanged(object sender, EventArgs e)
        {
            this.hasMoustache = "hasMoustache='None'";

        }

        private void WithBearedRadioButtonY_CheckedChanged(object sender, EventArgs e)
        {
            this.hasBeard = "(hasBeard='Some' OR hasBeard='Heavy')";
        }

        private void WithBearedRadioButtonN_CheckedChanged(object sender, EventArgs e)
        {
            this.hasBeard = "hasBeard='None'";
        }

        private void IncludeVoiceRadioButtonY_CheckedChanged(object sender, EventArgs e)
        {
            this.hasVoice = "hasVoice= '1'";
        }

        private void IncludeVoiceRadioButtonN_CheckedChanged(object sender, EventArgs e)
        {
            this.hasVoice = "hasVoice= '0'";
        }

        private void GenderRadioButtonA_CheckedChanged(object sender, EventArgs e)
        {
            this.Gender = "0";
        }

        private void GenderRadioButtonM_CheckedChanged(object sender, EventArgs e)
        {
            this.Gender = "Gender='Male'";
        }

        private void GenderRadioButtonF_CheckedChanged(object sender, EventArgs e)
        {
            this.Gender = "Gender='Female'";
        }

        private void WearingGlassesRadioButtonA_CheckedChanged(object sender, EventArgs e)
        {
            this.hasGlasses = "hasGlasses='all'";
        }

        private void WearingGlassesRadioButtonY_CheckedChanged(object sender, EventArgs e)
        {
            this.hasGlasses = "hasGlasses='yes'";
        }

        private void WearingGlassesRadioButtonN_CheckedChanged(object sender, EventArgs e)
        {
            this.hasGlasses = "hasGlasses='no'";
        }

        private void SubDatabaseCheckBoxCK_CheckedChanged_1(object sender, EventArgs e)
        {
            if (!this.SubDatabaseCheckBoxCK.Checked)
                this.SubDatabaseCheckBoxALL.Checked = false;
        }
    }
}
