﻿namespace MetaFace
{
    partial class retriveExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ResultsDataGrid = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GENDER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstimatedAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ETHNICITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HASGLASSES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HASMOUSTACHE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HasBeard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hasVoice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImageQuality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VideoPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateAnalysis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailedAnalysis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FromSubDB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new MetaFace.DataSet1();
            this.gBDistFolder = new System.Windows.Forms.GroupBox();
            this.materialRaisedButton3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.destinationFolderTextbox = new System.Windows.Forms.TextBox();
            this.gBExportOptions = new System.Windows.Forms.GroupBox();
            this.ExportOptionsRadioButtonAll = new MaterialSkin.Controls.MaterialRadioButton();
            this.ExportOptionsRadioButtonAnalysis = new MaterialSkin.Controls.MaterialRadioButton();
            this.ExportOptionsRadioButtonVideo = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButtonExport = new MaterialSkin.Controls.MaterialRaisedButton();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.ResultsDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.gBDistFolder.SuspendLayout();
            this.gBExportOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // ResultsDataGrid
            // 
            this.ResultsDataGrid.AllowUserToAddRows = false;
            this.ResultsDataGrid.AllowUserToDeleteRows = false;
            this.ResultsDataGrid.AllowUserToOrderColumns = true;
            this.ResultsDataGrid.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ResultsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ResultsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ResultsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.NAME,
            this.GENDER,
            this.EstimatedAge,
            this.ETHNICITY,
            this.HASGLASSES,
            this.HASMOUSTACHE,
            this.HasBeard,
            this.hasVoice,
            this.ImageQuality,
            this.Duration,
            this.FORMAT,
            this.VideoPath,
            this.StateAnalysis,
            this.DetailedAnalysis,
            this.FromSubDB});
            this.ResultsDataGrid.DataSource = this.dataSet1BindingSource;
            this.ResultsDataGrid.Location = new System.Drawing.Point(39, 87);
            this.ResultsDataGrid.Name = "ResultsDataGrid";
            this.ResultsDataGrid.ReadOnly = true;
            this.ResultsDataGrid.Size = new System.Drawing.Size(924, 398);
            this.ResultsDataGrid.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "VideoID";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ID.DefaultCellStyle = dataGridViewCellStyle2;
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 50;
            // 
            // NAME
            // 
            this.NAME.DataPropertyName = "Name";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.NAME.DefaultCellStyle = dataGridViewCellStyle3;
            this.NAME.HeaderText = "Name";
            this.NAME.Name = "NAME";
            this.NAME.ReadOnly = true;
            this.NAME.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NAME.Width = 80;
            // 
            // GENDER
            // 
            this.GENDER.DataPropertyName = "Gender";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GENDER.DefaultCellStyle = dataGridViewCellStyle4;
            this.GENDER.HeaderText = "Gender";
            this.GENDER.Name = "GENDER";
            this.GENDER.ReadOnly = true;
            this.GENDER.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GENDER.Width = 60;
            // 
            // EstimatedAge
            // 
            this.EstimatedAge.DataPropertyName = "EstimatedAge";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.NullValue = null;
            this.EstimatedAge.DefaultCellStyle = dataGridViewCellStyle5;
            this.EstimatedAge.HeaderText = "Estimated Age";
            this.EstimatedAge.Name = "EstimatedAge";
            this.EstimatedAge.ReadOnly = true;
            this.EstimatedAge.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EstimatedAge.Width = 60;
            // 
            // ETHNICITY
            // 
            this.ETHNICITY.DataPropertyName = "Ethnicity";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ETHNICITY.DefaultCellStyle = dataGridViewCellStyle6;
            this.ETHNICITY.HeaderText = "Ethnicity";
            this.ETHNICITY.Name = "ETHNICITY";
            this.ETHNICITY.ReadOnly = true;
            this.ETHNICITY.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ETHNICITY.Width = 70;
            // 
            // HASGLASSES
            // 
            this.HASGLASSES.DataPropertyName = "HasGlasses";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HASGLASSES.DefaultCellStyle = dataGridViewCellStyle7;
            this.HASGLASSES.HeaderText = "Has Glasses?";
            this.HASGLASSES.Name = "HASGLASSES";
            this.HASGLASSES.ReadOnly = true;
            this.HASGLASSES.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HASGLASSES.Width = 80;
            // 
            // HASMOUSTACHE
            // 
            this.HASMOUSTACHE.DataPropertyName = "HasMoustache";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HASMOUSTACHE.DefaultCellStyle = dataGridViewCellStyle8;
            this.HASMOUSTACHE.HeaderText = "Has Moustache?";
            this.HASMOUSTACHE.Name = "HASMOUSTACHE";
            this.HASMOUSTACHE.ReadOnly = true;
            this.HASMOUSTACHE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HASMOUSTACHE.Width = 80;
            // 
            // HasBeard
            // 
            this.HasBeard.DataPropertyName = "HasBeard";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HasBeard.DefaultCellStyle = dataGridViewCellStyle9;
            this.HasBeard.HeaderText = "Has Beard?";
            this.HasBeard.Name = "HasBeard";
            this.HasBeard.ReadOnly = true;
            this.HasBeard.Width = 80;
            // 
            // hasVoice
            // 
            this.hasVoice.DataPropertyName = "HasVoice";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.hasVoice.DefaultCellStyle = dataGridViewCellStyle10;
            this.hasVoice.HeaderText = "Has Voice?";
            this.hasVoice.Name = "hasVoice";
            this.hasVoice.ReadOnly = true;
            this.hasVoice.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.hasVoice.Width = 60;
            // 
            // ImageQuality
            // 
            this.ImageQuality.DataPropertyName = "ImageQuality";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = null;
            this.ImageQuality.DefaultCellStyle = dataGridViewCellStyle11;
            this.ImageQuality.HeaderText = "Image Quality";
            this.ImageQuality.Name = "ImageQuality";
            this.ImageQuality.ReadOnly = true;
            this.ImageQuality.Width = 60;
            // 
            // Duration
            // 
            this.Duration.DataPropertyName = "Duration";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.Duration.DefaultCellStyle = dataGridViewCellStyle12;
            this.Duration.HeaderText = "Duration [Minutes]";
            this.Duration.Name = "Duration";
            this.Duration.ReadOnly = true;
            this.Duration.Width = 70;
            // 
            // FORMAT
            // 
            this.FORMAT.DataPropertyName = "Format";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.FORMAT.DefaultCellStyle = dataGridViewCellStyle13;
            this.FORMAT.HeaderText = "Format";
            this.FORMAT.Name = "FORMAT";
            this.FORMAT.ReadOnly = true;
            this.FORMAT.Width = 60;
            // 
            // VideoPath
            // 
            this.VideoPath.DataPropertyName = "path";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.VideoPath.DefaultCellStyle = dataGridViewCellStyle14;
            this.VideoPath.HeaderText = "Video Path";
            this.VideoPath.Name = "VideoPath";
            this.VideoPath.ReadOnly = true;
            // 
            // StateAnalysis
            // 
            this.StateAnalysis.DataPropertyName = "StateAnalysis";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.StateAnalysis.DefaultCellStyle = dataGridViewCellStyle15;
            this.StateAnalysis.HeaderText = "State Analysis";
            this.StateAnalysis.Name = "StateAnalysis";
            this.StateAnalysis.ReadOnly = true;
            // 
            // DetailedAnalysis
            // 
            this.DetailedAnalysis.DataPropertyName = "DetailedAnalysis";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DetailedAnalysis.DefaultCellStyle = dataGridViewCellStyle16;
            this.DetailedAnalysis.HeaderText = "Detailed Analysis";
            this.DetailedAnalysis.Name = "DetailedAnalysis";
            this.DetailedAnalysis.ReadOnly = true;
            // 
            // FromSubDB
            // 
            this.FromSubDB.DataPropertyName = "subDBName";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.FromSubDB.DefaultCellStyle = dataGridViewCellStyle17;
            this.FromSubDB.HeaderText = "From Sub DB";
            this.FromSubDB.Name = "FromSubDB";
            this.FromSubDB.ReadOnly = true;
            // 
            // dataSet1BindingSource
            // 
            this.dataSet1BindingSource.DataSource = this.dataSet1;
            this.dataSet1BindingSource.Position = 0;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.ExcludeSchema;
            // 
            // gBDistFolder
            // 
            this.gBDistFolder.BackColor = System.Drawing.Color.Transparent;
            this.gBDistFolder.Controls.Add(this.materialRaisedButton3);
            this.gBDistFolder.Controls.Add(this.destinationFolderTextbox);
            this.gBDistFolder.Location = new System.Drawing.Point(39, 527);
            this.gBDistFolder.Name = "gBDistFolder";
            this.gBDistFolder.Size = new System.Drawing.Size(518, 82);
            this.gBDistFolder.TabIndex = 4;
            this.gBDistFolder.TabStop = false;
            this.gBDistFolder.Text = "Select Destination Folder";
            // 
            // materialRaisedButton3
            // 
            this.materialRaisedButton3.AutoSize = true;
            this.materialRaisedButton3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton3.Depth = 0;
            this.materialRaisedButton3.Icon = null;
            this.materialRaisedButton3.Location = new System.Drawing.Point(455, 35);
            this.materialRaisedButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton3.Name = "materialRaisedButton3";
            this.materialRaisedButton3.Primary = true;
            this.materialRaisedButton3.Size = new System.Drawing.Size(57, 36);
            this.materialRaisedButton3.TabIndex = 9;
            this.materialRaisedButton3.Text = "Open";
            this.materialRaisedButton3.UseVisualStyleBackColor = true;
            this.materialRaisedButton3.Click += new System.EventHandler(this.materialRaisedButton3_Click);
            // 
            // destinationFolderTextbox
            // 
            this.destinationFolderTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destinationFolderTextbox.Location = new System.Drawing.Point(10, 40);
            this.destinationFolderTextbox.Name = "destinationFolderTextbox";
            this.destinationFolderTextbox.Size = new System.Drawing.Size(439, 24);
            this.destinationFolderTextbox.TabIndex = 1;
            // 
            // gBExportOptions
            // 
            this.gBExportOptions.BackColor = System.Drawing.Color.White;
            this.gBExportOptions.Controls.Add(this.ExportOptionsRadioButtonAll);
            this.gBExportOptions.Controls.Add(this.ExportOptionsRadioButtonAnalysis);
            this.gBExportOptions.Controls.Add(this.ExportOptionsRadioButtonVideo);
            this.gBExportOptions.Location = new System.Drawing.Point(568, 527);
            this.gBExportOptions.Name = "gBExportOptions";
            this.gBExportOptions.Size = new System.Drawing.Size(395, 82);
            this.gBExportOptions.TabIndex = 5;
            this.gBExportOptions.TabStop = false;
            this.gBExportOptions.Text = "Export Options";
            // 
            // ExportOptionsRadioButtonAll
            // 
            this.ExportOptionsRadioButtonAll.AutoSize = true;
            this.ExportOptionsRadioButtonAll.Depth = 0;
            this.ExportOptionsRadioButtonAll.Font = new System.Drawing.Font("Roboto", 10F);
            this.ExportOptionsRadioButtonAll.Location = new System.Drawing.Point(15, 34);
            this.ExportOptionsRadioButtonAll.Margin = new System.Windows.Forms.Padding(0);
            this.ExportOptionsRadioButtonAll.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ExportOptionsRadioButtonAll.MouseState = MaterialSkin.MouseState.HOVER;
            this.ExportOptionsRadioButtonAll.Name = "ExportOptionsRadioButtonAll";
            this.ExportOptionsRadioButtonAll.Ripple = true;
            this.ExportOptionsRadioButtonAll.Size = new System.Drawing.Size(45, 30);
            this.ExportOptionsRadioButtonAll.TabIndex = 5;
            this.ExportOptionsRadioButtonAll.TabStop = true;
            this.ExportOptionsRadioButtonAll.Text = "All";
            this.ExportOptionsRadioButtonAll.UseVisualStyleBackColor = true;
            // 
            // ExportOptionsRadioButtonAnalysis
            // 
            this.ExportOptionsRadioButtonAnalysis.AutoSize = true;
            this.ExportOptionsRadioButtonAnalysis.Depth = 0;
            this.ExportOptionsRadioButtonAnalysis.Font = new System.Drawing.Font("Roboto", 10F);
            this.ExportOptionsRadioButtonAnalysis.Location = new System.Drawing.Point(258, 34);
            this.ExportOptionsRadioButtonAnalysis.Margin = new System.Windows.Forms.Padding(0);
            this.ExportOptionsRadioButtonAnalysis.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ExportOptionsRadioButtonAnalysis.MouseState = MaterialSkin.MouseState.HOVER;
            this.ExportOptionsRadioButtonAnalysis.Name = "ExportOptionsRadioButtonAnalysis";
            this.ExportOptionsRadioButtonAnalysis.Ripple = true;
            this.ExportOptionsRadioButtonAnalysis.Size = new System.Drawing.Size(112, 30);
            this.ExportOptionsRadioButtonAnalysis.TabIndex = 4;
            this.ExportOptionsRadioButtonAnalysis.TabStop = true;
            this.ExportOptionsRadioButtonAnalysis.Text = "Only Analysis";
            this.ExportOptionsRadioButtonAnalysis.UseVisualStyleBackColor = true;
            // 
            // ExportOptionsRadioButtonVideo
            // 
            this.ExportOptionsRadioButtonVideo.AutoSize = true;
            this.ExportOptionsRadioButtonVideo.Depth = 0;
            this.ExportOptionsRadioButtonVideo.Font = new System.Drawing.Font("Roboto", 10F);
            this.ExportOptionsRadioButtonVideo.Location = new System.Drawing.Point(115, 34);
            this.ExportOptionsRadioButtonVideo.Margin = new System.Windows.Forms.Padding(0);
            this.ExportOptionsRadioButtonVideo.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ExportOptionsRadioButtonVideo.MouseState = MaterialSkin.MouseState.HOVER;
            this.ExportOptionsRadioButtonVideo.Name = "ExportOptionsRadioButtonVideo";
            this.ExportOptionsRadioButtonVideo.Ripple = true;
            this.ExportOptionsRadioButtonVideo.Size = new System.Drawing.Size(102, 30);
            this.ExportOptionsRadioButtonVideo.TabIndex = 3;
            this.ExportOptionsRadioButtonVideo.TabStop = true;
            this.ExportOptionsRadioButtonVideo.Text = "Only Videos";
            this.ExportOptionsRadioButtonVideo.UseVisualStyleBackColor = true;
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(39, 732);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(86, 36);
            this.materialRaisedButton1.TabIndex = 6;
            this.materialRaisedButton1.Text = "Previous";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // materialRaisedButtonExport
            // 
            this.materialRaisedButtonExport.AutoSize = true;
            this.materialRaisedButtonExport.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButtonExport.Depth = 0;
            this.materialRaisedButtonExport.Icon = null;
            this.materialRaisedButtonExport.Location = new System.Drawing.Point(890, 732);
            this.materialRaisedButtonExport.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButtonExport.Name = "materialRaisedButtonExport";
            this.materialRaisedButtonExport.Primary = true;
            this.materialRaisedButtonExport.Size = new System.Drawing.Size(73, 36);
            this.materialRaisedButtonExport.TabIndex = 7;
            this.materialRaisedButtonExport.Text = "Export";
            this.materialRaisedButtonExport.UseVisualStyleBackColor = true;
            this.materialRaisedButtonExport.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // retriveExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1000, 800);
            this.Controls.Add(this.materialRaisedButtonExport);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.gBExportOptions);
            this.Controls.Add(this.gBDistFolder);
            this.Controls.Add(this.ResultsDataGrid);
            this.MaximizeBox = false;
            this.Name = "retriveExportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Video export and analytics";
            ((System.ComponentModel.ISupportInitialize)(this.ResultsDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.gBDistFolder.ResumeLayout(false);
            this.gBDistFolder.PerformLayout();
            this.gBExportOptions.ResumeLayout(false);
            this.gBExportOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ResultsDataGrid;
        private System.Windows.Forms.GroupBox gBDistFolder;
        private System.Windows.Forms.GroupBox gBExportOptions;
        private System.Windows.Forms.TextBox destinationFolderTextbox;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButtonExport;
        private MaterialSkin.Controls.MaterialRadioButton ExportOptionsRadioButtonAll;
        private MaterialSkin.Controls.MaterialRadioButton ExportOptionsRadioButtonAnalysis;
        private MaterialSkin.Controls.MaterialRadioButton ExportOptionsRadioButtonVideo;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton3;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource dataSet1BindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn GENDER;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstimatedAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn ETHNICITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn HASGLASSES;
        private System.Windows.Forms.DataGridViewTextBoxColumn HASMOUSTACHE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HasBeard;
        private System.Windows.Forms.DataGridViewTextBoxColumn hasVoice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImageQuality;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
        private System.Windows.Forms.DataGridViewTextBoxColumn FORMAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn VideoPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateAnalysis;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetailedAnalysis;
        private System.Windows.Forms.DataGridViewTextBoxColumn FromSubDB;
    }
}