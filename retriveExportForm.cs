﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using System.IO;
using Shell32;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Collections;
namespace MetaFace
{
    
    public partial class retriveExportForm : MaterialForm
    {
        public DataSet data;
        public ProgressBar exportProgressBar;

        public retriveExportForm()
        {
            //Progress Bar Init
            this.exportProgressBar = new ProgressBar();
            this.exportProgressBar.Hide();
            this.exportProgressBar.Location = new System.Drawing.Point(39, 648);
            this.exportProgressBar.Name = "exportProgressBar";
            this.exportProgressBar.Size = new System.Drawing.Size(924, 32);
            this.exportProgressBar.TabIndex = 26;  
            this.Controls.Add(exportProgressBar);
            this.Sizable = false;
            InitializeComponent();
        }

        //retriveExportForm Constructor
        public retriveExportForm( DataSet ds)
        {
            //Progress Bar Init
            this.exportProgressBar = new ProgressBar();
            this.exportProgressBar.Hide();

            this.exportProgressBar.MarqueeAnimationSpeed = 12;
            this.exportProgressBar.Location = new System.Drawing.Point(39, 648);
            this.exportProgressBar.Name = "exportProgressBar";
            //this.exportProgressBar.Style= ProgressBarStyle.Marquee;

            this.exportProgressBar.Size = new System.Drawing.Size(924, 32);
            this.exportProgressBar.TabIndex = 26;
            this.Controls.Add(exportProgressBar);
            InitializeComponent();
            this.data = ds;
            this.Sizable = false;


            ResultsDataGrid.DataSource = ds.Tables[0];
        }//retriveExportForm


        //Clicking on Previous Button
        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            retriveDataFilterForm retriveDataFilterForm  = new retriveDataFilterForm(true);
            this.Hide();
            retriveDataFilterForm.Show();
        }//materialRaisedButton1_Click


        //Clicking on Directory Path Button
        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            destinationFolderTextbox.Clear();
            folderBrowserDialog1.ShowDialog();
            string dirPath = folderBrowserDialog1.SelectedPath;
            destinationFolderTextbox.AppendText(dirPath);
            destinationFolderTextbox.Show();
        }//materialRaisedButton3_Click


        //Clicking on Export Button
        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            //If Destination Folder path isn't valid!
            if (!Directory.Exists(destinationFolderTextbox.Text))
                MessageBox.Show("Destination Folder path isn't valid!", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //Destination Folder path is valid
            else
            {
                String outputFolder = destinationFolderTextbox.Text + @"\Outputs #" + DateTime.Now.ToShortDateString().Replace('/', '.') + " " + DateTime.Now.ToShortTimeString().Replace(':','-');
                
                //List for Referenced Sub DB's
                List<String> referenceList = new List<String>();


                //Output Folder & Time stamp Creation 
                Directory.CreateDirectory(outputFolder);

                //Exporting Videos Only
                if (ExportOptionsRadioButtonVideo.Checked)
                {

                    //Create Videos Directory in Output-TimeStamped Folder
                    String OutputVideosFolder = outputFolder + @"\Videos";
                    Directory.CreateDirectory(OutputVideosFolder);

                    //Set Progress bar Max Size
                    this.exportProgressBar.Maximum = ResultsDataGrid.RowCount-1;
                    this.exportProgressBar.MarqueeAnimationSpeed = 100;
                    this.exportProgressBar.Show();

                    //Copy filtered videos to new Video Folder
                    for (int i=0;i < ResultsDataGrid.RowCount; i++)
                    {
                        //Increase Progressbar
                        this.exportProgressBar.Increment(1);

                        //Add sub DB's to Reference List
                        referenceList.Add(ResultsDataGrid[15, i].Value.ToString());

                        //Video Source path
                        String videoSourcePath = ResultsDataGrid[12, i].Value.ToString();
                        
                        //Checks If File & Folder Exist
                        if (File.Exists(videoSourcePath) && Directory.Exists(OutputVideosFolder))
                        {
                            //Build Output Target String
                            String OutputVideosFolderFile = OutputVideosFolder + @"\" + ResultsDataGrid[0, i].Value.ToString() + "-" + ResultsDataGrid[1,i].Value.ToString() + "." + ResultsDataGrid[11,i].Value.ToString();
                            
                            //Copy videoPath to Videos            
                            File.Copy(videoSourcePath, OutputVideosFolderFile,true);
                        }//If
                    }//for

                    
                }//If Videos Only

                //Exporting Analysis Only
                else if (ExportOptionsRadioButtonAnalysis.Checked)
                {
                    String stateSourcePath = "", detailedSourcePath = "";
                    //Create State & Detailed Analysis Directory in Output-TimeStamped Folder
                    String OutputStateFolder = outputFolder + @"\Analysis\State Analysis";
                    String OutputDetailedFolder = outputFolder + @"\Analysis\Detailed Analysis";
                    Directory.CreateDirectory(OutputStateFolder);
                    Directory.CreateDirectory(OutputDetailedFolder);

                    //Set Progress bar Max Size
                    this.exportProgressBar.Maximum = ResultsDataGrid.RowCount-1;
                    this.exportProgressBar.MarqueeAnimationSpeed = 100;
                    this.exportProgressBar.Show();

                    //Copy filtered Analysis to relevant analysis Folder
                    for (int i = 0; i < ResultsDataGrid.RowCount; i++)
                    {
                        //Increase Progressbar
                        this.exportProgressBar.Increment(1);

                        //State & Detailed Analysis paths
                        stateSourcePath = ResultsDataGrid[13, i].Value.ToString();
                        detailedSourcePath = ResultsDataGrid[14, i].Value.ToString();

                        //Checks If File & Folder Exist
                        if (File.Exists(stateSourcePath) && File.Exists(detailedSourcePath) && Directory.Exists(OutputStateFolder) && Directory.Exists(OutputDetailedFolder))
                        {
                            //Build Output Target String - State Analysis
                            String OutputStateFolderFile = OutputStateFolder + @"\" + ResultsDataGrid[0, i].Value.ToString() +"-"+ ResultsDataGrid[1, i].Value.ToString() + "_State.txt";

                            //Build Output Target String - Detailed Analysis
                            String OutputDetailedFolderFile = OutputDetailedFolder + @"\" + ResultsDataGrid[0, i].Value.ToString() +"-"+ ResultsDataGrid[1, i].Value.ToString() + "_Detailed.txt";

                            //Copy State Analysis to State Folder            
                            File.Copy(stateSourcePath, OutputStateFolderFile, true);

                            //Copy Detailed Analysis to Detailed Folder             
                            File.Copy(detailedSourcePath, OutputDetailedFolderFile, true);
                        }//If
                    }//for
                }//If Analysis Only

                //Exporting Both Videos & Analysis
                else
                {
                    //Create Videos Directory in Output-TimeStamped Folder
                    String OutputVideosFolder = outputFolder + @"\Videos";
                    Directory.CreateDirectory(OutputVideosFolder);

                    String stateSourcePath = "", detailedSourcePath = "";
                    //Create State & Detailed Analysis Directory in Output-TimeStamped Folder
                    String OutputStateFolder = outputFolder + @"\Analysis\State Analysis";
                    String OutputDetailedFolder = outputFolder + @"\Analysis\Detailed Analysis";
                    Directory.CreateDirectory(OutputStateFolder);
                    Directory.CreateDirectory(OutputDetailedFolder);

                    //Set Progress bar Max Size
                    this.exportProgressBar.Maximum = ResultsDataGrid.RowCount-1;
                    this.exportProgressBar.Show();
                    this.exportProgressBar.MarqueeAnimationSpeed = 100;

                    //Copy filtered videos & Analysis to new Video Folder
                    for (int i = 0; i < ResultsDataGrid.RowCount; i++)
                    {
                        //Increase Progressbar
                        this.exportProgressBar.Increment(1);

                        //Add sub DB's to Reference List
                        referenceList.Add(ResultsDataGrid[15, i].Value.ToString());

                        //Video Source path
                        String videoSourcePath = ResultsDataGrid[12, i].Value.ToString();

                        //Checks If File & Folder Exist
                        if (File.Exists(videoSourcePath) && Directory.Exists(OutputVideosFolder))
                        {
                            //Build Output Target String
                            String OutputVideosFolderFile = OutputVideosFolder + @"\" + ResultsDataGrid[0, i].Value.ToString() + "-" + ResultsDataGrid[1, i].Value.ToString() + "." + ResultsDataGrid[11, i].Value.ToString();

                            //Copy videoPath to Videos            
                            File.Copy(videoSourcePath, OutputVideosFolderFile,true);
                        }//If

                        //State & Detailed Analysis paths
                        stateSourcePath = ResultsDataGrid[13, i].Value.ToString();
                        detailedSourcePath = ResultsDataGrid[14, i].Value.ToString();

                        //Checks If File & Folder Exist
                        if (File.Exists(stateSourcePath) && File.Exists(detailedSourcePath) && Directory.Exists(OutputStateFolder) && Directory.Exists(OutputDetailedFolder))
                        {
                            //Build Output Target String - State Analysis
                            String OutputStateFolderFile = OutputStateFolder + @"\" + ResultsDataGrid[0, i].Value.ToString() + "-" + ResultsDataGrid[1, i].Value.ToString() + "_State.txt";

                            //Build Output Target String - Detailed Analysis
                            String OutputDetailedFolderFile = OutputDetailedFolder + @"\"+ ResultsDataGrid[0, i].Value.ToString() + "-" + ResultsDataGrid[1, i].Value.ToString() + "_Detailed.txt";

                            //Copy State Analysis to State Folder            
                            File.Copy(stateSourcePath, OutputStateFolderFile,true);

                            //Copy Detailed Analysis to Detailed Folder             
                            File.Copy(detailedSourcePath, OutputDetailedFolderFile, true);
                        }//If
                    }//for each row in Returieved Dataset
                }//Else - Both Videos & Analysis

                //Copying Refernce Sub DB Information to Output Folder
                if (ExportOptionsRadioButtonAll.Checked||ExportOptionsRadioButtonVideo.Checked)
                {
                    List<String> uniqueSDBList = referenceList.Distinct().ToList();
                    String[] referenceArray = uniqueSDBList.ToArray();

                    //Build Reference Sub DB SQL QUERY
                    var subDBQuery = "SELECT ID,NAME,TermOfUse FROM [CORPUS].[dbo].[SubDB] Where Name IN(";
                    for (int i = 0; i < referenceArray.Length; i++)
                    {
                        if (i < (referenceArray.Length - 1))
                            subDBQuery += "'"+referenceArray[i] + "',";
                        else subDBQuery += "'"+referenceArray[i] + "')";
                    }//for

                    //Connect to Server to get Sub DB's Info
                    try
                    {
                        //Retrieving Data From SQL Server
                        var dataAdapter = new SqlDataAdapter(subDBQuery, Program.getgCnn());
                        var commandBuilder = new SqlCommandBuilder(dataAdapter);
                        DataSet ds = new DataSet();
                        dataAdapter.FillSchema(ds, SchemaType.Mapped);
                        dataAdapter.Fill(ds);

                        //Reference Folder in Output Folder
                        String referenceFolder = outputFolder + @"\Reference";
                        Directory.CreateDirectory(referenceFolder);

                        //For each unique Sub DB
                        for (int i = 0; (i < referenceArray.Length) && (ds.Tables[0].Rows[i]["ID"].ToString() != "10"); i++)
                        {

                            String subDBPath = referenceFolder + @"\" + ds.Tables[0].Rows[i]["NAME"].ToString();
                            Directory.CreateDirectory(subDBPath);

                            //Checks If File & Folder Exist
                            if (Directory.Exists(subDBPath))
                            {
                                //Build Output Target String
                                String OutputVideosFolderFile = subDBPath + @"\Info & Terms";
                                Directory.CreateDirectory(OutputVideosFolderFile);

                                //Copy videoPath to Videos            

                                String sourceFolder = Program.getDefaultDatabeseFolderPath() + @"\" + ds.Tables[0].Rows[i]["NAME"].ToString() + @"\Info & Terms";
                                MessageBox.Show(sourceFolder);

                                //If I/O Folders Exist - Copy Term Of Use Files
                                if (Directory.Exists(sourceFolder) && Directory.Exists(OutputVideosFolderFile))
                                {
                                    String[] files = System.IO.Directory.GetFiles(sourceFolder);

                                    // Copy the files and overwrite destination files if they already exist.
                                    foreach (string s in files)
                                    {
                                        // Use static Path methods to extract only the file name from the path.
                                        String fileName = System.IO.Path.GetFileName(s);
                                        String destFile = System.IO.Path.Combine(OutputVideosFolderFile, fileName);
                                        System.IO.File.Copy(s, destFile, true);
                                    }//files copy
                                } //if
                            }//if folders exist

                        }//for
                    }//try
                    catch (Exception ex)
                    {

                    }//catch
                }//Term Of Use Information    
            }//Else - Folder is Valid

            MessageBox.Show( ResultsDataGrid.RowCount + " Files were Transfered Successfully", "MetaFace", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();

        }//materialRaisedButton2_Click

       
    }//retriveExportForm
}//MetaFace
